#include "IRQ_HANDLER.h"


CanRxMsg msg;
void CAN1_RX0_IRQHandler (void)
{
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
		CAN_Receive(CAN1, CAN_FIFO0, &msg);
        
        if(msg.StdId == MC_BAMOCAR_RESPONSE_ID)
        {
            msgResponse = msg;
            msgPendingFlag = 1;
        }
	}
	__enable_irq();
}

/* ---START--- */
void EXTI15_10_IRQHandler(void)
{
    __disable_irq();
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
    {
        if(BTN_Status(BTN_GPIOx(START), BTN_GPIO_Pin(START)) == BTN_HIGH) // Prell
        {
            if(CARSTATUS == Standby) // Is car in standby
            {
                if(pedal_High(BREAK_PEDAL)) // Brake pedal is pressed down
                {
                    if(pedal_OFF(GAS_R) && pedal_OFF(GAS_L)) // Gas pedal is NOT pressed down
                    {
                        EEPROM_AT24C256C_Write8(CARISRUNNING_ADDRESS, Running);
                        ION_QUEUE_Insert(CAN_Queue, BTN_START, 0, 0);
                        IO_pinHigh(RFE_GPIO, RFE_PIN);
                        IO_pinHigh(FRG_RUN_GPIO, FRG_RUN_PIN);
                        
                        
                        IO_pinHigh(RTDS_GPIO, RTDS_PIN);
                        uint32_t timeStamp = relativeTime;
                        while((timeStamp + 2000) > relativeTime);
                        IO_pinLow(RTDS_GPIO, RTDS_PIN);
                        CARSTATUS = Running;
                    }
                }
            }
        }
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
    
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line14) != RESET) {
        
        // Her sender jeg den nyeste dempe verdien
        //COMLine_Send(dempe);
        
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
    if (EXTI_GetITStatus(EXTI_Line15) != RESET) {
        
        // Her sender jeg den nyeste speed verdien
        //COMLine_Send(speed);
        
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
    
	__enable_irq();
}

/* ---STOP--- */
void EXTI9_5_IRQHandler(void)
{
    __disable_irq();
	if(EXTI_GetITStatus(EXTI_Line6) != RESET)
    {
		if(BTN_Status(BTN_GPIOx(STOP), BTN_GPIO_Pin(STOP)) == BTN_HIGH) // Prell
        {
            if(CARSTATUS == Running) // Is car running
            {
                EEPROM_AT24C256C_Write8(CARISRUNNING_ADDRESS, Standby);
                ION_QUEUE_Insert(CAN_Queue, BTN_STOP, 0, 0);
                IO_pinLow(RFE_GPIO, RFE_PIN);
                IO_pinLow(FRG_RUN_GPIO, FRG_RUN_PIN);
                CARSTATUS = Standby;
            }
        }
		EXTI_ClearITPendingBit(EXTI_Line6);
	}
	__enable_irq();
}
