#include "ION_QUEUE.h"

xQueueHandle CAN_Queue;

void ION_QUEUE_Initialise(uint16_t size)
{
    CAN_Queue = xQueueCreate(size, sizeof(dataPackage));
}

static dataPackage DataSensorPackage;
void ION_QUEUE_Insert(xQueueHandle *QUEUE, uint16_t id, uint8_t length, uint32_t data)
{
    DataSensorPackage.ID = id;
    DataSensorPackage.LEN = length;
    DataSensorPackage.DATA = data;
    xQueueSendToBack(QUEUE, &DataSensorPackage, portMAX_DELAY);
}
