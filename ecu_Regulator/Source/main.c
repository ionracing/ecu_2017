//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

//******************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
//******************************************************************************

//******************************************************************************
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "croutine.h"
//******************************************************************************

//******************************************************************************
#include "global.h"
#include "io_mapping.h"
#include "ION_QUEUE.h"
#include "watchdog.h"
#include "EEPROM_AT24C256C.h"
#include "BTN.h"
#include "LED.h"
#include "TIM.h"
#include "IO.h"
#include "ADC_SIMPLE.h"
#include "ION_ID.h"
#include "PWM.h"
#include "MC_BAMOCAR.h"
//******************************************************************************

uint32_t *ptrValues[100];

void vLED(void *pvParameters);
void vWatchdog(void *pvParameters);
void vCAN(void *pvParameters);
void vADC(void *pvParameters);

#define STACK_SIZE_MIN	    1000	/* usStackDepth	- the stack size DEFINED IN WORDS.*/
#define NUMBER_OF_THREADS   2     /* does not include watchdog thread. */

/* ---Globals--- */
uint32_t relativeTime = 0;
carStatus CARSTATUS = Standby;

/* ---LocalVariables--- */
FLAG FLAGS[10] = {ASLEEP};
uint8_t DIFF = 0x8D;

//******************************************************************************
int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
	
    /* ---LEDS--- */
    LED_Initialise(LED1);
    LED_Initialise(LED2);
    LED_Initialise(LED3);
    LED_Initialise(LED4);
    
    /* ---IO--- */
    IO_initPinOut(KERS_GPIO, KERS_PIN);
    IO_initPinOut(FLT_GPIO, FLT_PIN);
    IO_initPinOut(TEMP_GPIO, TEMP_PIN);
    IO_initPinOut(EXTRA_GPIO, EXTRA_PIN);
    IO_initPinOut(BRAKE_LIGHT_GPIO, BRAKE_LIGHT_PIN);
    IO_initPinOut(FAN_GPIO, FAN_PIN);
    IO_initPinOut(SC_REALAY_GPIO, SC_REALAY_PIN);
    IO_initPinOut(RTDS_GPIO, RTDS_PIN);
    
    /* ---QUEUE--- */
    ION_QUEUE_Initialise(100);
    
    /* ---BUTTON--- */
    BTN_Initialise(START);
    BTN_Initialise(STOP);
    
    /* ---ADC--- */
    ADC_SIMPLE_init(APPS1);
    ADC_SIMPLE_init(APPS2);
    ADC_SIMPLE_init(BPPS);
    ADC_SIMPLE_init(BSP2);
    ADC_SIMPLE_init(BPS1);
    ADC_SIMPLE_init(AIRTEMP);
    ADC_SIMPLE_init(STEERING);
    
    /* ---CAN--- */
    CAN_Initialise();
    
    /* ---PWM--- */
    //PWM_BMS_Init();
    
    /* ---WATCHDOG--- */
    if (RCC->CSR & RCC_CSR_WDGRSTF)
    {
        //CAR WAS RUNNING
        if(EEPROM_AT24C256C_Read8(CARISRUNNING_ADDRESS) == Running)
        {
            ION_QUEUE_Insert(CAN_Queue, WATCHDOG, 0, 0);
            GPIO_SetBits(RFE_GPIO, RFE_PIN);
            GPIO_SetBits(FRG_RUN_GPIO, FRG_RUN_PIN);
            CARSTATUS = Running;
        }
    }
    else
    {
        EEPROM_AT24C256C_Write8(CARISRUNNING_ADDRESS, Standby);
    }
    
    
    /* ---THREADS--- */
    xTaskCreate(vWatchdog, (const char*)"Watchdog", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL);
    xTaskCreate(vLED, (const char*)"LED", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL);
    xTaskCreate(vCAN, (const char*)"CAN", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL);
    xTaskCreate(vADC, (const char*)"ADC", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL);
	
    vTaskStartScheduler();
}
//******************************************************************************

//******************************************************************************
void vLED(void *pvParameters)
{
    for(;;)
    {
        LEDx_Toggle(LED1);
        vTaskDelay( 250 / portTICK_RATE_MS );
    }
    
}

void vWatchdog(void *pvParameters)
{
    uint8_t xFlag;
    for(;;)
    {
        xFlag = 0;
        for(int i = 0;i < NUMBER_OF_THREADS;i++)
        {
            switch(FLAGS[i])
            {
                case UNKNOWN:
                    xFlag++;
                    break;
                case ALIVE:
                    FLAGS[i] = UNKNOWN;
                    break;
                case ASLEEP:
                    break;
            }
        }
        
        if(xFlag == 0)
            watchdog_pet();
        taskYIELD();
    }
}

void vCAN(void *pvParameters)
{
    FLAG* flg = &FLAGS[2];
    dataPackage ReceivedValue;
    portBASE_TYPE xStatus;
    uint8_t data[8] = {0};
    
    for(;;)
    {
        *flg = ALIVE;
        xStatus = xQueueReceive(CAN_Queue, &ReceivedValue, portMAX_DELAY);
        if(xStatus == pdPASS)
        {
            data[0] = 0xFF & ReceivedValue.DATA;
            data[1] = 0xFF & (ReceivedValue.DATA >> 8);
            data[2] = 0xFF & (ReceivedValue.DATA >> 16);
            data[3] = 0xFF & (ReceivedValue.DATA >> 24);
            CAN_Write_8(CAN_CANx, ReceivedValue.ID, ReceivedValue.LEN, data);
        }
        *flg = ASLEEP;
    }
}

void vADC(void *pvParameters)
{
    
    uint16_t valBPS1 = 0, valBSP2 = 0, valAIRTEMP = 0, valAPPS1 = 0, valAPPS2 = 0, valBPPS = 0, valSTEERING = 0;
    uint16_t Difference = 0;
    uint8_t command[5] = {0};
    uint32_t gasPedal = 0;
    int16_t torque = 0;
    
    for(;;)
    {
        valBPS1 = ADC_SIMPLE_readChannel(BPS1);
        valBSP2 = ADC_SIMPLE_readChannel(BSP2);
        valAIRTEMP = ADC_SIMPLE_readChannel(AIRTEMP);
        valAPPS1 = ADC_SIMPLE_readChannel(APPS1);
        valAPPS2 = ADC_SIMPLE_readChannel(APPS2);
        valBPPS = ADC_SIMPLE_readChannel(BPPS);
        valSTEERING = ADC_SIMPLE_readChannel(STEERING);
        
        Difference = valAPPS1 >= valAPPS2 ? valAPPS1 - valAPPS2 : valAPPS2 - valAPPS1;
        if(Difference > (DIFF * 1.05))
        {
            EEPROM_AT24C256C_Write8(CARISRUNNING_ADDRESS, Standby);
            ION_QUEUE_Insert(CAN_Queue, ADC_MISMATCH, 0, 0);
            IO_pinLow(RFE_GPIO, RFE_PIN);
            IO_pinLow(FRG_RUN_GPIO, FRG_RUN_PIN);
            CARSTATUS = Standby;
        }
        //SEND ON CAN
        ION_QUEUE_Insert(CAN_Queue, ADC_BPS1, 2, valBPS1);
        ION_QUEUE_Insert(CAN_Queue, ADC_BPS2, 2, valBSP2);
        ION_QUEUE_Insert(CAN_Queue, ADC_AIRTEMP, 2, valAIRTEMP);
        ION_QUEUE_Insert(CAN_Queue, ADC_APPS1, 2, valAPPS1);
        ION_QUEUE_Insert(CAN_Queue, ADC_APSS2, 2, valAPPS2);
        ION_QUEUE_Insert(CAN_Queue, ADC_BPPS, 2, valBPPS);
        ION_QUEUE_Insert(CAN_Queue, ADC_STEERING, 2, valSTEERING);
        
        // Brake ligth
        if(valBPPS > 0x400)
            IO_pinHigh(BRAKE_LIGHT_GPIO, BRAKE_LIGHT_PIN);
        else
            IO_pinHigh(BRAKE_LIGHT_GPIO, BRAKE_LIGHT_PIN);
        
        //Send to MC
        if(CARSTATUS == Running)
        {
            gasPedal = ((valAPPS1 + valAPPS2) / 2);
            if(gasPedal < 0x400)
                gasPedal = 0;
            if(gasPedal > 4095)
                gasPedal = 4095;
            torque = (int16_t)(gasPedal << 3);
            if(torque < 0)
                torque = 0;
            torque = (torque*(-1));
            
            command[0] = 0x90; //ID for the tourque
            command[1] = (0xFF & torque);
            command[2] = (0xFF & (torque >> 8));
            
            MC_BAMOCAR_Command(command, 3);
        }
    }
    
}
//******************************************************************************
