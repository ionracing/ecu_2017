#include "COMLine.h"

/*---COMLine---*/
/* 
Is the lines that connect collector chip and regulator chip.
We use these to comunicate and send speed and damper movements.
We want only to send when we get a high pin on the line. I am
not sure how I want to do with all the data that is read inbetween.
*/


void COMLine_initialise()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
  
    /*---Clock---*/
    RCC_AHB1PeriphClockCmd(COM_GPIO_RCC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /*---COMLines out---*/
    GPIO_InitStruct.GPIO_Pin = COM_GPIO_PIN_OUT | COM_GPIO_PIN_ACCEPTED;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = COM_GPIO_OTYPE;
    GPIO_InitStruct.GPIO_PuPd = COM_GPIO_PUPD;
    GPIO_InitStruct.GPIO_Speed = COM_GPIO_SPEED;
    GPIO_Init(COM_GPIO_GPIOX, &GPIO_InitStruct);
    
    /*---COMLines in---*/
    GPIO_InitStruct.GPIO_Pin = COM_GPIO_PIN_INN | COM_GPIO_PIN_READY;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(COM_GPIO_GPIOX, &GPIO_InitStruct);
    
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource12);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource13);

	EXTI_InitStruct.EXTI_Line = EXTI_Line12 | EXTI_Line13; 
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

/*---Send---*/
void COMLine_Send(uint16_t data)
{
    COM_GPIO_GPIOX->ODR |= (0xFFF & data);
    GPIO_SetBits(COM_GPIO_GPIOX, COM_GPIO_PIN_READY);
    while(GPIO_ReadInputDataBit(COM_GPIO_GPIOX, COM_GPIO_PIN_ACCEPTED));
    GPIO_ResetBits(COM_GPIO_GPIOX, COM_GPIO_PIN_READY);
}
