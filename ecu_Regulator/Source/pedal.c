#include "pedal.h"

uint8_t pedal_High(PEDAL pedal)
{
    uint16_t p = ADC_SIMPLE_readChannel(PEDAL_ADC(pedal)); //First mesure pedal.
    if(p > ((PEDAL_MAX(pedal) - (PEDAL_MAX(pedal) * PEDAL_MAX_PERCENTAGE) / 100))) // if pedal is between a certain percentage and max then it is high
        return 1;
    else
        return 0;
}

uint8_t pedal_OFF(PEDAL pedal)
{
    uint16_t p = ADC_SIMPLE_readChannel(PEDAL_ADC(pedal)); //First mesure pedal.
    if(p < ((PEDAL_MIN(pedal) + (PEDAL_MIN(pedal) * PEDAL_OFF_PERCENTAGE) / 100))) // if pedal is between a certain percentage and max then it is high
        return 1;
    else
        return 0;
}
