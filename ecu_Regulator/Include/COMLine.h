#ifndef  COMLINE_H
#define COMLINE_H

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "global.h"
#include "IO_Mapping.h"
                        
void COMLine_initialise(void);
void COMLine_Send(uint16_t data);

#endif // COMLINE_H
