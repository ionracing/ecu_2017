#ifndef IO_MAPPING_H
#define IO_MAPPING_H

#include "ERROR_Handeling.h"

#if defined COMPILING_FOR_DEVBOARD
    //#warning "compiling for development board!"
    #include "IO_Mapping_Discovery.h"
#elif defined COMPILING_FOR_ECU_REGULATOR
    #include "IO_Mapping_ECU_Regulator.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
