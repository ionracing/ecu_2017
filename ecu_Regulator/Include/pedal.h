#ifndef PEDAL_H
#define PEDAL_H

#include "stdint.h"
#include "IO_Mapping.h"
#include "ADC_SIMPLE.h"

uint8_t pedal_High(PEDAL pedal);
uint8_t pedal_OFF(PEDAL pedal);

#endif //PEDAL_H
