#ifndef GLOBAL_H
#define GLOBAL_H

#include "FreeRTOS.h"
#include "queue.h"

typedef struct dataPackage
{
    uint16_t ID;
    uint8_t LEN;
    uint32_t DATA;
} dataPackage;

typedef enum carStatus
{
    Standby = 0,
    Running
} carStatus;

extern carStatus CARSTATUS;

extern xQueueHandle CAN_Queue;

extern uint32_t relativeTime;

#endif //GLOBAL_H
