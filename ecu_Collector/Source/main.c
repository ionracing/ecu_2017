//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

//******************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
//******************************************************************************

//******************************************************************************
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "croutine.h"
//******************************************************************************

//******************************************************************************
#include "global.h"
#include "IO_Mapping.h"
#include "ION_QUEUE.h"
#include "LED.h"
#include "ADC.h"
#include "TIM.h"
//******************************************************************************

uint32_t *ptrValues[100];

void vLedBlink(void *pvParameters);

#define STACK_SIZE_MIN	1000	/* usStackDepth	- the stack size DEFINED IN WORDS.*/

/* ---Globals--- */
uint32_t relativeTime = 0;
uint16_t Sllip_Value = 0;
uint16_t Damper_Values[4] = {0};

//******************************************************************************
int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
	
    //INITS
    //LED_Initialise();
    ION_QUEUE_Initialise(100);
    
    
    /* ---THREADS--- */
    xTaskCreate( vLedBlink, (const char*)"Led Blink Task", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );

	//vTaskStartScheduler();
}
//******************************************************************************

//******************************************************************************
void vLedBlink(void *pvParameters)
{
	for(;;)
	{
		LEDx_Toggle(RED);
		vTaskDelay( 250 / portTICK_RATE_MS );
         LEDx_Toggle(GREEN);
		vTaskDelay( 250 / portTICK_RATE_MS );
        LEDx_Toggle(BLUE);
		vTaskDelay( 250 / portTICK_RATE_MS );
        LEDx_Toggle(ORANGE);
		vTaskDelay( 250 / portTICK_RATE_MS );
	}
}
//******************************************************************************
