#ifndef  COMLINE_H
#define COMLINE_H

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "global.h"

#define COM_GPIO GPIOD

#define COMLines_out    GPIO_Pin_0 | \
                        GPIO_Pin_1 | \
                        GPIO_Pin_2 | \
                        GPIO_Pin_3 | \
                        GPIO_Pin_4 | \
                        GPIO_Pin_5 | \
                        GPIO_Pin_6 | \
                        GPIO_Pin_7 | \
                        GPIO_Pin_8 | \
                        GPIO_Pin_9 | \
                        GPIO_Pin_10 | \
                        GPIO_Pin_11
                        
#define IRQ_Pins        GPIO_Pin_12 | \
                        GPIO_Pin_13
                        
#define DATA_READY      GPIO_Pin_14
#define DATA_ACCEPTED   GPIO_Pin_15
                        
void COMLine_initialise(void);
void COMLine_Send(uint16_t data);

#endif // COMLINE_H
