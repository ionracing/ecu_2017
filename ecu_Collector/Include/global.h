#ifndef GLOBAL_H
#define GLOBAL_H

#include "FreeRTOS.h"
#include "queue.h"

extern xQueueHandle TT_Queue;
extern xQueueHandle SD_Queue;
extern xQueueHandle GPS_Queue;

extern uint32_t relativeTime;

extern uint16_t Damper_Values[4];
extern uint16_t Slip_Value;

#endif //GLOBAL_H
