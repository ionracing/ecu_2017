#ifndef IO_MAPPING_H
#define IO_MAPPING_H

#include "ERROR_Handeling.h"

#if defined COMPILING_FOR_DEVBOARD
    //#warning "compiling for development board!"
    #include "IO_Mapping_Discovery.h"
#elif defined COMPILING_FOR_V1
    #include "IO_Mapping_V1.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
