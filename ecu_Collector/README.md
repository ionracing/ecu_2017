# READ ME - ECU_Collector
This is the reposetory for the ecu_Collector chip that will be in the ECU.
This project uses FREERTOS.

The job for this chip is to collect data from the car damper Sensors and speed sensors.

### Damper sensors
We use RSL-75mm to mesure the damper travel. Since we need better resolution we use LTC2400 and talk to these with SPI.

### Speed sensors
We use SNG-QPLA-000 to messur the wheel spin. The wheel have 24 pins that the sensors will detect as high and we use IRQ handler to log each high pulse and mesure the time between each high pulse to be able to calculate the speed.

## Sketch
![alt text](https://bytebucket.org/ionracing/ecu_collector/raw/c233f8052d57ebea76259d7a25764f72bd530682/Photos/skisse1.jpg "Skisse 1")

## Plan Data
1. Sensors (I^2C/SPI/ADC)
    * Get data
    * Transform data
    * Packet data
2. Telemetry (SPI)
    * Send
    * Recieve
    * Change parameters
3. SD-Card (SPI)
    * Save data
    * Read data
    * Save parameters
    * Read parameters
4. Change
5. GPS (USART)
6. IMU (SPI)
7. GPIO

## Plan Elektro
1. Kontroll
2. Pådrag
3. Regler
4. Tester o.l
