# READ ME
This is the reposetory for the ecu_logger chip that will be in the ECU.
This project uses FREERTOS.

The job for this chip is to log data from all the sensors and from the CAN bus line. We log our data on SD_Card and send it on telemetry to display and log it on our pit gui analyser program.

## Sketch
![alt text](https://bytebucket.org/ionracing/ecu_logger/raw/8fc9a5a38bd7b2c600d86530386e0a76dab83829/Photos/skisse1.jpg "Skisse 1")

## Plan Data
1. Sensors (I^2C/SPI/ADC)
    * Get data
    * Transform data
    * Packet data
2. Telemetry (SPI)
    * Send
    * Recieve
    * Change parameters
3. SD-Card (SPI)
    * Save data
    * Read data
    * Save parameters
    * Read parameters
4. Change
5. GPS (USART)
6. IMU (SPI)
7. GPIO

## Plan Elektro
1. Kontroll
2. Pådrag
3. Regler
4. Tester o.l
