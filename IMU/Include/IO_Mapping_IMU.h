#ifndef IOMAPPING_IMU_H
#define IOMAPPING_IMU_H

/* ---LED--- */
typedef enum
{
    LED1 = 0,
    LED2
} LEDs;

#define LED_GPIO_RCC(LEDx)  (LEDx == LED1) ? RCC_AHB1Periph_GPIOB : \
                            (LEDx == LED2) ? RCC_AHB1Periph_GPIOB : 666
#define LED_GPIO_PIN(LEDx)  (LEDx == LED1) ? GPIO_Pin_12 : \
                            (LEDx == LED2) ? GPIO_Pin_13 : 666
#define LED_GPIO(LEDx)      (LEDx == LED1) ? GPIOB : \
                            (LEDx == LED2) ? GPIOB : GPIOG

/* ---CAN--- */
#define CAN_GPIO_RCC                RCC_AHB1Periph_GPIOB
#define CAN_GPIO_MODE               GPIO_Mode_AF
#define CAN_GPIO_OTYPE              GPIO_OType_PP
#define CAN_GPIO_PIN_RX             GPIO_Pin_8
#define CAN_GPIO_PIN_TX             GPIO_Pin_9
#define CAN_GPIO_PUPD               GPIO_PuPd_UP
#define CAN_GPIO_SPEED              GPIO_Speed_100MHz
#define CAN_GPIO_GPIOX              GPIOB
#define CAN_GPIO_PinSource_RX       GPIO_PinSource8
#define CAN_GPIO_PinSource_TX       GPIO_PinSource9
#define CAN_GPIO_AF_CAN             GPIO_AF_CAN1
#define CAN_CAN_RCC                 RCC_APB1Periph_CAN1
#define CAN_CANx                    CAN1
#define CAN_NVIC_IRQChannel         CAN1_RX0_IRQn
#define CAN_NVIC_IRQChannelPP       0
#define CAN_NVIC_IRQChannelSP       1
#define CAN_NVIC_IRQChannelCmd      ENABLE

/* ---SPI--- */
#define SPI_RCC_GPIO(SPIx)              ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                        ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : \
                                        ((SPIx == SPI3) ? RCC_AHB1Periph_GPIOC : ERROR_Msg("WRONG SPI_RCC_GPIO"))))
#define SPI_RCC_GPIO_CS(SPIx)           ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                        ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : \
                                        ((SPIx == SPI3) ? RCC_AHB1Periph_GPIOA : ERROR_Msg("WRONG SPI_RCC_GPIO_CS"))))
#define SPI_RCC_SPI(SPIx)               ((SPIx == SPI1) ? RCC_APB2Periph_SPI1 : \
                                        ((SPIx == SPI2) ? RCC_APB1Periph_SPI2 : \
                                        ((SPIx == SPI3) ? RCC_APB1Periph_SPI3 : ERROR_Msg("WRONG SPI_RCC_SPI"))))
#define SPI_GPIOx(SPIx)                 ((SPIx == SPI1) ? GPIOA : \
                                        ((SPIx == SPI2) ? GPIOB : \
                                        ((SPIx == SPI3) ? GPIOC : (ERROR_Msg("WRONG SPI_GPIOx") == 0 ? GPIOA : GPIOA))))
#define SPI_GPIOx_CS(SPIx)              ((SPIx == SPI1) ? GPIOA : \
                                        ((SPIx == SPI2) ? GPIOB : \
                                        ((SPIx == SPI3) ? GPIOA : (ERROR_Msg("WRONG SPI_GPIOx_CS") == 0 ? GPIOA : GPIOA))))
#define SPI_GPIO_PinSource_SCLK(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource5 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource13 : \
                                        ((SPIx == SPI3) ? GPIO_PinSource10 : ERROR_Msg("WRONG SPI_GPIO_PinSource_SCLK"))))
#define SPI_GPIO_PinSource_MISO(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource6 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource14 : \
                                        ((SPIx == SPI3) ? GPIO_PinSource11 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MISO"))))
#define SPI_GPIO_PinSource_MOSI(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource7 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource15 : \
                                        ((SPIx == SPI3) ? GPIO_PinSource12 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MOSI"))))
#define SPI_GPIO_AF_SPI(SPIx)           ((SPIx == SPI1) ? GPIO_AF_SPI1 : \
                                        ((SPIx == SPI2) ? GPIO_AF_SPI2 : \
                                        ((SPIx == SPI3) ? GPIO_AF_SPI2 : ERROR_Msg("WRONG SPI_GPIO_AF_SPI"))))
#define SPI_GPIO_Pin_CS(SPIx)           ((SPIx == SPI1) ? GPIO_Pin_4 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_12 : \
                                        ((SPIx == SPI3) ? GPIO_Pin_12 : ERROR_Msg("WRONG SPI_GPIO_Pin_CS"))))
#define SPI_GPIO_Pin_SCLK(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_5 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_13 : \
                                        ((SPIx == SPI3) ? GPIO_Pin_13 : ERROR_Msg("WRONG SPI_GPIO_Pin_SCLK"))))
#define SPI_GPIO_Pin_MISO(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_6 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_14 : \
                                        ((SPIx == SPI3) ? GPIO_Pin_14 : ERROR_Msg("WRONG SPI_GPIO_Pin_MISO"))))
#define SPI_GPIO_Pin_MOSI(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_7 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_15 : \
                                        ((SPIx == SPI3) ? GPIO_Pin_15 : ERROR_Msg("WRONG SPI_GPIO_Pin_MOSI"))))

#endif
