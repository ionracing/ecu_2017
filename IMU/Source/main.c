//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

//******************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
//******************************************************************************

//******************************************************************************
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "croutine.h"
//******************************************************************************

//******************************************************************************
#include "global.h"
#include "watchdog.h"
#include "LED.h"
#include "IMU_SCC2130.h"
#include "IO_Mapping.h"
#include "IO_Mapping_IMU.h"
#include "CAN.h"
//******************************************************************************
/* ---Defines--- */
#define STACK_SIZE_MIN 1000     /* usStackDepth - the stack size DEFINED IN WORDS.*/
#define NUMBER_OF_THREADS 2     /* does not include watchdog thread. */
#define QUEUE_SIZE 100

/* ---Globals--- */
uint32_t relativeTime = 0;

/* ---LocalVariables--- */
uint32_t *ptrValues[100];
FLAG FLAGS[2] = {ASLEEP, ASLEEP};

/* ---Threads--- */
void vLED(void *pvParameters);
void vWatchdog(void *pvParameters);
void vIMU(void *pvParameters);

//******************************************************************************
int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
    
    /* ---Inits--- */
    //watchdog_init();
    LED_Initialise(LED1);
    LED_Initialise(LED2);
    CAN_Initialise();
    //IMU_Initialise();
    
    if (RCC->CSR & RCC_CSR_WDGRSTF)
    {
        LEDx_On(LED1);
        LEDx_On(LED2);
        /* Clear reset flags */ 
        RCC->CSR |= RCC_CSR_RMVF;
        while(1){};
    }
    
    while(1)
    {
        CAN_Write_16_bit(CAN1, 0xAA, 0xAA);
        LEDx_Toggle(LED2);
        for(int i = 0;i < 0xFFFFFF;i++);
        if(1 == 2)
            break;
    }
    /* ---THREADS--- */
    xTaskCreate( vLED, (const char*)"LED", STACK_SIZE_MIN, NULL, 1, NULL );
    xTaskCreate( vIMU, (const char*)"IMU", STACK_SIZE_MIN, NULL, 1, NULL );
    //xTaskCreate( vWatchdog, (const char*)"Watchdog", STACK_SIZE_MIN, NULL, 1, NULL );

    vTaskStartScheduler();
    
    while(1);
}
//******************************************************************************

//******************************************************************************
void vWatchdog(void *pvParameters)
{
    uint8_t xFlag;
    for(;;)
    {
        xFlag = 0;
        for(int i = 0;i < NUMBER_OF_THREADS;i++)
        {
            switch(FLAGS[i])
            {
                case UNKNOWN:
                    xFlag++;
                    break;
                case ALIVE:
                    FLAGS[i] = UNKNOWN;
                    break;
                case ASLEEP:
                    break;
            }
        }
        
        if(xFlag == 0)
            watchdog_pet();
        taskYIELD();
    }
}


void vLED(void *pvParameters)
{
    FLAG* flg = &FLAGS[0];
    for(;;)
    {
        *flg = ALIVE;
        LEDx_Toggle(LED2);
        vTaskDelay( 250 / portTICK_RATE_MS );
        *flg = ASLEEP;
        taskYIELD();
    }
}

void vIMU(void *pvParameters)
{
    FLAG* flg = &FLAGS[1];
    for(;;)
    {
        *flg = ALIVE;
        IMU_WriteRead(RATE);
        uint32_t rate = IMU_WriteRead(ACC_X);
        uint32_t acc_x = IMU_WriteRead(ACC_Y);
        uint32_t acc_y = IMU_WriteRead(ACC_Z);
        uint32_t acc_z = IMU_WriteRead(TEMP);
        uint32_t tempp = IMU_WriteRead(0xFFFFFFFF);
        
        CAN_Write_16_bit(CAN1, IMU_RATE, 0xFF & (rate >> 8));
        CAN_Write_16_bit(CAN1, IMU_ACC_X, 0xFF & (acc_x >> 8));
        CAN_Write_16_bit(CAN1, IMU_ACC_Y, 0xFF & (acc_y >> 8));
        CAN_Write_16_bit(CAN1, IMU_ACC_Z, 0xFF & (acc_z >> 8));
        CAN_Write_16_bit(CAN1, IMU_TEMP, 0xFF & (tempp >> 8));
        LEDx_Toggle(LED1);
        vTaskDelay( 250 / portTICK_RATE_MS );
        *flg = ASLEEP;
        taskYIELD();
    }
}

//******************************************************************************
