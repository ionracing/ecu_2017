#include "defines.h"

void defines_initialise()
{
    SPI1_Struct.SPIx = SPI1;
    SPI1_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOA;
    SPI2_Struct.RCC_GPIO_CS = RCC_AHB1Periph_GPIOA;
    SPI1_Struct.RCC_SPI = RCC_APB2Periph_SPI1;
    SPI1_Struct.GPIOx = GPIOA;
    SPI1_Struct.GPIOx_CS = GPIOA;
    SPI1_Struct.GPIO_PinSource_SCLK = GPIO_PinSource5;
    SPI1_Struct.GPIO_PinSource_MISO = GPIO_PinSource6;
    SPI1_Struct.GPIO_PinSource_MOSI = GPIO_PinSource7;
    SPI1_Struct.GPIO_AF_SPIx = GPIO_AF_SPI1;
    SPI1_Struct.GPIO_Pin_CS = GPIO_Pin_4;
    SPI1_Struct.GPIO_Pin_SCLK = GPIO_Pin_5;
    SPI1_Struct.GPIO_Pin_MISO = GPIO_Pin_6;
    SPI1_Struct.GPIO_Pin_MOSI = GPIO_Pin_7;

    SPI2_Struct.SPIx = SPI2;
    SPI2_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOB;
    SPI2_Struct.RCC_GPIO_CS = RCC_AHB1Periph_GPIOB;
    SPI2_Struct.RCC_SPI = RCC_APB1Periph_SPI2;
    SPI2_Struct.GPIOx = GPIOB;
    SPI2_Struct.GPIOx_CS = GPIOB;
    SPI2_Struct.GPIO_PinSource_SCLK = GPIO_PinSource13;
    SPI2_Struct.GPIO_PinSource_MISO = GPIO_PinSource14;
    SPI2_Struct.GPIO_PinSource_MOSI = GPIO_PinSource15;
    SPI2_Struct.GPIO_AF_SPIx = GPIO_AF_SPI2;
    SPI2_Struct.GPIO_Pin_CS = GPIO_Pin_12;
    SPI2_Struct.GPIO_Pin_SCLK = GPIO_Pin_13;
    SPI2_Struct.GPIO_Pin_MISO = GPIO_Pin_14;
    SPI2_Struct.GPIO_Pin_MOSI = GPIO_Pin_15;
    
    SPI2_Struct.SPIx = SPI3;
    SPI2_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOC;
    SPI2_Struct.RCC_GPIO_CS = RCC_AHB1Periph_GPIOA;
    SPI2_Struct.RCC_SPI = RCC_APB1Periph_SPI3;
    SPI2_Struct.GPIOx = GPIOC;
    SPI2_Struct.GPIOx_CS = GPIOA;
    SPI2_Struct.GPIO_PinSource_SCLK = GPIO_PinSource10;
    SPI2_Struct.GPIO_PinSource_MISO = GPIO_PinSource11;
    SPI2_Struct.GPIO_PinSource_MOSI = GPIO_PinSource12;
    SPI2_Struct.GPIO_AF_SPIx = GPIO_AF_SPI2;
    SPI2_Struct.GPIO_Pin_CS = GPIO_Pin_12;
    SPI2_Struct.GPIO_Pin_SCLK = GPIO_Pin_13;
    SPI2_Struct.GPIO_Pin_MISO = GPIO_Pin_14;
    SPI2_Struct.GPIO_Pin_MOSI = GPIO_Pin_15;
    
    USART1_Struct.USARTx = USART1;
    USART1_Struct.RCC_USART = RCC_APB2Periph_USART1;
    USART1_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOD;
    USART1_Struct.GPIO_Pin_Tx = GPIO_Pin_1;
    USART1_Struct.GPIO_Pin_Rx = GPIO_Pin_2;
    USART1_Struct.GPIOx = GPIOA;
    USART1_Struct.GPIO_PinSource_Tx = GPIO_PinSource2;
    USART1_Struct.GPIO_PinSource_Rx = GPIO_PinSource3;
    USART1_Struct.GPIO_AF_USARTx = GPIO_AF_USART2;
    USART1_Struct.USART_BaudRate = 9600;
    USART1_Struct.NVIC_Enable = ENABLE;
    USART1_Struct.NVIC_IRQChannel = USART1_IRQn;
    USART1_Struct.NVIC_IRQChannelPreemptionPriority = 0;
    USART1_Struct.NVIC_IRQChannelSubPriority = 1;
    
    USART2_Struct.USARTx = USART2;
    USART2_Struct.RCC_USART = RCC_APB1Periph_USART2;
    USART2_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOD;
    USART2_Struct.GPIO_Pin_Tx = GPIO_Pin_1;
    USART2_Struct.GPIO_Pin_Rx = GPIO_Pin_2;
    USART2_Struct.GPIOx = GPIOA;
    USART2_Struct.GPIO_PinSource_Tx = GPIO_PinSource2;
    USART2_Struct.GPIO_PinSource_Rx = GPIO_PinSource3;
    USART2_Struct.GPIO_AF_USARTx = GPIO_AF_USART2;
    USART2_Struct.USART_BaudRate = 9600;
    USART2_Struct.NVIC_Enable = ENABLE;
    USART2_Struct.NVIC_IRQChannel = USART2_IRQn;
    USART2_Struct.NVIC_IRQChannelPreemptionPriority = 0;
    USART2_Struct.NVIC_IRQChannelSubPriority = 1;
    
    USART3_Struct.USARTx = USART3;
    USART3_Struct.RCC_USART = RCC_APB1Periph_USART3;
    USART3_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOD;
    USART3_Struct.GPIO_Pin_Tx = GPIO_Pin_1;
    USART3_Struct.GPIO_Pin_Rx = GPIO_Pin_2;
    USART3_Struct.GPIOx = GPIOA;
    USART3_Struct.GPIO_PinSource_Tx = GPIO_PinSource2;
    USART3_Struct.GPIO_PinSource_Rx = GPIO_PinSource3;
    USART3_Struct.GPIO_AF_USARTx = GPIO_AF_USART2;
    USART3_Struct.USART_BaudRate = 9600;
    USART3_Struct.NVIC_Enable = ENABLE;
    USART3_Struct.NVIC_IRQChannel = USART3_IRQn;
    USART3_Struct.NVIC_IRQChannelPreemptionPriority = 0;
    USART3_Struct.NVIC_IRQChannelSubPriority = 1;
    
    USART6_Struct.USARTx = USART6;
    USART6_Struct.RCC_USART = RCC_APB2Periph_USART6;
    USART6_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOD;
    USART6_Struct.GPIO_Pin_Tx = GPIO_Pin_1;
    USART6_Struct.GPIO_Pin_Rx = GPIO_Pin_2;
    USART6_Struct.GPIOx = GPIOA;
    USART6_Struct.GPIO_PinSource_Tx = GPIO_PinSource2;
    USART6_Struct.GPIO_PinSource_Rx = GPIO_PinSource3;
    USART6_Struct.GPIO_AF_USARTx = GPIO_AF_USART2;
    USART6_Struct.USART_BaudRate = 9600;
    USART6_Struct.NVIC_Enable = ENABLE;
    USART6_Struct.NVIC_IRQChannel = USART6_IRQn;
    USART6_Struct.NVIC_IRQChannelPreemptionPriority = 0;
    USART6_Struct.NVIC_IRQChannelSubPriority = 1;
    
    BTN1_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOA;
    BTN1_Struct.GPIO_Pin = GPIO_Pin_0;  
    BTN1_Struct.GPIOx = GPIOA;
    BTN1_Struct.EXTI_PortSource = EXTI_PortSourceGPIOA;
    BTN1_Struct.EXTI_PinSource = EXTI_PinSource0;
    BTN1_Struct.EXTI_Linex = EXTI_Line0;
    BTN1_Struct.NVIC_Enable = ENABLE;
    BTN1_Struct.NVIC_IRQChannel = EXTI0_IRQn;
    BTN1_Struct.NVIC_IRQChannelPreemptionPriority = 0;
    BTN1_Struct.NVIC_IRQChannelSubPriority = 1;
    
    LED1_Struct.RCC_GPIO = RCC_AHB1Periph_GPIOD;
    LED1_Struct.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    LED1_Struct.GPIOx = GPIOD;
}
