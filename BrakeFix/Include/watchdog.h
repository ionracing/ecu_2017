#ifndef WATCHDOG_H
#define WATCHDOG_H

#include "stm32f4xx.h"

typedef enum
{
    UNKNOWN = 0,
    ASLEEP,
    ALIVE
} FLAG;

void watchdog_init(void);
void watchdog_pet(void);

#endif //WATCHDOG_H
