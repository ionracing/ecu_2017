#ifndef ADC_SIMPLE_H
#define ADC_SIMPLE_H

#include "stdint.h"
#include "ADC.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_tim.h"
#include "io_mapping_ECU.h"
#include "stm32f4xx_adc.h"

#define ADC_CH2 ADC_Channel_2
#define ADC_CH3 ADC_Channel_3
#define ADC_CH4 ADC_Channel_4
#define ADC_CH5 ADC_Channel_5
#define ADC_CH8 ADC_Channel_8
#define ADC_CH9 ADC_Channel_9
#define ADC_CH14 ADC_Channel_14

#define ADC_ALL_CHANNELS (ADC_CH2 | ADC_CH3 | ADC_CH4 | ADC_CH5 | ADC_CH8 | ADC_CH9 | ADC_CH14)
#define ADC_SAMPLING_FREQUENCY 1000 //Hz

void ADC_SIMPLE_init(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
uint16_t ADC_SIMPLE_readChannel(uint8_t channel);

#endif //ADC_SIMPLE_H
