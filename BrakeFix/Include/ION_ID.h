#ifndef ION_ID_H
#define ION_ID_H

#define BTN_STOP            (uint16_t)0x64
#define BTN_START           (uint16_t)0x65
#define BTN_MODE            (uint16_t)0x66
#define BTN_LOG             (uint16_t)0x67

#define IMU_RATE            (uint16_t)0x68
#define IMU_ACC_X           (uint16_t)0x69
#define IMU_ACC_Y           (uint16_t)0x6A
#define IMU_ACC_Z           (uint16_t)0x6B
#define IMU_TEMP            (uint16_t)0x6C

#define WATCHDOG            (uint16_t)0x6D

#define GPS_TIME_LSB        (uint16_t)0x70
#define GPS_TIME_MSB        (uint16_t)0x71
#define GPS_VALIDITY        (uint16_t)0x72
#define GPS_LATITUDE        (uint16_t)0x73
#define GPS_LONGITUDE       (uint16_t)0x74
#define GPS_SPEED           (uint16_t)0x75
#define GPS_COURSE          (uint16_t)0x76

#define ADC_BPS1            (uint16_t)0x7A
#define ADC_BPS2            (uint16_t)0x7B
#define ADC_AIRTEMP         (uint16_t)0x7C
#define ADC_APPS1           (uint16_t)0x7D
#define ADC_APPS2           (uint16_t)0x7E
#define ADC_BPPS            (uint16_t)0x7F
#define ADC_STEERING        (uint16_t)0x80
#define ADC_MISMATCH        (uint16_t)0x81
#define ADC_FLOW            (uint16_t)0x82

#define DASH_7_SEG          (uint16_t)0x190
#define DASH_LEDS           (uint16_t)0x191
#define MC_ADDRESS          (uint16_t)0x210
#define MC_THROTTLE         (uint16_t)0x90

//PARAM
//#define SOMETHING           (uint16_t)0x8A
#define TRACTION_CONTROL    (uint16_t)0x8B
#endif
