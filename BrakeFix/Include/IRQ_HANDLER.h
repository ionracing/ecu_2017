/* Include guard ---------------------------------- */
#ifndef IRQ_HANDLER_H
#define IRQ_HANDLER_H

/* Includes ---------------------------------------- */
#include "stm32f4xx_usart.h"
#include "CAN.h"
#include "BTN.h"
#include "LED.h"


extern volatile uint8_t MODE_FLAG;
extern volatile uint8_t SD_FLAG;


#endif //IRQ_HANDLER_H
