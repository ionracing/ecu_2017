#include "IO_Mapping_ECU.h"
#include "LED.h"
#include "BTN.h"
#include "IO.h"
#include "ADC_SIMPLE.h"
#include "CAN.h"
#include "systick.h"
#include "ION_ID.h"
#include "Segments.h"
#include "watchdog.h"
#include "IRQ_Handler.h"

#define APPS_THRESHOLD  0x420
#define BPS_THRESHOLD   0x500
#define BRAKE_THRESHOLD 0x200
#define APPS_SEPERATOR  0x570
#define BPS_SEPERATOR   0x300
#define APPS_GRADE      0x9F

void initialize(void);
void logSensors(void);
void standbyMode(void);
void pedalProximityCheck(void);
void pedalConcurrencyCheck(void);
void driveMode(void);
void stopBtn(void);
void brakeLight(void);
void sendTrottle(uint16_t torque);

uint16_t airTemp_Val = 0;
uint16_t steering_Val = 0;
uint16_t APPS1_Val = 0;
uint16_t APPS2_Val = 0;
uint16_t BPS1_Val = 0;
uint16_t BPS2_Val = 0;
uint16_t BPPS_Val = 0;
uint8_t START_btnStatus = BTN_LOW;
uint8_t STOP_btnStatus = BTN_LOW;
uint16_t pedalStartValue = 0x3EA;
uint16_t pedalSentage = 0;
uint8_t dash[8] = {0};
uint8_t start[8] = {0x51, 0x02, 0x00};
uint8_t stop[8] = {0x51, 0x04, 0x00};
uint8_t null[8] = {0x3F, 0x00, 0x3F, 0x00, 0x3F, 0x00, 0x3F, 0x00};
uint8_t startMsg[8] = {0x6D, 0x00, 0x78, 0x00, 0x77, 0x00, 0x33, 0x00};
uint8_t stopMsg[8] = {0x6D, 0x00, 0x78, 0x00, 0x3F, 0x00, 0x73, 0x00};
uint8_t dashLeds = 0;

/* Main code */
int main(void)
{
    SystemCoreClockUpdate();
    initialize();
    
	while(1)
	{
        if(LOGDATA_FLAG == SET)
        {
            LOGDATA_FLAG = RESET;
            logSensors();
        }
        
        if(START_btnStatus == BTN_LOW)
            standbyMode();
        else
            driveMode();
        
        
        if(STOP_btnStatus == BTN_LOW)
            stopBtn();
        
        
        if(BRAKE_FLAG == SET)
        {
            BRAKE_FLAG = RESET;
            brakeLight();
        }
        
        if(clk1000ms == CLK_COMPLETE)
            clk1000ms = CLK_RESET;
        if(clk100ms == CLK_COMPLETE)
            clk100ms = CLK_RESET;
        
        // ---WATCHDOG--- //
        //watchdog_pet();
    }
}

void initialize()
{
    LED_Initialise(LED1); //LED
    LED_Initialise(LED2); //LED
    LED_Initialise(LED3); //LED
    LED_Initialise(LED4); //LED
    
    BTN_Initialise(START); //BTN
    //BTN_Initialise(STOP); //BTN
    
    IO_initPinOut(BRAKE_GPIO, BRAKE_PIN); // BRAKELIGHT
    IO_initPinOut(RTDS_GPIO, RTDS_PIN); //KERS
    IO_initPinOut(FRG_RUN_GPIO, FRG_RUN_PIN);
    IO_initPinOut(RFE_GPIO, RFE_PIN);
    IO_initPinOut(RELAY_GPIO, RELAY_PIN);
    GPIO_SetBits(RELAY_GPIO, RELAY_PIN);
    
    ADC_SIMPLE_init(APPS1_GPIO, APPS1_PIN);
    ADC_SIMPLE_init(APPS2_GPIO, APPS2_PIN);
    ADC_SIMPLE_init(BPS1_GPIO, BPS1_PIN);
    ADC_SIMPLE_init(BPS2_GPIO, BPS2_PIN);
    ADC_SIMPLE_init(BPPS_GPIO, BPPS_PIN);
    ADC_SIMPLE_init(AIRTEMP_GPIO, AIRTEMP_PIN);
    ADC_SIMPLE_init(STEERING_GPIO, STEERING_PIN);
    
    IO_initPinOut(GPIOC, GPIO_Pin_10); // CAN enable pin
    GPIO_SetBits(GPIOC, GPIO_Pin_10);
    CAN_Initialise(); //CAN
    systick_init(); //Systick
    //watchdog_init(); //Watchdog
}

void logSensors()
{
    APPS1_Val = ADC_SIMPLE_readChannel(APPS1_CHANNEL);
    APPS2_Val = ADC_SIMPLE_readChannel(APPS2_CHANNEL);
    BPS1_Val = ADC_SIMPLE_readChannel(BPS1_CHANNEL);
    BPS2_Val = ADC_SIMPLE_readChannel(BPS2_CHANNEL);
    BPPS_Val = ADC_SIMPLE_readChannel(BPPS_CHANNEL);
    airTemp_Val = ADC_SIMPLE_readChannel(AIRTEMP_CHANNEL);
    steering_Val = ADC_SIMPLE_readChannel(STEERING_CHANNEL);
    
    CAN_Write_16_bit(CAN1, ADC_APPS1, APPS1_Val);
    CAN_Write_16_bit(CAN1, ADC_APPS2, APPS2_Val);
    CAN_Write_16_bit(CAN1, ADC_BPS1, BPS1_Val);
    CAN_Write_16_bit(CAN1, ADC_BPS2, BPS2_Val);
    CAN_Write_16_bit(CAN1, ADC_BPPS, BPPS_Val);
    CAN_Write_16_bit(CAN1, ADC_AIRTEMP, airTemp_Val);
    CAN_Write_16_bit(CAN1, ADC_STEERING, steering_Val);
    
    dashLeds = 0;
    if(BPS2_Val > BPS_THRESHOLD)
        dashLeds |= 1;
    if(APPS1_Val >= APPS_THRESHOLD)
        dashLeds |= 2;
    if(APPS_GRADE < (APPS1_Val > APPS2_Val ? APPS1_Val - APPS2_Val : APPS2_Val - APPS1_Val))
        dashLeds |= 4;
    if((APPS1_Val > APPS_THRESHOLD && BPS2_Val > BPS_SEPERATOR) || (BPS2_Val > APPS_THRESHOLD && APPS1_Val > APPS_SEPERATOR))
        dashLeds |= 8;
    
    CAN_Write_8_bit(CAN1, DASH_LEDS, dashLeds);
    
    if(START_btnStatus == BTN_HIGH)
    {
        //---SEND TO DASH---//
        if(APPS1_Val >= APPS_THRESHOLD)
        {
            pedalSentage = ((APPS1_Val-APPS_THRESHOLD) * 100) / (0xFFF-APPS_THRESHOLD);
            dash[6] = alphafonttable[(pedalSentage % 10) + '0'];
            dash[7] = (alphafonttable[(pedalSentage % 10) + '0'] >> 8);
            pedalSentage =  pedalSentage / 10;
            dash[4] = alphafonttable[(pedalSentage % 10) + '0'];
            dash[5] = (alphafonttable[(pedalSentage % 10) + '0'] >> 8);
            pedalSentage = pedalSentage / 10;
            dash[2] = alphafonttable[(pedalSentage % 10) + '0'];
            dash[3] = (alphafonttable[(pedalSentage % 10) + '0'] >> 8);
            dash[0] = 0x3F;
            dash[1] = 0x00;
            CAN_Write_8(CAN1, DASH_7_SEG, 8, dash);
        }
        else
        {
            CAN_Write_8(CAN1, DASH_7_SEG, 8, null);
        }
    }
}

void standbyMode()
{
    if(LED_FLAG == SET)
    {
        LED_FLAG = RESET;
        LEDx_Toggle(LED1);
    }
    START_btnStatus = BTN_Status(BTN_GPIOx(START), BTN_GPIO_Pin(START), Bit_SET); //Check StartButton
    if(START_btnStatus == BTN_HIGH)
    {
        if(APPS1_Val < APPS_THRESHOLD && BPS2_Val > BPS_THRESHOLD)
        {
            //STOP_btnStatus = BTN_LOW;
            
            CAN_Write_8(CAN_CANx, DASH_7_SEG, 8, startMsg);
            
            GPIO_SetBits(FRG_RUN_GPIO, RFE_PIN);
            GPIO_SetBits(RFE_GPIO, RFE_PIN);
            
            CAN_Write_8(CAN_CANx, MC_ADDRESS, 3, stop);
            clk1000ms = CLK_RESET;
            while(clk1000ms != CLK_COMPLETE);
            CAN_Write_8(CAN_CANx, MC_ADDRESS, 3, start);
    
            // ---KERS--- //
            GPIO_SetBits(RTDS_GPIO, RTDS_PIN);
            clk2000ms = CLK_RESET;
            while(clk2000ms != CLK_COMPLETE);
            GPIO_ResetBits(RTDS_GPIO, RTDS_PIN);
        }
        else
        {
            START_btnStatus = BTN_LOW;
        }
    }
}

void pedalProximityCheck()
{
    if(APPS_GRADE < (APPS1_Val > APPS2_Val ? APPS1_Val - APPS2_Val : APPS2_Val - APPS1_Val))
        {
            GPIO_ResetBits(RFE_GPIO, RFE_PIN);
            while(APPS_GRADE < (APPS1_Val > APPS2_Val ? APPS1_Val - APPS2_Val : APPS2_Val - APPS1_Val))
            {
                APPS1_Val = ADC_SIMPLE_readChannel(APPS1_CHANNEL);
                APPS2_Val = ADC_SIMPLE_readChannel(APPS2_CHANNEL);
                clk100ms = CLK_RESET;
                while(clk100ms != CLK_COMPLETE);
            }
            GPIO_SetBits(RFE_GPIO, RFE_PIN);
        }
}

void pedalConcurrencyCheck()
{
    if((APPS1_Val > APPS_THRESHOLD && BPS2_Val > BPS_SEPERATOR) || (BPS2_Val > APPS_THRESHOLD && APPS1_Val > APPS_SEPERATOR))
        {
            GPIO_ResetBits(RFE_GPIO, RFE_PIN);
            while((APPS1_Val > APPS_SEPERATOR && BPS2_Val > BPS_SEPERATOR) || (BPS2_Val > BPS_THRESHOLD && APPS1_Val > APPS_SEPERATOR))
            {
                APPS1_Val = ADC_SIMPLE_readChannel(APPS1_CHANNEL);
                BPS2_Val = ADC_SIMPLE_readChannel(BPS2_CHANNEL);
                clk100ms = CLK_RESET;
                while(clk100ms != CLK_COMPLETE);
            }
            GPIO_SetBits(RFE_GPIO, RFE_PIN);
        }
}

void driveMode()
{
    if(LED_FLAG == SET)
    {
        LED_FLAG = RESET;
        LEDx_Toggle(LED2);
    }
    if(GAS_FLAG == SET)
    {
        GAS_FLAG = RESET;
        
        pedalProximityCheck();
        pedalConcurrencyCheck();
        
        if(APPS1_Val > APPS_THRESHOLD)
          sendTrottle(APPS1_Val);
        else
            sendTrottle(0);
    }
}

void stopBtn()
{
    STOP_btnStatus = BTN_Status(BTN_GPIOx(STOP), BTN_GPIO_Pin(STOP), Bit_RESET);
            
    if(STOP_btnStatus == BTN_HIGH)
    {
        START_btnStatus = BTN_LOW;
        
        CAN_Write_8(CAN_CANx, DASH_7_SEG, 8, stopMsg);
        
        GPIO_ResetBits(FRG_RUN_GPIO, RFE_PIN);
        GPIO_ResetBits(RFE_GPIO, RFE_PIN);
        
        STOP_btnStatus = BTN_LOW;
    }
}

void brakeLight()
{
    if(BPS2_Val > BRAKE_THRESHOLD)
        GPIO_SetBits(BRAKE_GPIO, BRAKE_PIN);
    else
        GPIO_ResetBits(BRAKE_GPIO, BRAKE_PIN);
}

CanTxMsg TQ;
void sendTrottle(uint16_t torque)
{
    if(torque > 4095)
    {
        torque = 4095;
    }
    int16_t T = (int16_t)(torque << 3);
    
    if(T < 0)
    {
        T = 0;
    }
    T = (T*(-1));
    
    TQ.StdId = MC_ADDRESS;
	TQ.IDE = 0;
	TQ.RTR = 0;
	TQ.DLC = 3;
    
    TQ.Data[0] = MC_THROTTLE;
    TQ.Data[1] = (0xFF & T);
    TQ.Data[2] = (0xFF & (T >> 8));
    
    while(CAN_Transmit(CAN_CANx, &TQ) == CAN_TxStatus_NoMailBox)
    {
        LEDx_On(LED4);
    }
    LEDx_Off(LED4);
}
