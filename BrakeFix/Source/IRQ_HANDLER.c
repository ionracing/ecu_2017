#include "IRQ_HANDLER.h"

void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        /* Do your stuff when PD0 is changed */
        //STARTUP_MODE = STARTUP_MODE == 0 ? 1 : 0;
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}


volatile uint8_t MODE_FLAG = RESET;
volatile uint8_t SD_FLAG = RESET;
void CAN1_RX0_IRQHandler (void)
{
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
        CanRxMsg msg;
        CAN_Receive(CAN1, 0, &msg);
        
        if(msg.StdId == 0x66) // MODE
        {
            MODE_FLAG = SET;
        }
        
        if(msg.StdId == 0x67) // SD
        {
            SD_FLAG = SET;
        }
	}
	__enable_irq();
}
