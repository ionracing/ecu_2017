# READ ME
This is the reposetory for the ecu_light_regulator chip that will be in the ECU.
This project is a backup for ecu_regulator in case that fails.

The job for this chip is to control the car and regulate the power to give to the motor. This is the main chip on the ECU and is the most critical to work.
One of the main task of this chip is to calculate and regulate the power the motor will have to give to get the best speed with not to much wheel spin. The simplest solution to this is to give the throttle position as the power, but this is giving the driver no help and he needs to do all the regulating himself.
The more innputs we get to control the wheel spin the better the car will drive.
The chip also controlls a lot of the safety controlls that are needed to fulfill the rules. This includes the brake and pedal, brakelight, RTDS and a few error handelings.

### Regulator
The more inputs we can calculate together the more precicely we can get to how much power we want to give to the car and get the wanted wheel spin. We get the most out of logging the wheel turns on all 4 wheels. This makes it so that we can mesure how much spin we get on the different wheels, especially the different on the front wheels and the back wheels. Then we can also include the speed of the car, and the surface. Then if possible we can include gps, accelorometer and gyrosocope to make it even more precise.

### Rules
The different rules that this chip controlls are:
* Not be able to start the car if the brake is not pressed down.
* The car stops if you press the gas pedal and brake pedal at the same time.
* Stop the car if the brake is pressed too far.
* Light up the brake light when pressing the brake.
* 


## Sketch
![alt text](https://bytebucket.org/ionracing/ecu_light_regulator/raw/2004163e479ab69a71032596eb35c7d0f7229d2c/Photos/skisse1.jpg "Skisse 1")

## Plan Data
1. Sensors (I^2C/SPI/ADC)
    * Get data
    * Transform data
    * Packet data
2. Telemetry (SPI)
    * Send
    * Recieve
    * Change parameters
3. SD-Card (SPI)
    * Save data
    * Read data
    * Save parameters
    * Read parameters
4. Change
5. GPS (USART)
6. IMU (SPI)
7. GPIO

## Plan Elektro
1. Kontroll
2. Pådrag
3. Regler
4. Tester o.l
