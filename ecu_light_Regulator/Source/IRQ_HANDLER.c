#include "IRQ_HANDLER.h"

uint8_t status = 0xEE;
CanTxMsg canMsg;
uint8_t preAmbl = 0x00;
uint8_t idLsb = 0;
uint16_t idCan = 0;

void USART3_IRQHandler(void)
{
    uint8_t data;
	// check if the USART3 receive interrupt flag was set
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
        data = USART_ReceiveData(USART3);
        
        if (status < 2) //add ID check for error handeling
        {
            if(status == 0)
            {
                idLsb = data;
            }
            else
            {
                idCan = (data << 8) | idLsb;
                if(idCan < 700 || idCan >= 800) //Wrong ID send error to RPI.
                {
                    USART_SendData(USART3, 0x20);
                    status = 0xEE;
                }
                canMsg.StdId = idCan;
                canMsg.IDE = CAN_Id_Standard;
                canMsg.RTR = CAN_RTR_Data;
                canMsg.DLC = 4;
            }
            status++;
        }
        else if (status >= 2 && status < 6) // Add the 4 bytes of data to the CAN data frame.
        {
            canMsg.Data[status-2] = data;
            status++;
        }
        else if (status == 6) // Send to RPI usaing CAN1.
        {
            CAN_Transmit(CAN1, &canMsg);
            status++;
        }
        else
        {
            if(data == 0xFF)
            {
                if(preAmbl == 0xFF)
                {
                    status = 0;
                    preAmbl = 0x00;
                }
                else
                {
                    preAmbl = data;
                }
            }
        }
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
	}
}

uint8_t RxByte = 0;
/*
void USART1_IRQHandler(void){
    GPIO_SetBits(GPIOD, GPIO_Pin_14);
	// check if the USART1 receive interrupt flag was set
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(USART1,USART_IT_RXNE);
        USART_Read(USART1);
	}
}
*/
void USART2_IRQHandler(void){
	// check if the USART2 receive interrupt flag was set
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
        USART_ClearITPendingBit(USART2,USART_IT_RXNE);
    }
}

void EXTI0_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
        /* Do your stuff when PD0 is changed */
        //STARTUP_MODE = STARTUP_MODE == 0 ? 1 : 0;
        
        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}

void CAN2_RX0_IRQHandler (void)
{
	__disable_irq();
	if(CAN2->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
		//CAN_Receive(CAN2, CAN_FIFO0, &msgRx);
        GPIO_SetBits(GPIOD, GPIO_Pin_13);
        //Add msgRx to a buffer and prosess it another place.
        //if(*msgRx.Data == 0xFF)
        {
            GPIO_SetBits(GPIOD, GPIO_Pin_14);
        }
	}
	__enable_irq();
}
