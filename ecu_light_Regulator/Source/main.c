//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

//******************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
//******************************************************************************

//******************************************************************************
#include "global.h"
#include "IO_Mapping.h"
#include "BTN.h"
#include "LED.h"
#include "ADC.h"
#include "TIM.h"
#include "Buffer.h"
//******************************************************************************

uint32_t *ptrValues[100];

void vLedBlink(void *pvParameters);
void vTT_Task(void *pvParameters);
void vSD_Task(void *pvParameters);
void vGPS_Task(void *pvParameters);

#define STACK_SIZE_MIN	1000	/* usStackDepth	- the stack size DEFINED IN WORDS.*/

/* ---GLOBALS--- */
uint32_t relativeTime = 0;
sensorPackage pkgBuffer[100];

//******************************************************************************
int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
	
    /* ---INITS--- */
    Buffer_Init(pkgBuffer, 100);
    LED_Initialise(RED);
    TIM_initialise();
    BTN_Initialise(START);
    
    /* ---Wait for start signal--- */
    while(BTN_Status(GPIOB, GPIO_Pin_2) == BTN_LOW);
    
    /* ---Super loop--- */
    while(1)
    {
        /* ---log stuff--- */
        // ADC
        /* ---send stuff--- */
        // Telemetry
        // SD_Card
    }
}
//******************************************************************************
