#include "COMLine.h"

/*---COMLine---*/
/* 
Is the lines that connect collector chip and regulator chip.
We use these to comunicate and send speed and damper movements.
We want only to send when we get a high pin on the line. I am
not sure how I want to do with all the data that is read inbetween.
*/


void COMLine_initialise()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
  
    /*---Clock---*/
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /*---COMLines out---*/
    GPIO_InitStruct.GPIO_Pin = COMLines_out | DATA_READY;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(COM_GPIO, &GPIO_InitStruct);
    
    /*---COMLines in---*/
    GPIO_InitStruct.GPIO_Pin = IRQ_Pins | DATA_ACCEPTED;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(COM_GPIO, &GPIO_InitStruct);
    
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource12);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource13);

	EXTI_InitStruct.EXTI_Line = EXTI_Line12 | EXTI_Line13; 
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
}

/*---Send---*/
void COMLine_Send(uint16_t data)
{
    COM_GPIO->ODR |= (0xFFF & data);
    GPIO_SetBits(COM_GPIO, DATA_READY);
    while(GPIO_ReadInputDataBit(COM_GPIO, DATA_ACCEPTED));
    GPIO_ResetBits(COM_GPIO, DATA_READY);
}

void EXTI15_10_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line14) != RESET) {
        
        // Her sender jeg den nyeste dempe verdien
        //COMLine_Send(dempe);
        
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
    if (EXTI_GetITStatus(EXTI_Line15) != RESET) {
        
        // Her sender jeg den nyeste speed verdien
        //COMLine_Send(speed);
        
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}
