# READ ME
ECU projects

## Sketch
![alt text](https://bytebucket.org/ionracing/ecu_regulator/raw/7aa5d1f93805e59c0c99d79bf8d4abaed55ad219/Photos/skisse1.jpg "Skisse 1")

## Plan Data
1. Sensors (I^2C/SPI/ADC)
    * Get data
    * Transform data
    * Packet data
2. Telemetry (SPI)
    * Send
    * Recieve
    * Change parameters
3. SD-Card (SPI)
    * Save data
    * Read data
    * Save parameters
    * Read parameters
4. Change
5. GPS (USART)
6. IMU (SPI)
7. GPIO

## Plan Elektro
1. Kontroll
2. Pådrag
3. Regler
4. Tester o.l
