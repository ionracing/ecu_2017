#include "ADC_SIMPLE.h"

/* Single channel configuration. */
void ADC_SIMPLE_init(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{	
	ADC_InitTypeDef ADC_initStruct;
	ADC_CommonInitTypeDef ADC_commonInitStruct;
	GPIO_InitTypeDef GPIO_initStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_AHB1PeriphClockCmd(GPIOx == GPIOA ? RCC_AHB1Periph_GPIOA : RCC_AHB1Periph_GPIOB, ENABLE);
    
	ADC_StructInit(&ADC_initStruct);
	ADC_CommonStructInit(&ADC_commonInitStruct);
	GPIO_StructInit(&GPIO_initStruct);
	
    /* Configurate GPIO */
    GPIO_initStruct.GPIO_Pin = GPIO_Pin;
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOx, &GPIO_initStruct);
    
	/* Configure ADC in independent mode */
	ADC_commonInitStruct.ADC_DMAAccessMode    = ADC_DMAAccessMode_Disabled;
	ADC_commonInitStruct.ADC_Mode             = ADC_Mode_Independent;
	ADC_commonInitStruct.ADC_Prescaler        = ADC_Prescaler_Div2;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);
	
	/* Configure the ADC: 12-bit single conversion */
	ADC_initStruct.ADC_Resolution           = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode         = DISABLE;
	ADC_initStruct.ADC_ContinuousConvMode   = DISABLE;
	ADC_initStruct.ADC_ExternalTrigConvEdge = 0;
	ADC_initStruct.ADC_ExternalTrigConv     = 0;
	ADC_initStruct.ADC_DataAlign            = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion      = 1;
	ADC_Init(ADC1, &ADC_initStruct);
	
	ADC_Cmd(ADC1, ENABLE);
}

uint16_t ADC_SIMPLE_readChannel(uint8_t channel)
{
	ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_480Cycles);
	ADC_SoftwareStartConv(ADC1);
	while((ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET));
	return ADC_GetConversionValue(ADC1);
}
