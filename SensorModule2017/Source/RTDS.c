#include "RTDS.h"

RTDS_Struct *Str;

void RTDS_initialise(RTDS_Struct *Struct)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
 
    Str = Struct;
    
    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(Struct->RCC_GPIO, ENABLE);

    /* Configure the GPIO_LED pins */
    GPIO_InitStructure.GPIO_Pin = Struct->GPIO_Pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(Struct->GPIOx, &GPIO_InitStructure);
}

void RTDS_Set(uint8_t STATUS)
{
    if(STATUS == 0)
    {
        GPIO_ResetBits(Str->GPIOx, Str->GPIO_Pin);
    }
    else if(STATUS == 1)
    {
        GPIO_SetBits(Str->GPIOx, Str->GPIO_Pin);
    }
    else
    {
        while(1);
    }
}
