#ifndef IO_MAPPING_ECU_REGULATOR_H
#define IO_MAPPING_ECU_REGULATOR_H

/* LEDs ---------------------------------------------------------*/
typedef enum
{
    LED1 = 0,
    LED2,
    LED3,
    LED4
} LEDS;
/*
#define LED_GPIO_RCC(LED)   ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? RCC_AHB1Periph_GPIOD : 0)
#define LED_GPIO_PIN(LED)   ((LED == LED1) ? GPIO_Pin_0 : (LED == LED2) ? GPIO_Pin_2 : (LED == LED3) ? GPIO_Pin_1 : (LED == LED4 ? GPIO_Pin_3 : 0))
#define LED_GPIO(LED)       ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? GPIOD : GPIOD)
*/
#define LED_GPIO_RCC(LEDx)  (LEDx == LED1) ? RCC_AHB1Periph_GPIOB : \
                            (LEDx == LED2) ? RCC_AHB1Periph_GPIOB : 666
#define LED_GPIO_PIN(LEDx)  (LEDx == LED1) ? GPIO_Pin_12 : \
                            (LEDx == LED2) ? GPIO_Pin_13 : 666
#define LED_GPIO(LEDx)      (LEDx == LED1) ? GPIOB : \
                            (LEDx == LED2) ? GPIOB : GPIOG

/* PINs ---------------------------------------------------------*/
// BRAKE
#define BRAKE_GPIO  GPIOD
#define BRAKE_PIN   GPIO_Pin_5

// RTDS
#define RTDS_GPIO GPIOD
#define RTDS_PIN GPIO_Pin_6

/* ADC ---------------------------------------------------------*/
#define BP_PIN          GPIO_Pin_0
#define BP_CHANNEL      ADC_Channel_0
#define GP_PIN          GPIO_Pin_1
#define GP_CHANNEL      ADC_Channel_1

/* BTNs ---------------------------------------------------------*/

typedef enum
{
    START = 0,
    STOP
} BUTTONS;
#define BTN_GPIO_RCC(BTN)               (BTN == START   ? RCC_AHB1Periph_GPIOB : \
                                        (BTN == STOP    ? RCC_AHB1Periph_GPIOC : 0))
#define BTN_GPIO_Pin(BTN)               (BTN == START   ? GPIO_Pin_12 : \
                                        (BTN == STOP    ? GPIO_Pin_6 : 0))
#define BTN_GPIOx(BTN)                  (BTN == START   ? GPIOB : \
                                        (BTN == STOP    ? GPIOC : (0 == 0 ? GPIOA : GPIOA)))
#define BTN_EXTI_PortSource(BTN)        (BTN == START   ? EXTI_PortSourceGPIOB : \
                                        (BTN == STOP    ? EXTI_PortSourceGPIOC : 0))
#define BTN_EXTI_PinSource(BTN)         (BTN == START   ? EXTI_PinSource12 : \
                                        (BTN == STOP    ? EXTI_PinSource6 : 0))
#define BTN_EXTI_Linex(BTN)             (BTN == START   ? EXTI_Line12 : \
                                        (BTN == STOP    ? EXTI_Line6 : 0))
#define BTN_NVIC_Enable(BTN)            (BTN == START   ? ENABLE : \
                                        (BTN == STOP    ? ENABLE : (0 == 0 ? DISABLE : DISABLE)))
#define BTN_NVIC_IRQChannel(BTN)        (BTN == START   ? EXTI15_10_IRQn : \
                                        (BTN == STOP    ? EXTI9_5_IRQn : 0))
#define BTN_NVIC_IRQChannelPP(BTN)      (BTN == START   ? 0 : \
                                        (BTN == STOP    ? 0 : 0))
#define BTN_NVIC_IRQChannelSP(BTN)      (BTN == START   ? 1 : \
                                        (BTN == STOP    ? 1 : 0))

/* ---CAN--- */
#define CAN_GPIO_RCC                RCC_AHB1Periph_GPIOB
#define CAN_GPIO_MODE               GPIO_Mode_AF
#define CAN_GPIO_OTYPE              GPIO_OType_PP
#define CAN_GPIO_PIN_RX             GPIO_Pin_8
#define CAN_GPIO_PIN_TX             GPIO_Pin_9
#define CAN_GPIO_PUPD               GPIO_PuPd_UP
#define CAN_GPIO_SPEED              GPIO_Speed_100MHz
#define CAN_GPIO_GPIOX              GPIOB
#define CAN_GPIO_PinSource_RX       GPIO_PinSource8
#define CAN_GPIO_PinSource_TX       GPIO_PinSource9
#define CAN_GPIO_AF_CAN             GPIO_AF_CAN1
#define CAN_CAN_RCC                 RCC_APB1Periph_CAN1
#define CAN_CANx                    CAN1
#define CAN_NVIC_IRQChannel         CAN1_RX0_IRQn
#define CAN_NVIC_IRQChannelPP       0
#define CAN_NVIC_IRQChannelSP       1
#define CAN_NVIC_IRQChannelCmd      ENABLE

#endif //IO_MAPPING_SM_H
