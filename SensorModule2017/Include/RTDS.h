#ifndef RTDS_H
#define RTDS_H

#include "stdint.h"
#include "stm32f4xx_gpio.h"

typedef struct {
    uint32_t            RCC_GPIO;
    uint16_t            GPIO_Pin;
    GPIO_TypeDef*       GPIOx;
} RTDS_Struct;

void RTDS_initialise(RTDS_Struct *Struct);

#endif //RTDS_H
