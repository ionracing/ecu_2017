#include "SD_CARD.h"

/*
SPI2(GPIOA):    -   Type:   -   SD CARD:
Pin4            -   CS      -   Pin2
Pin5            -   SCLK    -   Pin5
Pin6            -   MISO    -   Pin7
Pin7            -   MOSI    -   Pin3
*/
uint8_t SD_CARD_InitialiseCard()
{
    INT i = 0;
    
    SPI_initialise(SPI2);

    SPI2_ChipSelect(PIN_HIGH);
    for(i = 0;i < 16;i++)
        SPI_Write(SPI2, 0xFF);

    for(i = 0;i < 0xFFFF;i++);

    while(SD_CARD_Cmd(GO_IDLE_STATE, 0) != R1_IDLE_STATE); //CMD0

    uint32_t r = SD_CARD_Cmd(SEND_IF_COND, 0x1AA); //CMD8
    if(r == 0x1AA)
        return SD_CARD_InitialiseCardV2();
    else if(r == (R1_IDLE_STATE | R1_ILLEGAL_COMMAND))
        return SD_CARD_InitialiseCardV1();
    else
        return SDCARD_FAIL;
}

uint8_t SD_CARD_WriteRead(INT arg)
{
    SPI_Write(SPI2, arg);
    uint8_t test = SPI_Read(SPI2);
    return test;
}

uint8_t SD_CARD_InitialiseCardV1()
{
    uint8_t cmd;
    INT i = 0;
    
    if(SD_CARD_Cmd(SD_SEND_OP_COND, 0x40040000) <= 1) //ACMD41 - set to 3V, use 0x40200000 for 3V3
        cmd = SD_SEND_OP_COND;
    else
        cmd = SEND_OP_COND;
    
    for(i = 0; i < SD_COMMAND_TIMEOUT;i++)
    {
        if(SD_CARD_Cmd(cmd, 0) == 0) //CMD1 or ACMD41
        {
            // Set block length to 512 (CMD16)
            if(SD_CARD_Cmd(SET_BLOCKLEN, 512) != 0) //CMD16
                return SDCARD_FAIL;
            
            //Init: SEDCARD_V1
            if(cmd == SD_SEND_OP_COND)
                return SDCARD_V1;
            else
                return SDCARD_MMCV3;
        }
    }

    //Timeout waiting for v1.x card
    return SDCARD_FAIL;
}

uint8_t SD_CARD_InitialiseCardV2()
{
    INT i = 0;
    INT j = 0;

    for(i = 0;i < SD_COMMAND_TIMEOUT;i++)
    {
        for(j = 0;j < 0xFF;j++);
        if(SD_CARD_Cmd(SD_SEND_OP_COND, 0x40040000) == 0) //ACMD41 - set to 3V, use 0x40200000 for 3V3
        {
            uint32_t ocr = SD_CARD_Cmd(READ_OCR, 0); //CMD58
            return (ocr & 0x40000000) ? SDCARD_V2 | SDCARD_BLOCK : SDCARD_V2;
        }
    }

    //Timed out waiting for v2.x card
    return SDCARD_FAIL;
}

uint32_t SD_CARD_Cmd(INT cmd, INT arg)
{
    command_fields com;
    com.start_bit = 0;
    com.transmitter_bit = 1;
    com.index = cmd;
    com.argument = arg;
    if(cmd == GO_IDLE_STATE)
        com.crc = 0x4A;
    else if(cmd == SEND_IF_COND)
        com.crc = 0x43;
    else
        com.crc = 0x7F;
    com.end_bit = 1;
    
    if(cmd == SD_STATUS | cmd == SET_WR_BLOCK_ERASE_COUNT | cmd == SD_SEND_OP_COND) //ACMDx
        SD_CARD_Cmd(APP_CMD, 0); //CMD55

    SD_CARD_WriteCom(&com);

    if(cmd == SEND_IF_COND)
        return SD_CARD_RecieveR7();
    else if(cmd == READ_OCR)
        return SD_CARD_RecieveR3();
    else
        return SD_CARD_RecieveR1();
}

void SD_CARD_WriteCom(command_fields *com)
{

    SPI2_ChipSelect(PIN_LOW);
    SPI_Write(SPI2, 0xFF);
    SPI_Write(SPI2, (0xFF & ((com->start_bit << 7) | (com->transmitter_bit << 6) | com->index)));
    SPI_Write(SPI2, (0xFF & (com->argument >> 24)));
    SPI_Write(SPI2, (0xFF & (com->argument >> 16)));
    SPI_Write(SPI2, (0xFF & (com->argument >> 8)));
    SPI_Write(SPI2, (0xFF & com->argument));
    SPI_Write(SPI2, (0xFF & ((com->crc << 1) | com->end_bit)));
}

INT SD_CARD_Write(const BYTE *buffer, BYTE token)
{
    INT i = 0;
    SPI2_ChipSelect(PIN_LOW);
    
    while(SD_CARD_WriteRead(0xFF) != 0xFF);

    // indicate start of block
    SPI_Write(SPI2, token);

    if(token != 0xFD)
    {
        // write the data
        for(i = 0;i < 512;i++)
        {
            SPI_Write(SPI2, *buffer);
            buffer++;
        }

        // write the checksum
        SPI_Write(SPI2, 0xFF);
        SPI_Write(SPI2, 0xFF);

        // check the repsonse token
        uint8_t resp = 0x00;
        do
        {
            resp = SD_CARD_WriteRead(0xFF);
        }
        while(resp == 0x00);
        
        if((resp & 0x1F) != 0x05)
        {
            // wait for write to finish
            while(SD_CARD_WriteRead(0xFF) != 0xFF);
            
            SPI_Write(SPI2, 0xFF);
            SPI2_ChipSelect(PIN_HIGH);
            SPI_Write(SPI2, 0xFF);
            
            return SUCCESS;
        }
    }
    
    // wait for write to finish
    while(SD_CARD_WriteRead(0xFF) != 0xFF);

    SPI_Write(SPI2, 0xFF);
    SPI2_ChipSelect(PIN_HIGH);
    SPI_Write(SPI2, 0xFF);
    
    if(token == 0xFD)
        return SUCCESS;
    
    return ERROR;
}

INT SD_CARD_Read(BYTE *buffer, INT length)
{
    INT i = 0;
    SPI2_ChipSelect(PIN_LOW);

    for(i = 0; i < SD_COMMAND_TIMEOUT;i++)
    {
        // read until start byte (0xFF)
        if(SD_CARD_WriteRead(0xFF) == 0xFE)
        {
            // read data
            for(i = 0;i < length;i++)
                buffer[i] = SD_CARD_WriteRead(0xFF);
            
            SPI_Write(SPI2, 0xFF); // checksum
            SPI_Write(SPI2, 0xFF);

            SPI_Write(SPI2, 0xFF);
            SPI2_ChipSelect(PIN_HIGH);
            SPI_Write(SPI2, 0xFF);
            return SUCCESS;
        }
    }
    
    return ERROR;
}

uint8_t SD_CARD_RecieveR1()
{
    INT i;
    uint8_t response = 0xFF;
    for(i = 0;i < SD_COMMAND_TIMEOUT;i++)
    {
        response = SD_CARD_WriteRead(0xFF);
        if((response == 0x00) || (response == 0x01))
        {
            SPI_Write(SPI2, 0xFF);
            SPI2_ChipSelect(PIN_HIGH);
            SPI_Write(SPI2, 0xFF);
            return response;
        }
    }
    SPI_Write(SPI2, 0xFF);
    SPI2_ChipSelect(PIN_HIGH);
    SPI_Write(SPI2, 0xFF);
    return 0xFF;
}

uint32_t SD_CARD_RecieveR7()
{
    INT i = 0, j = 0;
    for(i = 0;i < (SD_COMMAND_TIMEOUT * 1000);i++)
    {
        uint8_t response[5];
        response[0] = SD_CARD_WriteRead(0xFF);
        if(!(response[0] & 0x80))
        {
                for(j = 1;j < 5;j++)
                {
                    response[j] = SD_CARD_WriteRead(0xFF);
                }
                SPI_Write(SPI2, 0xFF);
                SPI2_ChipSelect(PIN_HIGH);
                SPI_Write(SPI2, 0xFF);
                return ((response[1] << 24) | (response[2] << 16) | (response[3] << 8) | response[4]);
        }
    }
    SPI_Write(SPI2, 0xFF);
    SPI2_ChipSelect(PIN_HIGH);
    SPI_Write(SPI2, 0xFF);
    return 0xFFFFFFFF; // timeout
}

uint32_t SD_CARD_RecieveR3()
{
    uint32_t ocr = 0;
    INT response;
    for(int i=0; i < SD_COMMAND_TIMEOUT; i++)
    {
        response = SD_CARD_WriteRead(0xFF);
        if(!(response & 0x80))
        {
            ocr = SD_CARD_WriteRead(0xFF) << 24;
            ocr |= SD_CARD_WriteRead(0xFF) << 16;
            ocr |= SD_CARD_WriteRead(0xFF) << 8;
            ocr |= SD_CARD_WriteRead(0xFF);
            SPI_Write(SPI2, 0xFF);
            SPI2_ChipSelect(PIN_HIGH);
            SPI_Write(SPI2, 0xFF);
            return ocr;
        }
    }
    SPI_Write(SPI2, 0xFF);
    SPI2_ChipSelect(PIN_HIGH);
    SPI_Write(SPI2, 0xFF);
    return 0xFFFFFFFF; // timeout
}

INT SD_CARD_InitialiseDisk()
{
    if(SD_CARD_InitialiseCard() == SDCARD_FAIL)
        return SDCARD_FAIL;

    SPI_SetSpeed(SPI2, SPI_SPEED_1300KHz);
    return SUCCESS;
}

static uint8_t FILEOPEN = 0;
uint8_t SD_CARD_OpenFile(FIL *fil, const char *fileName, unsigned char mode)
{
    if(!FILEOPEN)
    {
        if(f_open(fil, fileName, mode) == FR_OK)
            FILEOPEN = 1;
    }
    return FILEOPEN;
}

uint8_t SD_CARD_CloseFile(FIL *fil)
{
    if(FILEOPEN)
    {
        if(f_close(fil) == FR_OK)
            FILEOPEN = 0;
    }
    return FILEOPEN;
}

static uint8_t MOUNTED = 0;
uint8_t SD_CARD_Mount(FATFS *FatFs)
{
    if(!MOUNTED)
    {
        if(f_mount(FatFs, "", 1) == FR_OK)
            MOUNTED = 1;
    }
    return MOUNTED;
}

uint8_t SD_CARD_UnMount()
{
    if(MOUNTED)
    {
        if(f_mount(0, "", 1) == FR_OK)
            MOUNTED = 0;
    }
    return MOUNTED;
}

void SD_CARD_writeInt(FIL *fil, uint32_t data, uint8_t btw)
{
    UINT* bw;
    for(int i = (btw-1);i >= 0;i--)
        f_write(fil, (const void*)(0xFF & (data >> (i*8))), 1, bw);
}

void SD_CARD_writeArray(FIL *fil, char data[])
{
    UINT* bw;
    
    uint8_t i = 0;
    do
    {
        f_write(fil, (const void*)(0xFF & data[i]), 1, bw);
        i++;
    }
    while(data[i-1] != '\n');
}
