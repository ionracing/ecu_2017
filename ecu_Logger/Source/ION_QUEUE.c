#include "ION_QUEUE.h"

xQueueHandle TT_Queue;
xQueueHandle SD_Queue;
xQueueHandle GPS_Queue;

void ION_QUEUE_Initialise(uint16_t size)
{
    TT_Queue = xQueueCreate(size, sizeof(sensorPackage));
    SD_Queue = xQueueCreate(size, sizeof(sensorPackage));
    uint8_t data[100];
    GPS_Queue = xQueueCreate(10, sizeof(data));
}

static sensorPackage DataSensorPackage;
void ION_QUEUE_Insert(xQueueHandle *QUEUE, uint16_t ID, uint32_t DATA, uint32_t TIME)
{
    DataSensorPackage.ID = ID;
    DataSensorPackage.DATA = DATA;
    DataSensorPackage.TIME = TIME;
    xQueueSendToBack(QUEUE, &DataSensorPackage, portMAX_DELAY);
}
