/**************************************************
*   GPS                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "GPS.h"
#include "LED.h"
#include "CAN.h"

/**
 * We are using Adafruit Ultimate GPS Breakout that you can find here.
 * https://www.adafruit.com/products/746
 *
 * We are using USART2 and are connected like this.
 * Gps	-	stm32f4xx
 * 3.3V	- 	Vdd
 * Gnd 	- 	Gnd
 * RX	-	PA2
 * TX 	- 	PA3
 *
 * We use 3.3V instead of 5V.
 *
 */
USART_TypeDef* USARTx;
FIL *fill;

uint8_t RxByte1 = 0;
uint8_t GPS_Initialise()
{   
    /*---INIT---*/
    USARTx = USART3;
    
    USART_initialise(USARTx);
    
    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

    uint32_t timeStamp = relativeTime;
    while((timeStamp + 1000) > relativeTime); //Wait 1 sec.
    
    //USART_Write_String(USARTx, PMTK_SET_NMEA_OUTPUT_RMCONLY);
    if(!GPS_Check(PMTK_SET_NMEA_OUTPUT_RMCONLY)) 
        return 0;
    
    //USART_Write_String(USARTx, PMTK_SET_NMEA_UPDATE_10HZ);
    if(!GPS_Check(PMTK_SET_NMEA_UPDATE_10HZ))
        return 0;
    
    //USART_Write_String(USARTx, PMTK_API_SET_FIX_CTL_5HZ);
    if(!GPS_Check(PMTK_API_SET_FIX_CTL_5HZ))
        return 0;
    
    return 1;
}

char gps_data[100] = {0};
char ACK[] = "$PMTK";
char ID[6] = "";
uint8_t GPS_Success(uint8_t turns)
{    
    int i = 0;
    while(turns--)
    {
        i = 0;
        gps_data[0] = 0;
        do
        {
            if(gps_data[0] != 0x24) // $
                i = 0;
            while(USART_GetFlagStatus(USARTx, USART_FLAG_RXNE) != SET);
            USART_ClearFlag(USARTx, USART_FLAG_RXNE);
            gps_data[i] = USART_ReceiveData(USARTx);
            //CAN_Write_16_bit(CAN1, 0xAA, (i + 48) << 8 | gps_data[i]);
            i++;
        }
        while(gps_data[i-1] != 0x0A || gps_data[0] != 0x24); // checking for \n and $.
        //SD_CARD_writeArray(fill, gps_data);
        //f_sync(fill);CAN_Write_8(CAN1, 0xAA, 8, d);
        uint8_t d0[8] = {gps_data[0],gps_data[1],gps_data[2],gps_data[3],gps_data[4],gps_data[5],gps_data[6],gps_data[7]};
        CAN_Write_8(CAN1, 0xAA, 8, d0);
        uint8_t d1[8] = {gps_data[8],gps_data[9],gps_data[10],gps_data[11],gps_data[12],gps_data[13],gps_data[14],gps_data[15]};
        CAN_Write_8(CAN1, 0xAA, 8, d1);
        uint8_t d2[8] = {gps_data[16],gps_data[17],gps_data[18],gps_data[19],gps_data[20],gps_data[21],gps_data[22],gps_data[23]};
        CAN_Write_8(CAN1, 0xAA, 8, d2);
        uint8_t d3[8] = {gps_data[24],gps_data[25],gps_data[26],gps_data[27],gps_data[28],gps_data[29],gps_data[30],gps_data[31]};
        CAN_Write_8(CAN1, 0xAA, 8, d3);
        LEDx_Toggle(LED12);
        strncpy(ID, gps_data, 5);
        if(strcmp(ID, ACK) == 0)
        {
            if(gps_data[13] == '3') // checking Ack Flag. 3 eq success, else fail.
                return 1;
            else
                return 0;
        }
    }
    return  0;
}

uint8_t GPS_Check(char *chr)
{
    int tries = 0;
    do
    {
        if(tries >= 1)
            return 0;
        USART_Write_String(USARTx, chr);
        tries++;
    }
    while(!GPS_Success(2));
    return 1;
}

void GPS_Read(char *chr)
{
    uint8_t length = 0, lengthCord = 0, index = 0;
    uint64_t datetime;
    uint32_t time, latitude, longitude, speed, course, date;
    uint32_t timeStamp = relativeTime;
    
    while(*chr != '\0')
    {
        while(*(chr) != ',')
        {
            length++;
            if(*chr == '\0')
                break;
            chr++;
        }
        
        if(length == 0)
        {
            length = 0;
            index++;
            chr++;
            continue;
        }
        
        switch(index)
        {
            case TYPE:
                //printf("$GPRMC - start: %d, length: %d\n", start, length);
                break;
            case TIME:
                time = GPS_Parse(chr, length);
                break;
            case VALID:
                //printf("VALID - start: %d, length: %d\n", start, length);
            case LAT:
                lengthCord = length;
                //printf("LAT - start: %d, length: %d\n", start, length);
                break;
            case LAT_DIR:
                latitude = GPS_ParseCord(chr, lengthCord);
                ION_QUEUE_Insert(TT_Queue, GPS_LATITUDE, latitude, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_LATITUDE, latitude, timeStamp);
                break;
            case LON:
                lengthCord = length;
                //printf("LONG - start: %d, length: %d\n", start, length);
                break;
            case LON_DIR:
                longitude = GPS_ParseCord(chr, lengthCord);
                ION_QUEUE_Insert(TT_Queue, GPS_LONGITUDE, longitude, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_LONGITUDE, longitude, timeStamp);
                break;
            case SPEED:
                speed = GPS_Parse(chr, length);
                ION_QUEUE_Insert(TT_Queue, GPS_SPEED, speed, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_SPEED, speed, timeStamp);
                break;
            case COURSE:
                course = GPS_Parse(chr, length);
                ION_QUEUE_Insert(TT_Queue, GPS_COURSE, course, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_COURSE, course, timeStamp);
                break;
            case DATE:
                date = GPS_Parse(chr, length);
                datetime = DATETIME_intToTime(date, time);
                ION_QUEUE_Insert(TT_Queue, GPS_TIME_LSB, datetime & 0xFFFF, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_TIME_LSB, datetime & 0xFFFF, timeStamp);
                ION_QUEUE_Insert(TT_Queue, GPS_TIME_MSB, (datetime >> 32)  & 0xFFFF, timeStamp);
                ION_QUEUE_Insert(SD_Queue, GPS_TIME_MSB, (datetime >> 32)  & 0xFFFF, timeStamp);
                break;
            case NADA1:
            case NADA2:
                //printf("NADA - start: %u, length: %d\n", start, length);
                break;
            case GPS_CRC:
                //printf("CRC - start: %u, length: %d\n", start, length);
                break;
            default:
                while(1);
        }
        
        if(*chr == '\0')
                break;
    
        length = 0;
        index++;
        chr++;
    }
}

uint32_t GPS_Parse(char *chr, uint8_t length)
{
    uint32_t value = 0, multi = 1;
    uint8_t des = 0;
    int i;
    chr--;
    
    for(i = 0;i < length;i++)
    {
        if(*chr != '.')
        {
            value += ((*chr) - '0') * multi;
            multi *= 10;
        }
        else
            des = i;
            
        chr--;
    }
    
    if(des == 0)
        value *= 10000;
    else if(des == 1)
        value *= 1000;
    else if(des == 2)
        value *= 100;
    else if(des == 3)
        value *= 10;
    return value;
}

uint32_t GPS_ParseCord(char *chr, uint8_t length)
{
    uint8_t dir = 0;
    uint32_t cord;
    uint32_t deg = 0; 
    uint32_t min = 0;
    uint32_t sek = 0;
    
    chr--;
    if(*chr == 'N' || *chr == 'E')
        dir = 0;
    else if(*chr == 'S' || *chr == 'W')
        dir = 1;
        
    chr--;
    cord = GPS_Parse(chr, length);
    
    deg = cord / 1000000;
    min = ((cord) / 10000) - (deg * 100);
    sek = cord - (deg * 1000000) - (min * 10000);
    
    return ((dir << 31) + (sek * 600000) + (min * 10000) + sek);
}

/*
//used for creating files.
char* GPS_getDateTime()
{
    put(dateTime, '.');
    put(dateTime, 't');
    put(dateTime, 'x');
    put(dateTime, 't');
    return dateTime;
}
*/
