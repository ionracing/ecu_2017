//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

//******************************************************************************
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
//******************************************************************************

//******************************************************************************
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "croutine.h"
//******************************************************************************

//******************************************************************************
#include "SD_CARD.h"
#include "ff.h"
#include "diskio.h"
//******************************************************************************

//******************************************************************************
#include "global.h"
#include "ION_QUEUE.h"
#include "watchdog.h"
#include "BTN.h"
#include "LED.h"
#include "GPS.h"
#include "ADC.h"
#include "TIM.h"
#include "Telemetry.h"
#include "IMU_SCC2130.h"
#include "IO_Mapping.h"
#include "IO_Mapping_ECU_Logger.h"
//******************************************************************************
/* ---Defines--- */
#define STACK_SIZE_MIN 1000 /* usStackDepth - the stack size DEFINED IN WORDS.*/
#define NUMBER_OF_THREADS 4     /* does not include watchdog thread. */
#define QUEUE_SIZE 100

/* ---Globals--- */
uint32_t relativeTime = 0;

/* ---LocalVariables--- */
uint32_t *ptrValues[100];
FLAG FLAGS[10] = {ASLEEP};

/* ---Threads--- */
void vLedBlink(void *pvParameters);
void vTT_Task(void *pvParameters);
void vSD_Task(void *pvParameters);
void vGPS_Task(void *pvParameters);
void vWatchdog(void *pvParameters);

//******************************************************************************
int main(void)
{
    //while(1);
    SystemCoreClockUpdate(); //MUST BE HERE.
    
    LED_Initialise(LED9);
    LED_Initialise(LED10);
    LED_Initialise(LED11);
    LED_Initialise(LED12);
    
    CAN_Initialise();
    GPS_Initialise();
    
    while(1)
    {
        // Mode-Btn
        
        // SD-Btn
        
        // Watchdog
    }
    
    //xTaskCreate( vLedBlink, (const char*)"LED", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
    //xTaskCreate( vTT_Task, (const char*)"Telemetry", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
    //xTaskCreate( vSD_Task, (const char*)"SD-Card", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
    //xTaskCreate( vGPS_Task, (const char*)"GPS", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
    //xTaskCreate( vWatchdog, (const char*)"Watchdog", STACK_SIZE_MIN, NULL, tskIDLE_PRIORITY, NULL );
    //vTaskStartScheduler();
}
//******************************************************************************

//******************************************************************************
void vWatchdog(void *pvParameters)
{
    uint8_t xFlag;
    for(;;)
    {
        xFlag = 0;
        for(int i = 0;i < NUMBER_OF_THREADS;i++)
        {
            switch(FLAGS[i])
            {
                case UNKNOWN:
                    xFlag++;
                    break;
                case ALIVE:
                    FLAGS[i] = UNKNOWN;
                    break;
                case ASLEEP:
                    break;
            }
        }
        
        if(xFlag == 0)
            watchdog_pet();
        taskYIELD();
    }
}


void vLedBlink(void *pvParameters)
{
    //FLAG *flg = &FLAGS[0];
    for(;;)
    {
        LEDx_Toggle(LED9);
        vTaskDelay( 250 / portTICK_RATE_MS );
        LEDx_Toggle(LED10);
        vTaskDelay( 250 / portTICK_RATE_MS );
        LEDx_Toggle(LED11);
        vTaskDelay( 250 / portTICK_RATE_MS );
        LEDx_Toggle(LED12);
        vTaskDelay( 250 / portTICK_RATE_MS );
    }
}

void vTT_Task(void *pvParameters)
{
    FLAG *flg = &FLAGS[1];
    for(;;)
    {
        *flg = ALIVE;
        Telemetry_send();
        *flg = ASLEEP;
    }
}

void vSD_Task(void *pvParameters)
{
    FLAG *flg = &FLAGS[2];
    sensorPackage ReceivedValue;
    portBASE_TYPE xStatus;
    FATFS FatFs;
    FIL fil;
    SD_CARD_Mount(&FatFs);
    
    char *dateTime = 0;//GPS_getDateTime();
    
    SD_CARD_OpenFile(&fil, dateTime, FA_READ | FA_WRITE | FA_OPEN_ALWAYS);
    for(;;)
    {
        *flg = ALIVE;
        xStatus = xQueueReceive(SD_Queue, &ReceivedValue, portMAX_DELAY);
        if(xStatus == pdPASS)
        {
            SD_CARD_writeInt(&fil, ReceivedValue.ID, 2);
            SD_CARD_writeInt(&fil, ReceivedValue.DATA, 4);
            SD_CARD_writeInt(&fil, ReceivedValue.TIME, 4);
            f_putc(',', &fil);
        }
        *flg = ASLEEP;
    }
}

void vGPS_Task(void *pvParameters)
{
    FLAG *flg = &FLAGS[3];
    char ReceivedValue;
    portBASE_TYPE xStatus;
    for(;;)
    {
        *flg = ALIVE;
        xStatus = xQueueReceive(GPS_Queue, &ReceivedValue, portMAX_DELAY);
        if(xStatus == pdPASS)
            GPS_Read(&ReceivedValue);
        *flg = ASLEEP;
    }
}
//******************************************************************************
