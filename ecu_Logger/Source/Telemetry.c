#include "Telemetry.h"

/*
    Need to do:
    Create init function:
    
    
    
    Check if new data is ready or call function when data is added to queue.
    Get data from queue.
    Packet the data.
    Send data.

**/

void Telemetry_Initialise()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
    
    //SPI_initialise(SPI1);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    GPIO_InitStruct.GPIO_Pin = 0;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    /*
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);

	EXTI_InitStruct.EXTI_Line = EXTI_Line4;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_InitStruct);
	
	NVIC_InitStruct.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
    */
}

const uint8_t MAX = 2;
sensorPackage pkgRecieved[MAX];
void Telemetry_send()
{
    uint8_t i = 0, length = 0;
    uint8_t data[30];
    if(TWO_WAY == RESET)
    {
        data[0] = 0xAA;
        data[1] = 0xBB;   
        for(i = 0;i < MAX;i++, ++length)
        {
            xQueueReceive(TT_Queue, &pkgRecieved[i], portMAX_DELAY);
            if(uxQueueMessagesWaiting(TT_Queue) == 0)
                break;
        }
        data[2] = 0;//crc;
        data[3] = 0;//crc;
        data[4] = length;
        data[5] = 0xCC;
        
        for(i = 0;i < length;i++)
        {
            data[6+i*3] = pkgRecieved[i].ID;
            data[7+i*3] = pkgRecieved[i].DATA;
            data[8+i*3] = pkgRecieved[i].TIME;
        }
        
        nrf24_transmitSync(data, length+6);
    }
    else
    {
        
    }
}

void Telemetry_recive()
{
    if(TWO_WAY == RESET)
    {
        
    }
    else
    {
        
    }
}

void EXTI4_IRQHandler(void)
{
    uint8_t readSPI();
}
