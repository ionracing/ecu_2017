#ifndef IOMAPPING_DISCOVERY_H
#define IOMAPPING_DISCOVERY_H

/* Constants for GPIOs that are used on the ECU */
#define PIN_RTDS PIN_LED1
#define GPIO_RTDS GPIO_LED1

#define PIN_PILOTLINE GPIO_Pin_0
#define GPIO_PILOTLINE GPIOC

#define PIN_VSI_RUN GPIO_Pin_2
#define GPIO_VSI_RUN GPIOC

#define PIN_BRAKELIGHT PIN_LED4
#define GPIO_BRAKELIGHT GPIO_LED4

#define PIN_FAN_RADIATOR GPIO_Pin_12
#define GPIO_FAN_RADIATOR GPIOB

#define PIN_FAN_EXHAUST GPIO_Pin_13
#define GPIO_FAN_EXHAUST  GPIOB

#define PIN_CAN1_EN GPIO_Pin_7
#define GPIO_CAN1_EN GPIOC

#define PIN_CAN2_EN GPIO_Pin_9
#define GPIO_CAN2_EN GPIOC

/* LEDs ---------------------------------------------------------*/

typedef enum 
{
    RED = 0,
    BLUE,
    GREEN,
    ORANGE,
    ALL
} LEDS;
#define LED_GPIO_RCC(LED)   (LED == RED     ? RCC_AHB1Periph_GPIOD : \
                            (LED == BLUE    ? RCC_AHB1Periph_GPIOD : \
                            (LED == GREEN   ? RCC_AHB1Periph_GPIOD : \
                            (LED == ORANGE  ? RCC_AHB1Periph_GPIOD : \
                            (LED == ALL     ? RCC_AHB1Periph_GPIOD : ERROR_Msg("WRONG LED_RCC"))))))
#define LED_GPIO_PIN(LED)   (LED == RED     ? GPIO_Pin_14 : \
                            (LED == BLUE    ? GPIO_Pin_15 : \
                            (LED == GREEN   ? GPIO_Pin_12 : \
                            (LED == ORANGE  ? GPIO_Pin_13 : \
                            (LED == ALL     ? (GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15) : ERROR_Msg("WRONG LED_PIN"))))))
#define LED_GPIO(LED)       (LED == RED     ? GPIOD : \
                            (LED == BLUE    ? GPIOD : \
                            (LED == GREEN   ? GPIOD : \
                            (LED == ORANGE  ? GPIOD : \
                            (LED == ALL     ? GPIOD : (ERROR_Msg("WRONG LED_GPIO") == 0 ? GPIOA : GPIOB))))))

/* BTNs ---------------------------------------------------------*/

typedef enum
{
    START = 0,
    STOP
} Butoonsn;
#define BTN_GPIO_RCC(BTN)                               (BTN == START   ? RCC_AHB1Periph_GPIOA : \
                                                        (BTN == STOP    ? RCC_AHB1Periph_GPIOA : ERROR_Msg("WRONG BTN_GPIO_RCC")))
#define BTN_GPIO_Pin(BTN)                               (BTN == START   ? GPIO_Pin_0 : \
                                                        (BTN == STOP    ? GPIO_Pin_0 : ERROR_Msg("WRONG BTN_GPIO_Pin")))
#define BTN_GPIOx(BTN)                                  (BTN == START   ? GPIOA : \
                                                        (BTN == STOP    ? GPIOA : (ERROR_Msg("WRONG BTN_GPIOx") == 0 ? GPIOA : GPIOA)))
#define BTN_EXTI_PortSource(BTN)                        (BTN == START   ? EXTI_PortSourceGPIOA : \
                                                        (BTN == STOP    ? EXTI_PortSourceGPIOA : ERROR_Msg("WRONG BTN_EXTI_PortSource")))
#define BTN_EXTI_PinSource(BTN)                         (BTN == START   ? EXTI_PinSource0 : \
                                                        (BTN == STOP    ? EXTI_PinSource0 : ERROR_Msg("WRONG BTN_EXTI_PinSource")))
#define BTN_EXTI_Linex(BTN)                             (BTN == START   ? EXTI_Line0 : \
                                                        (BTN == STOP    ? EXTI_Line0 : ERROR_Msg("WRONG BTN_EXTI_Linex")))
#define BTN_NVIC_Enable(BTN)                            (BTN == START   ? ENABLE : \
                                                        (BTN == STOP    ? ENABLE : (ERROR_Msg("WRONG BTN_NVIC_Enable") == 0 ? DISABLE : DISABLE)))
#define BTN_NVIC_IRQChannel(BTN)                        (BTN == START   ? EXTI0_IRQn : \
                                                        (BTN == STOP    ? EXTI0_IRQn : ERROR_Msg("WRONG BTN_NVIC_IRQChannel")))
#define BTN_NVIC_IRQChannelPreemptionPriority(BTN)      (BTN == START   ? 0 : \
                                                        (BTN == STOP    ? 0 : ERROR_Msg("WRONG BTN_NVIC_IRQChannelPreemptionPriority")))
#define BTN_NVIC_IRQChannelSubPriority(BTN)             (BTN == START   ? 1 : \
                                                        (BTN == STOP    ? 1 : ERROR_Msg("WRONG BTN_NVIC_IRQChannelSubPriority")))

/* CAN-bus mapping ----------------------------------------------*/
/*---CAN1---*/
#define GPIO_CAN1_TX                    GPIOB
#define GPIO_CAN1_RX                    GPIOB
#define PIN_CAN1_TX                     GPIO_Pin_9
#define PIN_CAN1_RX                     GPIO_Pin_8
/*---CAN2---*/
#define GPIO_CAN2_TX                    GPIOB
#define GPIO_CAN2_RX                    GPIOB
#define PIN_CAN2_TX                     GPIO_Pin_6
#define PIN_CAN2_RX                     GPIO_Pin_5

/* EEPROM -------------------------------------------------------*/
#define EEPROM_GPIO_RCC                 RCC_AHB1Periph_GPIOB
#define EEPROM_GPIO_MODE                GPIO_Mode_AF
#define EEPROM_GPIO_OTYPE               GPIO_OType_OD
#define EEPROM_GPIO_PIN                 GPIO_Pin_10 | GPIO_Pin_11
#define EEPROM_GPIO_PUPD                GPIO_PuPd_NOPULL
#define EEPROM_GPIO_SPEED               GPIO_Speed_50MHz
#define EEPROM_GPIO_GPIOX               GPIOB
#define EEPROM_GPIO_PinSource_SCL       GPIO_PinSource10
#define EEPROM_GPIO_PinSource_SDA       GPIO_PinSource11
#define EEPROM_GPIO_AF_I2C              GPIO_AF_I2C2
#define EEPROM_I2C_RCC                  RCC_APB1Periph_I2C2
#define EEPROM_I2C_ACK                  I2C_Ack_Enable
#define EEPROM_I2C_ACKNOWLEDGEDADRESS   I2C_AcknowledgedAddress_7bit
#define EEPROM_I2C_CLOCKSPEED           100000
#define EEPROM_I2C_DUTYCYCLE            I2C_DutyCycle_2
#define EEPROM_I2C_MODE                 I2C_Mode_I2C
#define EEPROM_I2C_OWNADDRESS1          0x00
#define EEPROM_I2C_I2CX                 I2C2


/* ---SPI--- */
#define SPI_RCC_GPIO(SPIx)                                  ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                                            ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : \
                                                            ((SPIx == SPI3) ? RCC_AHB1Periph_GPIOC : ERROR_Msg("WRONG SPI_RCC_GPIO"))))
                                            
#define SPI_RCC_GPIO_CS(SPIx)                               ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                                            ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : \
                                                            ((SPIx == SPI3) ? RCC_AHB1Periph_GPIOA : ERROR_Msg("WRONG SPI_RCC_GPIO_CS"))))
                                            
#define SPI_RCC_SPI(SPIx)                                   ((SPIx == SPI1) ? RCC_APB2Periph_SPI1 : \
                                                            ((SPIx == SPI2) ? RCC_APB1Periph_SPI2 : \
                                                            ((SPIx == SPI3) ? RCC_APB1Periph_SPI3 : ERROR_Msg("WRONG SPI_RCC_SPI"))))
                                            
#define SPI_GPIOx(SPIx)                                     ((SPIx == SPI1) ? GPIOA : \
                                                            ((SPIx == SPI2) ? GPIOB : \
                                                            ((SPIx == SPI3) ? GPIOC : (ERROR_Msg("WRONG SPI_GPIOx") == 0 ? GPIOA : GPIOA))))
                                            
#define SPI_GPIOx_CS(SPIx)                                  ((SPIx == SPI1) ? GPIOA : \
                                                            ((SPIx == SPI2) ? GPIOB : \
                                                            ((SPIx == SPI3) ? GPIOA : (ERROR_Msg("WRONG SPI_GPIOx_CS") == 0 ? GPIOA : GPIOA))))
                                            
#define SPI_GPIO_PinSource_SCLK(SPIx)                       ((SPIx == SPI1) ? GPIO_PinSource5 : \
                                                            ((SPIx == SPI2) ? GPIO_PinSource13 : \
                                                            ((SPIx == SPI3) ? GPIO_PinSource10 : ERROR_Msg("WRONG SPI_GPIO_PinSource_SCLK"))))
                                            
#define SPI_GPIO_PinSource_MISO(SPIx)                       ((SPIx == SPI1) ? GPIO_PinSource6 : \
                                                            ((SPIx == SPI2) ? GPIO_PinSource14 : \
                                                            ((SPIx == SPI3) ? GPIO_PinSource11 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MISO"))))
                                            
#define SPI_GPIO_PinSource_MOSI(SPIx)                       ((SPIx == SPI1) ? GPIO_PinSource7 : \
                                                            ((SPIx == SPI2) ? GPIO_PinSource15 : \
                                                            ((SPIx == SPI3) ? GPIO_PinSource12 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MOSI"))))
                                            
#define SPI_GPIO_AF_SPI(SPIx)                               ((SPIx == SPI1) ? GPIO_AF_SPI1 : \
                                                            ((SPIx == SPI2) ? GPIO_AF_SPI2 : \
                                                            ((SPIx == SPI3) ? GPIO_AF_SPI2 : ERROR_Msg("WRONG SPI_GPIO_AF_SPI"))))
                                            
#define SPI_GPIO_Pin_CS(SPIx)                               ((SPIx == SPI1) ? GPIO_Pin_4 : \
                                                            ((SPIx == SPI2) ? GPIO_Pin_12 : \
                                                            ((SPIx == SPI3) ? GPIO_Pin_12 : ERROR_Msg("WRONG SPI_GPIO_Pin_CS"))))
                                            
#define SPI_GPIO_Pin_SCLK(SPIx)                             ((SPIx == SPI1) ? GPIO_Pin_5 : \
                                                            ((SPIx == SPI2) ? GPIO_Pin_13 : \
                                                            ((SPIx == SPI3) ? GPIO_Pin_13 : ERROR_Msg("WRONG SPI_GPIO_Pin_SCLK"))))
                                            
#define SPI_GPIO_Pin_MISO(SPIx)                             ((SPIx == SPI1) ? GPIO_Pin_6 : \
                                                            ((SPIx == SPI2) ? GPIO_Pin_14 : \
                                                            ((SPIx == SPI3) ? GPIO_Pin_14 : ERROR_Msg("WRONG SPI_GPIO_Pin_MISO"))))
                                            
#define SPI_GPIO_Pin_MOSI(SPIx)                             ((SPIx == SPI1) ? GPIO_Pin_7 : \
                                                            ((SPIx == SPI2) ? GPIO_Pin_15 : \
                                                            ((SPIx == SPI3) ? GPIO_Pin_15 : ERROR_Msg("WRONG SPI_GPIO_Pin_MOSI"))))
                                            

/* ---USART--- */
#define USART_RCC_USART(USARTx)                             ((USARTx == USART1) ? RCC_APB2Periph_USART1 : \
                                                            ((USARTx == USART2) ? RCC_APB2Periph_USART1 : \
                                                            ((USARTx == USART3) ? RCC_APB2Periph_USART1 : \
                                                            ((USARTx == USART6) ? RCC_APB2Periph_USART1 : ERROR_Msg("WRONG USART_RCC_USART")))))
                                                            
#define USART_RCC_GPIO(USARTx)                              ((USARTx == USART1) ? RCC_AHB1Periph_GPIOD : \
                                                            ((USARTx == USART2) ? RCC_AHB1Periph_GPIOD : \
                                                            ((USARTx == USART3) ? RCC_AHB1Periph_GPIOD : \
                                                            ((USARTx == USART6) ? RCC_AHB1Periph_GPIOD : ERROR_Msg("WRONG USART_RCC_GPIO")))))
                                                            
#define USART_GPIO_Pin_Tx(USARTx)                           ((USARTx == USART1) ? GPIO_Pin_1 : \
                                                            ((USARTx == USART2) ? GPIO_Pin_15 : \
                                                            ((USARTx == USART3) ? GPIO_Pin_15 : \
                                                            ((USARTx == USART6) ? GPIO_Pin_15 : ERROR_Msg("WRONG USART_GPIO_Pin_Tx")))))
                                                            
#define USART_GPIO_Pin_Rx(USARTx)                           ((USARTx == USART1) ? GPIO_Pin_2 : \
                                                            ((USARTx == USART2) ? GPIO_Pin_15 : \
                                                            ((USARTx == USART3) ? GPIO_Pin_15 : \
                                                            ((USARTx == USART6) ? GPIO_Pin_15 : ERROR_Msg("WRONG USART_GPIO_Pin_Rx")))))
                                                            
#define USART_GPIOx(USARTx)                                 ((USARTx == USART1) ? GPIOA : \
                                                            ((USARTx == USART2) ? GPIOA : \
                                                            ((USARTx == USART3) ? GPIOA : \
                                                            ((USARTx == USART6) ? GPIOA : ERROR_Msg("WRONG USART_GPIOx") == 0 ? GPIOA : GPIOA))))
                                                            
#define USART_GPIO_PinSource_Tx(USARTx)                     ((USARTx == USART1) ? GPIO_PinSource2 : \
                                                            ((USARTx == USART2) ? GPIO_PinSource2 : \
                                                            ((USARTx == USART3) ? GPIO_PinSource2 : \
                                                            ((USARTx == USART6) ? GPIO_PinSource2 : ERROR_Msg("WRONG USART_GPIOx")))))
                                                            
#define USART_GPIO_PinSource_Rx(USARTx)                     ((USARTx == USART1) ? GPIO_PinSource3 : \
                                                            ((USARTx == USART2) ? GPIO_PinSource3 : \
                                                            ((USARTx == USART3) ? GPIO_PinSource3 : \
                                                            ((USARTx == USART6) ? GPIO_PinSource3 : ERROR_Msg("WRONG USART_GPIO_PinSource_Rx")))))
                                                            
#define USART_GPIO_AF_USARTx(USARTx)                        ((USARTx == USART1) ? GPIO_AF_USART1 : \
                                                            ((USARTx == USART2) ? GPIO_AF_USART2 : \
                                                            ((USARTx == USART3) ? GPIO_AF_USART3 : \
                                                            ((USARTx == USART6) ? GPIO_AF_USART6 : ERROR_Msg("WRONG USART_GPIO_AF_USARTx")))))
                                                            
#define USART_USART_BaudRate(USARTx)                        ((USARTx == USART1) ? 9600 : \
                                                            ((USARTx == USART2) ? 9600 : \
                                                            ((USARTx == USART3) ? 9600 : \
                                                            ((USARTx == USART6) ? 9600 : ERROR_Msg("WRONG USART_USART_BaudRate")))))
                                                            
#define USART_NVIC_Enable(USARTx)                           ((USARTx == USART1) ? ENABLE : \
                                                            ((USARTx == USART2) ? ENABLE : \
                                                            ((USARTx == USART3) ? ENABLE : \
                                                            ((USARTx == USART6) ? ENABLE : (ERROR_Msg("WRONG USART_NVIC_Enable") == 0 ? DISABLE : DISABLE)))))
                                                            
#define USART_NVIC_IRQChannel(USARTx)                       ((USARTx == USART1) ? USART1_IRQn : \
                                                            ((USARTx == USART2) ? USART2_IRQn : \
                                                            ((USARTx == USART3) ? USART3_IRQn : \
                                                            ((USARTx == USART6) ? USART6_IRQn : ERROR_Msg("WRONG USART_NVIC_IRQChannel")))))
                                                            
#define USART_NVIC_IRQChannelPreemptionPriority(USARTx)     ((USARTx == USART1) ? 0 : \
                                                            ((USARTx == USART2) ? 0 : \
                                                            ((USARTx == USART3) ? 0 : \
                                                            ((USARTx == USART6) ? 0 : ERROR_Msg("WRONG USART_NVIC_IRQChannelPreemptionPriority")))))

#define USART_NVIC_IRQChannelSubPriority(USARTx)            ((USARTx == USART1) ? 1 : \
                                                            ((USARTx == USART2) ? 1 : \
                                                            ((USARTx == USART3) ? 1 : \
                                                            ((USARTx == USART6) ? 1 : ERROR_Msg("WRONG USART_NVIC_IRQChannelSubPriority")))))

#endif //IO_MAPPING_DISCOVERY_H
