#ifndef DEFINES_H
#define DEFINES_H

#include "SPI.h"
#include "USART.h"
#include "BTN.h"
#include "LED.h"

/*---SPI---*/
extern SPI_Struct SPI1_Struct;
extern SPI_Struct SPI2_Struct;
extern SPI_Struct SPI3_Struct;
extern USART_Struct USART1_Struct;
extern USART_Struct USART2_Struct;
extern USART_Struct USART3_Struct;
extern USART_Struct UART4_Struct;
extern USART_Struct UART5_Struct;
extern USART_Struct USART6_Struct;
extern BTN_Struct BTN1_Struct;
extern LED_Struct LED1_Struct;

void defines_initialise(void);

#endif
