#ifndef ION_QUEUE_H
#define ION_QUEUE_H

#include "global.h"

void ION_QUEUE_Initialise(uint16_t size);
void ION_QUEUE_Insert(xQueueHandle *QUEUE, uint16_t ID, uint32_t DATA, uint32_t TIME);

#endif //ION_QUEUE_H
