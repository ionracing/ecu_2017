#ifndef TELEMETRI_H
#define TELEMETRI_H

#include "stdint.h"
#include "stm32f4xx_gpio.h"

#include "global.h"
#include "nrf24.h"
#include "SPI.h"

static uint8_t TWO_WAY = RESET;

//6Bit
typedef struct telemetriPackage
{
    uint16_t protocol_ID;
    uint16_t checkSum;
    uint8_t  length;
    uint8_t  DEST_PORT;
} telemetriPackage;

//10Bit
typedef struct telemetriPackageExtended
{
    uint16_t protocol_ID;
    uint16_t checkSum;
    uint8_t  length;
    uint8_t  DEST_PORT;
    uint8_t  SOURCE;
    uint8_t  OPTIONS;
    uint8_t  SEQ;
    uint8_t  ACK;
} telemetriPackageExtended;

void Telemetry_Initialise(void);
void Telemetry_send(void);

#endif
