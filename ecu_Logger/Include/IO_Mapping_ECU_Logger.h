#ifndef IOMAPPING_DISCOVERY_H
#define IOMAPPING_DISCOVERY_H

/* LEDs ---------------------------------------------------------*/
typedef enum
{
    LED9 = 0,
    LED10,
    LED11,
    LED12
} LEDs;

/*
#define LED_GPIO_RCC(LED)   ((LED == LED9 | LED == LED10 | LED == LED11 | LED == LED12) ? RCC_AHB1Periph_GPIOE : ERROR_Msg("WRONG LED_RCC"))
#define LED_GPIO_PIN(LED)   (LED == LED9) ? GPIO_Pin_12 : (LED == LED10) ? GPIO_Pin_13 : (LED == LED11) ? GPIO_Pin_14 : (LED == LED12 ? GPIO_Pin_15 : ERROR_Msg("WRONG LED_PIN"))
#define LED_GPIO(LED)       ((LED == LED9 | LED == LED10 | LED == LED11 | LED == LED12) ? GPIOE : (ERROR_Msg("WRONG LED_GPIO") == 0 ? GPIOA : GPIOA))
*/

#define LED_GPIO_RCC(LED)   ((LED == LED9 | LED == LED10 | LED == LED11 | LED == LED12) ? RCC_AHB1Periph_GPIOD : ERROR_Msg("WRONG LED_RCC"))
#define LED_GPIO_PIN(LED)   (LED == LED9) ? GPIO_Pin_12 : (LED == LED10) ? GPIO_Pin_13 : (LED == LED11) ? GPIO_Pin_14 : (LED == LED12 ? GPIO_Pin_15 : ERROR_Msg("WRONG LED_PIN"))
#define LED_GPIO(LED)       ((LED == LED9 | LED == LED10 | LED == LED11 | LED == LED12) ? GPIOD : (ERROR_Msg("WRONG LED_GPIO") == 0 ? GPIOA : GPIOA))

/* BTNs ---------------------------------------------------------*/

typedef enum
{
    SD = 0,
    MODE
} BUTTONS;
#define BTN_GPIO_RCC(BTN)               (BTN == SD   ? RCC_AHB1Periph_GPIOC : \
                                        (BTN == MODE    ? RCC_AHB1Periph_GPIOC : ERROR_Msg("WRONG BTN_GPIO_RCC")))
#define BTN_GPIO_Pin(BTN)               (BTN == SD   ? GPIO_Pin_0 : \
                                        (BTN == MODE    ? GPIO_Pin_1 : ERROR_Msg("WRONG BTN_GPIO_Pin")))
#define BTN_GPIOx(BTN)                  (BTN == SD   ? GPIOC : \
                                        (BTN == MODE    ? GPIOC : (ERROR_Msg("WRONG BTN_GPIOx") == 0 ? GPIOA : GPIOA)))
#define BTN_EXTI_PortSource(BTN)        (BTN == SD   ? EXTI_PortSourceGPIOC : \
                                        (BTN == MODE    ? EXTI_PortSourceGPIOC : ERROR_Msg("WRONG BTN_EXTI_PortSource")))
#define BTN_EXTI_PinSource(BTN)         (BTN == SD   ? EXTI_PinSource0 : \
                                        (BTN == MODE    ? EXTI_PinSource1 : ERROR_Msg("WRONG BTN_EXTI_PinSource")))
#define BTN_EXTI_Linex(BTN)             (BTN == SD   ? EXTI_Line0 : \
                                        (BTN == MODE    ? EXTI_Line1 : ERROR_Msg("WRONG BTN_EXTI_Linex")))
#define BTN_NVIC_Enable(BTN)            (BTN == SD   ? ENABLE : \
                                        (BTN == MODE    ? ENABLE : (ERROR_Msg("WRONG BTN_NVIC_Enable") == 0 ? DISABLE : DISABLE)))
#define BTN_NVIC_IRQChannel(BTN)        (BTN == SD   ? EXTI0_IRQn : \
                                        (BTN == MODE    ? EXTI1_IRQn : ERROR_Msg("WRONG BTN_NVIC_IRQChannel")))
#define BTN_NVIC_IRQChannelPP(BTN)      (BTN == SD   ? 0 : \
                                        (BTN == MODE    ? 0 : ERROR_Msg("WRONG BTN_NVIC_IRQChannelPreemptionPriority")))
#define BTN_NVIC_IRQChannelSP(BTN)      (BTN == SD   ? 1 : \
                                        (BTN == MODE    ? 1 : ERROR_Msg("WRONG BTN_NVIC_IRQChannelSubPriority")))

/* ---CAN--- */
#define CAN_GPIO_RCC                RCC_AHB1Periph_GPIOB
#define CAN_GPIO_MODE               GPIO_Mode_AF
#define CAN_GPIO_OTYPE              GPIO_OType_PP
#define CAN_GPIO_PIN_RX             GPIO_Pin_8
#define CAN_GPIO_PIN_TX             GPIO_Pin_9
#define CAN_GPIO_PUPD               GPIO_PuPd_UP
#define CAN_GPIO_SPEED              GPIO_Speed_100MHz
#define CAN_GPIO_GPIOX              GPIOB
#define CAN_GPIO_PinSource_RX       GPIO_PinSource8
#define CAN_GPIO_PinSource_TX       GPIO_PinSource9
#define CAN_GPIO_AF_CAN             GPIO_AF_CAN1
#define CAN_CAN_RCC                 RCC_APB1Periph_CAN1
#define CAN_CANx                    CAN1
#define CAN_NVIC_IRQChannel         CAN1_RX0_IRQn
#define CAN_NVIC_IRQChannelPP       0
#define CAN_NVIC_IRQChannelSP       1
#define CAN_NVIC_IRQChannelCmd      ENABLE

/* ADC ---------------------------------------------------------*/
typedef enum
{
    BPS1 = 0,
    BSP2,
    AIRTEMP,
    APPS1,
    APPS2,
    BPPS,
    STEERING
} ADCS;

#define ADC_RCC(ADC)    ((ADC == BPS1) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == BSP2) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == AIRTEMP) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == APPS1) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == APPS2) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == BPPS) ? RCC_APB2Periph_ADC1 : \
                        ((ADC == STEERING) ? RCC_APB2Periph_ADC1 : ERROR_Msg("WRONG ADC_RCC"))))))))

#define ADC_GPIO_RCC(ADC)   ((ADC == BPS1) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == BSP2) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == AIRTEMP) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == APPS1) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == APPS2) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == BPPS) ? RCC_AHB1Periph_GPIOC : \
                            ((ADC == STEERING) ? RCC_AHB1Periph_GPIOC : (ERROR_Msg("WRONG ADC_GPIO_RCC") == 0 ? RCC_AHB1Periph_GPIOA : RCC_AHB1Periph_GPIOA))))))))

#define ADC_GPIO(ADC)   (ADC == BPS1 ? GPIOA : \
                        (ADC == BSP2 ? GPIOA : \
                        (ADC == AIRTEMP ? GPIOA : \
                        (ADC == APPS1 ? GPIOA : \
                        (ADC == APPS2 ? GPIOA : \
                        (ADC == BPPS ? GPIOA : \
                        (ADC == STEERING ? GPIOA : (ERROR_Msg("WRONG ADC_GPIO_PIN") == 0) ? GPIOA : GPIOA)))))))
                        
#define ADC_ADC(ADC)    (ADC == BPS1 ? ADC1 : \
                        (ADC == BSP2 ? ADC1 : \
                        (ADC == AIRTEMP ? ADC1 : \
                        (ADC == APPS1 ? ADC1 : \
                        (ADC == APPS2 ? ADC1 : \
                        (ADC == BPPS ? ADC1 : \
                        (ADC == STEERING ? ADC1 : (ERROR_Msg("WRONG ADC_GPIO_PIN") == 0) ? ADC1 : ADC1)))))))

#define ADC_GPIO_PIN(ADC)   (ADC == BPS1 ? GPIO_Pin_0 : \
                            (ADC == BSP2 ? GPIO_Pin_1 : \
                            (ADC == AIRTEMP ? GPIO_Pin_2 : \
                            (ADC == APPS1 ? GPIO_Pin_3 : \
                            (ADC == APPS2 ? GPIO_Pin_4 : \
                            (ADC == BPPS ? GPIO_Pin_5 : \
                            (ADC == STEERING ? GPIO_Pin_4 : ERROR_Msg("WRONG ADC_GPIO_PIN"))))))))

#define ADC_GPIO_PIN_CHANNEL(ADC)   (ADC == BPS1 ? ADC_Channel_8 : \
                                    (ADC == BSP2 ? ADC_Channel_9 : \
                                    (ADC == AIRTEMP ? ADC_Channel_2 : \
                                    (ADC == APPS1 ? ADC_Channel_3 : \
                                    (ADC == APPS2 ? ADC_Channel_4 : \
                                    (ADC == BPPS ? ADC_Channel_5 : \
                                    (ADC == STEERING ? ADC_Channel_14 : ERROR_Msg("WRONG ADC_GPIO_PIN_CHANNEL"))))))))


/* ---USART--- */
#define USART_RCC_USART(USARTx)             ((USARTx == USART1) ? RCC_APB2Periph_USART1 : \
                                            ((USARTx == USART2) ? RCC_APB1Periph_USART2 : \
                                            ((USARTx == USART3) ? RCC_APB1Periph_USART3 : ERROR_Msg("WRONG USART_RCC_USART"))))
#define USART_RCC_GPIO(USARTx)              ((USARTx == USART1) ? RCC_AHB1Periph_GPIOA : \
                                            ((USARTx == USART2) ? RCC_AHB1Periph_GPIOD : \
                                            ((USARTx == USART3) ? RCC_AHB1Periph_GPIOD : ERROR_Msg("WRONG USART_RCC_GPIO"))))
#define USART_GPIO_Pin_Tx(USARTx)           ((USARTx == USART1) ? GPIO_Pin_9 : \
                                            ((USARTx == USART2) ? GPIO_Pin_5 : \
                                            ((USARTx == USART3) ? GPIO_Pin_8 : ERROR_Msg("WRONG USART_GPIO_Pin_Tx"))))
#define USART_GPIO_Pin_Rx(USARTx)           ((USARTx == USART1) ? GPIO_Pin_10 : \
                                            ((USARTx == USART2) ? GPIO_Pin_6 : \
                                            ((USARTx == USART3) ? GPIO_Pin_9 : ERROR_Msg("WRONG USART_GPIO_Pin_Rx"))))
#define USART_GPIOx(USARTx)                 ((USARTx == USART1) ? GPIOA : \
                                            ((USARTx == USART2) ? GPIOD : \
                                            ((USARTx == USART3) ? GPIOD : ERROR_Msg("WRONG USART_GPIOx") == 0 ? GPIOA : GPIOA)))
#define USART_GPIO_PinSource_Tx(USARTx)     ((USARTx == USART1) ? GPIO_PinSource9 : \
                                            ((USARTx == USART2) ? GPIO_PinSource5 : \
                                            ((USARTx == USART3) ? GPIO_PinSource8 : ERROR_Msg("WRONG USART_GPIOx"))))
#define USART_GPIO_PinSource_Rx(USARTx)     ((USARTx == USART1) ? GPIO_PinSource10 : \
                                            ((USARTx == USART2) ? GPIO_PinSource6 : \
                                            ((USARTx == USART3) ? GPIO_PinSource9 : ERROR_Msg("WRONG USART_GPIO_PinSource_Rx"))))
#define USART_GPIO_AF_USARTx(USARTx)        ((USARTx == USART1) ? GPIO_AF_USART1 : \
                                            ((USARTx == USART2) ? GPIO_AF_USART2 : \
                                            ((USARTx == USART3) ? GPIO_AF_USART3 : ERROR_Msg("WRONG USART_GPIO_AF_USARTx"))))
#define USART_USART_BaudRate(USARTx)        ((USARTx == USART1) ? 9600 : \
                                            ((USARTx == USART2) ? 9600 : \
                                            ((USARTx == USART3) ? 9600 : ERROR_Msg("WRONG USART_USART_BaudRate"))))
#define USART_NVIC_Enable(USARTx)           ((USARTx == USART1) ? ENABLE : \
                                            ((USARTx == USART2) ? ENABLE : \
                                            ((USARTx == USART3) ? ENABLE : (ERROR_Msg("WRONG USART_NVIC_Enable") == 0 ? DISABLE : DISABLE))))
#define USART_NVIC_IRQChannel(USARTx)       ((USARTx == USART1) ? USART1_IRQn : \
                                            ((USARTx == USART2) ? USART2_IRQn : \
                                            ((USARTx == USART3) ? USART3_IRQn : ERROR_Msg("WRONG USART_NVIC_IRQChannel"))))
#define USART_NVIC_IRQChannelPP(USARTx)     ((USARTx == USART1) ? 0 : \
                                            ((USARTx == USART2) ? 0 : \
                                            ((USARTx == USART3) ? 0 : ERROR_Msg("WRONG USART_NVIC_IRQChannelPreemptionPriority"))))
#define USART_NVIC_IRQChannelSP(USARTx)     ((USARTx == USART1) ? 1 : \
                                            ((USARTx == USART2) ? 1 : \
                                            ((USARTx == USART3) ? 1 : ERROR_Msg("WRONG USART_NVIC_IRQChannelSubPriority"))))
                                            
/* ---SPI--- */
#define SPI_RCC_GPIO(SPIx)              ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                        ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : ERROR_Msg("WRONG SPI_RCC_GPIO")))
#define SPI_RCC_GPIO_CS(SPIx)           ((SPIx == SPI1) ? RCC_AHB1Periph_GPIOA : \
                                        ((SPIx == SPI2) ? RCC_AHB1Periph_GPIOB : ERROR_Msg("WRONG SPI_RCC_GPIO_CS")))
#define SPI_RCC_SPI(SPIx)               ((SPIx == SPI1) ? RCC_APB2Periph_SPI1 : \
                                        ((SPIx == SPI2) ? RCC_APB1Periph_SPI2 : ERROR_Msg("WRONG SPI_RCC_SPI")))
#define SPI_GPIOx(SPIx)                 ((SPIx == SPI1) ? GPIOA : \
                                        ((SPIx == SPI2) ? GPIOB : (ERROR_Msg("WRONG SPI_GPIOx") == 0 ? GPIOA : GPIOA)))
#define SPI_GPIOx_CS(SPIx)              ((SPIx == SPI1) ? GPIOA : \
                                        ((SPIx == SPI2) ? GPIOB : (ERROR_Msg("WRONG SPI_GPIOx_CS") == 0 ? GPIOA : GPIOA)))
#define SPI_GPIO_PinSource_SCLK(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource5 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource13 : ERROR_Msg("WRONG SPI_GPIO_PinSource_SCLK")))
#define SPI_GPIO_PinSource_MISO(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource6 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource14 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MISO")))
#define SPI_GPIO_PinSource_MOSI(SPIx)   ((SPIx == SPI1) ? GPIO_PinSource7 : \
                                        ((SPIx == SPI2) ? GPIO_PinSource15 : ERROR_Msg("WRONG SPI_GPIO_PinSource_MOSI")))
#define SPI_GPIO_AF_SPI(SPIx)           ((SPIx == SPI1) ? GPIO_AF_SPI1 : \
                                        ((SPIx == SPI2) ? GPIO_AF_SPI2 : ERROR_Msg("WRONG SPI_GPIO_AF_SPI")))
#define SPI_GPIO_Pin_CS(SPIx)           ((SPIx == SPI1) ? GPIO_Pin_4 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_12 : ERROR_Msg("WRONG SPI_GPIO_Pin_CS")))
#define SPI_GPIO_Pin_SCLK(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_5 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_13 : ERROR_Msg("WRONG SPI_GPIO_Pin_SCLK")))
#define SPI_GPIO_Pin_MISO(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_6 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_14 : ERROR_Msg("WRONG SPI_GPIO_Pin_MISO")))
#define SPI_GPIO_Pin_MOSI(SPIx)         ((SPIx == SPI1) ? GPIO_Pin_7 : \
                                        ((SPIx == SPI2) ? GPIO_Pin_15 : ERROR_Msg("WRONG SPI_GPIO_Pin_MOSI")))

#endif
