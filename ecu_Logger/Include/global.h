#ifndef GLOBAL_H
#define GLOBAL_H

#include "FreeRTOS.h"
#include "queue.h"

extern xQueueHandle TT_Queue;
extern xQueueHandle SD_Queue;
extern xQueueHandle GPS_Queue;

extern uint32_t relativeTime;

typedef struct {
    uint16_t ID;
    uint32_t DATA;
    uint32_t TIME;
} sensorPackage;

#endif //GLOBAL_H
