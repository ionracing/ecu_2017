#ifndef SD_CARD_H
#define SD_CARD_H

#include "SPI.h"

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_rcc.h"

#include "ff.h"
#include "Integer.h"

#define DBUG    1
#define NODBUG  0

#define SD_COMMAND_TIMEOUT 5000

typedef struct command_fields {
    unsigned        end_bit:1;            // always 1
    unsigned        crc:7;                // CRC
    unsigned int    argument;             // 32 bit argument
    unsigned        index:6;              // command index
    unsigned        transmitter_bit:1;    // always 1
    unsigned        start_bit:1;          // always 0
} command_fields;
typedef struct command_bytes {
    unsigned char    crc_w_stop;          // crc with stop bit
    unsigned char    do_not_send;         // filler to account for compiler TODO: Why?
    unsigned char    arg_lsb;             // least significant argument byte
    unsigned char    arg_3;
    unsigned char    arg_2;
    unsigned char    arg_msb;             // most sign byte of arb
    unsigned char    cmd_w_starttx;       // command with start and tx bits
} command_bytes;
union sd_command {
    command_fields fields;
    unsigned char bytewise [7];
    command_bytes bytes;
};

#define GO_IDLE_STATE               0           // CMD0 - resets card
#define SEND_OP_COND                1           // CMD1 - send operating condition
#define SEND_IF_COND                8           // CMD8 - send interface condition
#define SEND_CSD                    9           // CMD9 - send card status
#define SET_BLOCKLEN                16          // CMD16 - set blocklength
#define WRITE_SINGLE_BLOCK          24          // CMD24 - write (single) block
#define WRITE_MULTIPLE_BLOCKS       25          // CMD25 - write (multiple) blocks
#define APP_CMD                     55          // CMD55 - next command is an application specific command, not standard
#define READ_OCR                    58          // CMD58 - read the ocr register
#define CRC_ON_OFF                  59          // CMD59 - turns crc on or off

#define SD_STATUS                   13          // ACMD13 - send sd card status
#define SET_WR_BLOCK_ERASE_COUNT    23          // ACMD23 - For only SDC. Define number of blocks to pre-erase with next multi-block write command.
#define SD_SEND_OP_COND             41          // ACMD41 - send operating condition

#define READ_SINGLE_BLOCK           17          // CMD17 - read single block
#define READ_MULTIPLE_BLOCKS        18          // CMD18 - read multiple blocks
#define STOP_TRANSMISSION           12          // CMD12 - stop transmission  

#define CMDX                        99

#define START_BLOCK                 0xFE        // used to indicate the start of a data block
#define MMC_FLOATING_BUS            0xFF        // Floating bus condition(?)

#define R1_IDLE_STATE               (1 << 0)
#define R1_ERASE_RESET              (1 << 1)
#define R1_ILLEGAL_COMMAND          (1 << 2)
#define R1_COM_CRC_ERROR            (1 << 3)
#define R1_ERASE_SEQUENCE_ERROR     (1 << 4)
#define R1_ADDRESS_ERROR            (1 << 5)
#define R1_PARAMETER_ERROR          (1 << 6)

// Types
//  - v1.x Standard Capacity
//  - v2.x Standard Capacity
//  - v2.x High Capacity
//  - Not recognised as an SD Card
 
typedef enum
{
    SDCARD_FAIL = 0x00,
    SDCARD_MMCV3 = 0x01,
    SDCARD_V1 = 0x02,
    SDCARD_V2 = 0x04,
    SDCARD_SDC = (SDCARD_V1 | SDCARD_V2),
    SDCARD_BLOCK = 0x08
} SDCARD_STATUS;

// Summary: The format of an R1 type response
// Description: This union represents different ways to access an SD card R1 type response packet.
typedef union
{
    unsigned char     byte;                // Byte-wise access
    // This structure allows bitwise access of the response
    struct
    {
        unsigned     IN_IDLE_STATE:1;      // Card is in idle state
        unsigned     ERASE_RESET:1;        // Erase reset flag
        unsigned     ILLEGAL_CMD:1;        // Illegal command flag
        unsigned     CRC_ERR:1;            // CRC error flag
        unsigned     ERASE_SEQ_ERR:1;      // Erase sequence error flag
        unsigned     ADDRESS_ERR:1;        // Address error flag
        unsigned     PARAM_ERR:1;          // Parameter flag   
        unsigned     B7:1;                 // Unused bit 7
    }bits;
} SD_RESPONSE;

uint8_t SD_CARD_InitialiseCard(void);
uint8_t SD_CARD_WriteRead(INT arg);
uint8_t SD_CARD_InitialiseCardV1(void);
uint8_t SD_CARD_InitialiseCardV2(void);
void SD_CARD_WriteCom(command_fields *com);
INT SD_CARD_Read(BYTE *buffer, INT length);
INT SD_CARD_Write(const BYTE *buffer, BYTE token);
uint32_t SD_CARD_Cmd(INT cmd, INT arg);
uint8_t SD_CARD_RecieveR1(void);
uint32_t SD_CARD_RecieveR7(void);
uint32_t SD_CARD_RecieveR3(void);
INT SD_CARD_InitialiseDisk(void);
uint8_t SD_CARD_OpenFile(FIL *fil, const char *fileName, unsigned char mode);
uint8_t SD_CARD_CloseFile(FIL *fil);
uint8_t SD_CARD_Mount(FATFS *FatFs);
uint8_t SD_CARD_UnMount(void);
void SD_CARD_writeInt(FIL *fil, uint32_t data, uint8_t btw);
void SD_CARD_writeArray(FIL *fil, char data[]);

#endif //SD_CARD_H
