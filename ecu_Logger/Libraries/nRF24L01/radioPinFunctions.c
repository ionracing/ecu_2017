/*
* ----------------------------------------------------------------------------
* “THE COFFEEWARE LICENSE” (Revision 1):
* <ihsan@kehribar.me> wrote this file. As long as you retain this notice you
* can do whatever you want with this stuff. If we meet some day, and you think
* this stuff is worth it, you can buy me a coffee in return.
* -----------------------------------------------------------------------------
* Please define your platform spesific functions in this file ...
* -----------------------------------------------------------------------------
*/
#include "stm32f4xx.h"
#include "SPI.h"
#include "LED.h"
#include "Telemetry.h"

#define SPI_CS         GPIO_Pin_4
#define SPI_SCLK       GPIO_Pin_5
#define SPI_MISO       GPIO_Pin_6
#define SPI_MOSI       GPIO_Pin_7
#define SPI_EN         GPIO_Pin_8

#define set_bit(pin)    GPIOA->ODR |= (pin)
#define clr_bit(pin)    GPIOA->ODR &= ~(pin)
#define check_bit(pin)  GPIOA->IDR & (pin)

/* ------------------------------------------------------------------------- */
void nrf24_setupPins()
{
    GPIO_InitTypeDef GPIO_InitStruct;
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    GPIO_InitStruct.GPIO_Pin = SPI_EN | SPI_CS | SPI_SCLK | SPI_MOSI | SPI_MISO;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    GPIO_InitStruct.GPIO_Pin = SPI_MISO;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    set_bit(SPI_EN);
    set_bit(SPI_CS);
    set_bit(SPI_SCLK);
    set_bit(SPI_MOSI);
    clr_bit(SPI_MISO);
}
/* ------------------------------------------------------------------------- */
void nrf24_ce_digitalWrite(uint8_t state)
{
    if(state)
    {
        set_bit(SPI_EN);
    }
    else
    {
        clr_bit(SPI_EN);
    }
}
/* ------------------------------------------------------------------------- */
void nrf24_csn_digitalWrite(uint8_t state)
{
    if(state)
    {
        set_bit(SPI_CS);
    }
    else
    {
        clr_bit(SPI_CS);
    }
}
/* ------------------------------------------------------------------------- */
void nrf24_sck_digitalWrite(uint8_t state)
{
    if(state)
    {
        set_bit(SPI_SCLK);
    }
    else
    {
        clr_bit(SPI_SCLK);
    }
}
/* ------------------------------------------------------------------------- */
void nrf24_mosi_digitalWrite(uint8_t state)
{
    if(state)
    {
        set_bit(SPI_MOSI);
    }
    else
    {
        clr_bit(SPI_MOSI);
    }
}
/* ------------------------------------------------------------------------- */
uint8_t nrf24_miso_digitalRead()
{
    return check_bit(SPI_MISO);
}
/* ------------------------------------------------------------------------- */
