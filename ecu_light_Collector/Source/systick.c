
/*---INCLUDES---*/
#include "systick.h"

volatile uint16_t clk40ms = CLK_RESET;
volatile uint16_t clk1000ms = CLK_RESET;

volatile uint8_t LOGDATA_FLAG;
volatile uint8_t LED_FLAG;


void systick_init(void)
{
	SysTick_Config(SystemCoreClock / 1000);
}

void SysTick_Handler(void)
{
	if (clk40ms != CLK_COMPLETE) // 0.04 seconds
    {
		clk40ms++;
		if (clk40ms >= 41)
        {
            clk40ms = CLK_COMPLETE;
            LOGDATA_FLAG = SET;
        }
	}
    if (clk1000ms != CLK_COMPLETE) // 1 second
    {
		clk1000ms++;
		if (clk1000ms >= 1001)
        {
            clk1000ms = CLK_COMPLETE;
            LED_FLAG = SET;
        }
	}
}
