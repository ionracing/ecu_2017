//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

#include "stm32f4xx.h"
#include "CAN.h"
#include "systick.h"
#include "IC.h"
#include "LED.h"
#include "IO_Mapping_Collector.h"
#include "ION_ID.h"

void initialize(void);

int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
	
    initialize();
    
    while(1)
    {
        if(LED_FLAG == SET)
        {
            LED_FLAG = RESET;
            LEDx_Toggle(LED1);
        }
        if(LOGDATA_FLAG == SET)
        {
            clk40ms = CLK_RESET;
            CAN_Write_32_bit(CAN1, WHEEL_1, IC_getFreq(1));
            CAN_Write_32_bit(CAN1, WHEEL_2, IC_getFreq(2));
            CAN_Write_32_bit(CAN1, WHEEL_3, IC_getFreq(3));
            CAN_Write_32_bit(CAN1, WHEEL_4, IC_getFreq(4));
        }
        
        if(clk1000ms == CLK_COMPLETE)
            clk1000ms = CLK_RESET;
        if(clk40ms == CLK_COMPLETE)
            clk40ms = CLK_RESET;
    }
}

void initialize()
{
    LED_Initialise(LED1);
    CAN_Initialise();
    systick_init();
    IC_initialise();
}
