/*
* LEDs for debugging purposes.
* The LEDS are connected to:
*
* LED 1 : PD0
* LED 2 : PD1
* LED 3 : PD2
* LED 4 : PD3
*/

/* Includes --------------------------------------------------------*/
#include "LED.h"

/* Configuration ---------------------------------------------------*/
void LED_Initialise(LEDs LEDx)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
  
    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(LED_GPIO_RCC(LEDx), ENABLE);

    /* Configure the GPIO_LED pins */
    GPIO_InitStructure.GPIO_Pin = LED_GPIO_PIN(LEDx);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(LED_GPIO(LEDx), &GPIO_InitStructure);
}

void LEDx_On(uint16_t LEDx)
{
    GPIO_SetBits(LED_GPIO(LEDx), LED_GPIO_PIN(LEDx));
}

void LEDx_Off(uint16_t LEDx)
{
    GPIO_ResetBits(LED_GPIO(LEDx), LED_GPIO_PIN(LEDx));
}

void LEDx_Toggle(uint16_t LEDx)
{
    GPIO_ToggleBits(LED_GPIO(LEDx), LED_GPIO_PIN(LEDx));
}
