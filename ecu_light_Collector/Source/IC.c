#include "IC.h"

/**--------------------------------------------------------------
wheel sensor 1	: PC7	:	TIM8 CH2    -   RightFrontWheel
wheel sensor 2  : PC8	:   TIM8_CH3    -   LeftFrontWheel
wheel sensor 3  : PE9	:   TIM1_CH1    -   RightBackWheel
wheel sensor 4  : PE11  :   TIM1_CH2    -   LeftBackWheel
---------------------------------------------------------------*/
 
void IC_initialise()
{
    //STRUCT
	GPIO_InitTypeDef GPIO_initStruct;
    TIM_TimeBaseInitTypeDef TIM_initStruct;
	TIM_ICInitTypeDef TIM_ICinitStruct;
    NVIC_InitTypeDef NVIC_initStruct;
    
    //CLOCK
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);
    
    //GPIO
	GPIO_initStruct.GPIO_Mode   = GPIO_Mode_AF;
	GPIO_initStruct.GPIO_OType  = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Pin    = GPIO_Pin_7;
	GPIO_initStruct.GPIO_PuPd   = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Speed  = GPIO_Speed_100MHz;
	GPIO_Init(GPIOC, &GPIO_initStruct);
	
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_TIM1);
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM8);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM8);
    
    //TIM
    TIM_initStruct.TIM_Period            = 0xFFFF;
	TIM_initStruct.TIM_Prescaler         = 0x000;
	TIM_initStruct.TIM_ClockDivision     = TIM_CKD_DIV1;
	TIM_initStruct.TIM_CounterMode       = TIM_CounterMode_Up;
	TIM_initStruct.TIM_RepetitionCounter = 0x0000;
    TIM_TimeBaseInit(TIM1, &TIM_initStruct);
  
	TIM_ICinitStruct.TIM_Channel     = TIM_Channel_1 | TIM_Channel_2;
	TIM_ICinitStruct.TIM_ICFilter    = 0x0; // 0x0 -> 0xFF
	TIM_ICinitStruct.TIM_ICPolarity  = TIM_ICPolarity_Rising;
	TIM_ICinitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICinitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(TIM1, &TIM_ICinitStruct);
	
	TIM_Cmd(TIM1, ENABLE);
    
	TIM_initStruct.TIM_Period            = 0xFFFF;
	TIM_initStruct.TIM_Prescaler         = 0x000;
	TIM_initStruct.TIM_ClockDivision     = TIM_CKD_DIV1;
	TIM_initStruct.TIM_CounterMode       = TIM_CounterMode_Up;
	TIM_initStruct.TIM_RepetitionCounter = 0x0000;
    TIM_TimeBaseInit(TIM8, &TIM_initStruct);
  
	TIM_ICinitStruct.TIM_Channel     = TIM_Channel_2 | TIM_Channel_3;
	TIM_ICinitStruct.TIM_ICFilter    = 0x0; // 0x0 -> 0xFF
	TIM_ICinitStruct.TIM_ICPolarity  = TIM_ICPolarity_Rising;
	TIM_ICinitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICinitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(TIM8, &TIM_ICinitStruct);
	
	TIM_Cmd(TIM8, ENABLE);
	
    //NVIC
    NVIC_initStruct.NVIC_IRQChannel                   = TIM1_CC_IRQn | TIM8_CC_IRQn;
	NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_initStruct.NVIC_IRQChannelSubPriority        = 1;
	NVIC_initStruct.NVIC_IRQChannelCmd                = ENABLE;
	NVIC_Init(&NVIC_initStruct);
	
    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);
    TIM_ITConfig(TIM1, TIM_IT_CC1, ENABLE);
    TIM_ITConfig(TIM1, TIM_IT_CC2, ENABLE);
    TIM_ITConfig(TIM8, TIM_IT_Update, ENABLE);
	TIM_ITConfig(TIM8, TIM_IT_CC2, ENABLE);
    TIM_ITConfig(TIM8, TIM_IT_CC3, ENABLE);
}

void TIM1_CC_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM1, TIM_IT_CC1) == SET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC1);
		if(w3_captureNumber == 0)
		{
			w3_readValue1 = readTIM1CapturedData1;
			w3_captureNumber = 1;
		}
		else if(w3_captureNumber == 1)
		{
			w3_readValue2 = TIM_GetCapture1(TIM1);
			if(TIM_GetITStatus(TIM1, TIM_IT_Update) == SET)
            {
                TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
                w3_Freq = 0;
            }
            else
            {
                w3_capture = (w3_readValue2 - w3_readValue1);
                if(w3_capture == 0) //Avoid divide by zero error
                {
                    w3_Freq = 0;
                }else
                {
                w3_Freq = (uint32_t) SystemCoreClock / w3_capture;
                }
            }
			w3_captureNumber = 0;
		}
	}
    else if(TIM_GetITStatus(TIM1, TIM_IT_CC2) == SET)
	{
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC2);
		if(w4_captureNumber == 0)
		{
			w4_readValue1 = readTIM1CapturedData2;
			w4_captureNumber = 1;
		}
		else if(w4_captureNumber == 1)
		{
			w4_readValue2 = TIM_GetCapture2(TIM1);
            if(TIM_GetITStatus(TIM1, TIM_IT_Update) == SET)
            {
                TIM_ClearITPendingBit(TIM1, TIM_IT_Update);
                w4_Freq = 0;
            }
            else
            {
                w4_capture = (w4_readValue2 - w4_readValue1);
                if(w4_capture == 0) //Avoid divide by zero error
                {
                    w4_Freq = 0;
                }else
                {
                    w4_Freq = (uint32_t) SystemCoreClock / w4_capture;
                }
            }
			w4_captureNumber = 0;
		}
	}
}

void TIM8_CC_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM8, TIM_IT_CC2) == SET)
	{
		TIM_ClearITPendingBit(TIM8, TIM_IT_CC2);
		if(w1_captureNumber == 0)
		{
			w1_readValue1 = readTIM8CapturedData2;
			w1_captureNumber = 1;
		}
		else if(w1_captureNumber == 1)
		{
			w1_readValue2 = TIM_GetCapture3(TIM8);
            if(TIM_GetITStatus(TIM8, TIM_IT_Update) == SET)
            {
                TIM_ClearITPendingBit(TIM8, TIM_IT_Update);
                w1_Freq = 0;
            }
            else
            {
                w1_capture = (w1_readValue2 - w1_readValue1);
                if(w1_capture == 0) //Avoid divide by zero error
                {
                    w1_Freq = 0;
                }else
                {
                    w1_Freq = (uint32_t) SystemCoreClock / w1_capture;
                }
            }
			w1_captureNumber = 0;
		}
	}
    else if(TIM_GetITStatus(TIM8, TIM_IT_CC3) == SET)
	{
		TIM_ClearITPendingBit(TIM8, TIM_IT_CC3);
		if(w2_captureNumber == 0)
		{
			w2_readValue1 = readTIM8CapturedData3;
			w2_captureNumber = 1;
		}
		else if(w2_captureNumber == 1)
		{
			w2_readValue2 = TIM_GetCapture3(TIM8);
            if(TIM_GetITStatus(TIM8, TIM_IT_Update) == SET)
            {
                TIM_ClearITPendingBit(TIM8, TIM_IT_Update);
                w2_Freq = 0;
            }
            else
            {
                w2_capture = (w2_readValue2 - w2_readValue1);
                if(w2_capture == 0) //Avoid divide by zero error
                {
                    w2_Freq = 0;
                }else
                {
                    w2_Freq = (uint32_t) SystemCoreClock / w2_capture;
                }
            }
            w2_captureNumber = 0;
		}
	}
}

uint32_t IC_getFreq(uint8_t channel)
{
	switch(channel)
	{
		case(1):
			return w1_Freq;
		case(2):
			return w2_Freq;
		case(3):
			return w3_Freq;
		case(4):
			return w4_Freq;
		default:
			return 0;
	}
}
