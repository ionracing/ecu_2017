#ifndef IO_MAPPING_ECU_REGULATOR_H
#define IO_MAPPING_ECU_REGULATOR_H

/* Constants for GPIOs that are used on the ECU */
#define PIN_RTDS PIN_LED1
#define GPIO_RTDS GPIO_LED1

#define PIN_PILOTLINE GPIO_Pin_0
#define GPIO_PILOTLINE GPIOC

#define PIN_VSI_RUN GPIO_Pin_2
#define GPIO_VSI_RUN GPIOC

#define PIN_BRAKELIGHT PIN_LED4
#define GPIO_BRAKELIGHT GPIO_LED4

#define PIN_FAN_RADIATOR GPIO_Pin_12
#define GPIO_FAN_RADIATOR GPIOB

#define PIN_FAN_EXHAUST GPIO_Pin_13
#define GPIO_FAN_EXHAUST  GPIOB

#define PIN_CAN1_EN GPIO_Pin_7
#define GPIO_CAN1_EN GPIOC

#define PIN_CAN2_EN GPIO_Pin_9
#define GPIO_CAN2_EN GPIOC

/* LEDs ---------------------------------------------------------*/
typedef enum
{
    LED1 = 0,
    LED2,
    LED3,
    LED4
} LEDS;

#define LED_GPIO_RCC(LED)   ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? RCC_AHB1Periph_GPIOE : RCC_AHB1Periph_GPIOE)
#define LED_GPIO_PIN(LED)   ((LED == LED1) ? GPIO_Pin_15 : (LED == LED2) ? GPIO_Pin_14 : (LED == LED3) ? GPIO_Pin_13 : (LED == LED4 ? GPIO_Pin_12 : GPIO_Pin_12))
#define LED_GPIO(LED)       ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? GPIOE : GPIOA)

/* PINs ---------------------------------------------------------*/
typedef enum
{
    KERS = 0,
    FLT,
    TEMP,
    EXTRA,
    BRAKE,
    DEBUG_LEDS,
    DASH_LEDS
} PINS;

// KERS
#define KERS_GPIO   GPIOB
#define KERS_PIN    GPIO_Pin_12

// FLT
#define FLT_GPIO    GPIOB
#define FLT_PIN     GPIO_Pin_13

// TEMP
#define TEMP_GPIO   GPIOB
#define TEMP_PIN    GPIO_Pin_14

// EXTRA
#define EXTRA_GPIO  GPIOB
#define EXTRA_PIN   GPIO_Pin_15

// BRAKE
#define BRAKE_GPIO  GPIOE
#define BRAKE_PIN   GPIO_Pin_1

// FAN OE
#define FAN_GPIO  GPIOE
#define FAN_PIN   GPIO_Pin_0

/* ADC ---------------------------------------------------------*/
#define WATER_TEMP_1_PIN        GPIO_Pin_4
#define WATER_TEMP_1_CHANNEL    ADC_Channel_14
#define WATER_TEMP_2_PIN        GPIO_Pin_5
#define WATER_TEMP_2_CHANNEL    ADC_Channel_15
#define WATER_TEMP_3_PIN        GPIO_Pin_0
#define WATER_TEMP_3_CHANNEL    ADC_Channel_8
#define AIR_TEMP_PIN            GPIO_Pin_1
#define AIR_TEMP_CHANNEL        ADC_Channel_9

/* CAN-bus mapping ----------------------------------------------*/
/*---CAN1---*/
#define GPIO_CAN1_TX                    GPIOB
#define GPIO_CAN1_RX                    GPIOB
#define PIN_CAN1_TX                     GPIO_Pin_9
#define PIN_CAN1_RX                     GPIO_Pin_8
/*---CAN2---*/
#define GPIO_CAN2_TX                    GPIOB
#define GPIO_CAN2_RX                    GPIOB
#define PIN_CAN2_TX                     GPIO_Pin_6
#define PIN_CAN2_RX                     GPIO_Pin_5

/* EEPROM -------------------------------------------------------*/
#define EEPROM_GPIO_RCC                 RCC_AHB1Periph_GPIOB
#define EEPROM_GPIO_MODE                GPIO_Mode_AF
#define EEPROM_GPIO_OTYPE               GPIO_OType_OD
#define EEPROM_GPIO_PIN_SCL             GPIO_Pin_6
#define EEPROM_GPIO_PIN_SDA             GPIO_Pin_7
#define EEPROM_GPIO_WP                  GPIO_Pin_5 //If GPIO_Pin_ALL then wp is disabled
#define EEPROM_GPIO_PUPD                GPIO_PuPd_NOPULL
#define EEPROM_GPIO_SPEED               GPIO_Speed_50MHz
#define EEPROM_GPIO_GPIOX               GPIOB
#define EEPROM_GPIO_PinSource_SCL       GPIO_PinSource6
#define EEPROM_GPIO_PinSource_SDA       GPIO_PinSource7
#define EEPROM_GPIO_AF_I2C              GPIO_AF_I2C1
#define EEPROM_I2C_RCC                  RCC_APB1Periph_I2C1
#define EEPROM_I2C_ACK                  I2C_Ack_Enable
#define EEPROM_I2C_ACKNOWLEDGEDADRESS   I2C_AcknowledgedAddress_7bit
#define EEPROM_I2C_CLOCKSPEED           100000
#define EEPROM_I2C_DUTYCYCLE            I2C_DutyCycle_2
#define EEPROM_I2C_MODE                 I2C_Mode_I2C
#define EEPROM_I2C_OWNADDRESS1          0x00
#define EEPROM_I2C_I2CX                 I2C1

/* ---COMLines--- */
#define COM_GPIO_RCC                RCC_AHB1Periph_GPIOD
#define COM_GPIO_OTYPE              GPIO_OType_PP
#define COM_GPIO_PIN_INN            GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11
#define COM_GPIO_PIN_OUT            GPIO_Pin_12 | GPIO_Pin_13
#define COM_GPIO_PIN_READY          GPIO_Pin_14
#define COM_GPIO_PIN_ACCEPTED       GPIO_Pin_15
#define COM_GPIO_PUPD               GPIO_PuPd_NOPULL
#define COM_GPIO_SPEED              GPIO_Speed_100MHz
#define COM_GPIO_GPIOX              GPIOD

/* ---CAN--- */
#define CAN_GPIO_RCC                RCC_AHB1Periph_GPIOB
#define CAN_GPIO_MODE               GPIO_Mode_AF
#define CAN_GPIO_OTYPE              GPIO_OType_PP
#define CAN_GPIO_PIN_RX             GPIO_Pin_8
#define CAN_GPIO_PIN_TX             GPIO_Pin_9
#define CAN_GPIO_PUPD               GPIO_PuPd_UP
#define CAN_GPIO_SPEED              GPIO_Speed_100MHz
#define CAN_GPIO_GPIOX              GPIOB
#define CAN_GPIO_PinSource_RX       GPIO_PinSource8
#define CAN_GPIO_PinSource_TX       GPIO_PinSource9
#define CAN_GPIO_AF_CAN             GPIO_AF_CAN1
#define CAN_CAN_RCC                 RCC_APB1Periph_CAN1
#define CAN_CANx                    CAN1
#define CAN_NVIC_IRQChannel         CAN1_RX0_IRQn
#define CAN_NVIC_IRQChannelPP       0
#define CAN_NVIC_IRQChannelSP       1
#define CAN_NVIC_IRQChannelCmd      ENABLE

#endif //IO_MAPPING_ECU_REGULATOR_H
