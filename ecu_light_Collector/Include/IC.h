#ifndef IC_H
#define IC_H

#include "stm32f4xx_exti.h"
#include "stm32f4xx_tim.h"

#define OVERFLOW (w1_readValue2 < w1_readValue1)
#define readTIM8CapturedData1 TIM8->CCR1
#define readTIM8CapturedData2 TIM8->CCR2
#define readTIM8CapturedData3 TIM8->CCR3
#define readTIM8CapturedData4 TIM8->CCR4

/*wn = wheel sensor n*/
static uint16_t w1_readValue1 = 0;
static uint16_t w1_readValue2 = 0;
static uint16_t w1_captureNumber = 0;
static uint32_t w1_capture = 0;
static uint32_t w1_Freq = 0;

static uint16_t w2_readValue1 = 0;
static uint16_t w2_readValue2 = 0;
static uint16_t w2_captureNumber = 0;
static uint32_t w2_capture = 0;
static uint32_t w2_Freq = 0;

static uint16_t w3_readValue1 = 0;
static uint16_t w3_readValue2 = 0;
static uint16_t w3_captureNumber = 0;
static uint32_t w3_capture = 0;
static uint32_t w3_Freq = 0;

static uint16_t w4_readValue1 = 0;
static uint16_t w4_readValue2 = 0;
static uint16_t w4_captureNumber = 0;
static uint32_t w4_capture = 0;
static uint32_t w4_Freq = 0;

void IC_initialise(void);
uint32_t IC_getFreq(uint8_t channel);

#endif //IC_H
