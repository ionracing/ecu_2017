#ifndef IO_MAPPING_H
#define IO_MAPPING_H

#if defined COMPILING_FOR_DEVBOARD
    //#warning "compiling for development board!"
    #include "IO_Mapping_Discovery.h"
#elif defined COMPILING_FOR_COLLECTOR
    #include "IO_Mapping_Collector.h"
#else 
    #error "please specify which board you are compiling for."
#endif

#endif
