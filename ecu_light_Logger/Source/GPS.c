/**************************************************
*   GPS                                           *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "GPS.h"

/**
 * We are using Adafruit Ultimate GPS Breakout that you can find here.
 * https://www.adafruit.com/products/746
 *
 * We are using USART2 and are connected like this.
 * Gps	-	stm32f4xx
 * 3.3V	- 	Vdd
 * Gnd 	- 	Gnd
 * RX	-	PA2
 * TX 	- 	PA3
 *
 * We use 3.3V instead of 5V.
 *
 */

uint8_t RxByte1 = 0;
uint8_t GPS_Initialise()
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
	
    /*---Clock---*/
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

    /* Configure the GPIO pins */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);

	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USART3, &USART_InitStruct);

	USART_Cmd(USART3, ENABLE);
    
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
    
    NVIC_InitStruct.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStruct);

    for(int i = 0;i < 0x1FFFFF;i++);
    
    USART_Write_String(USART3, PMTK_SET_NMEA_OUTPUT_RMCONLY);
    for(int i = 0;i < 0x1FFFFF;i++);
    USART_Write_String(USART3, PMTK_SET_NMEA_UPDATE_10HZ);
    for(int i = 0;i < 0x1FFFFF;i++);
    USART_Write_String(USART3, PMTK_API_SET_FIX_CTL_5HZ);
    
    return 1;
}

char gps_data[100];
char ACK[] = "$PMTK";
char ID[6] = "";
uint8_t GPS_Success(uint8_t turns)
{    
    int i = 0;
    while(turns--)
    {
        i = 0;
        do
        {
            while(USART_GetFlagStatus(USART3, USART_FLAG_RXNE) != SET);
            USART_ClearFlag(USART3, USART_FLAG_RXNE);
            gps_data[i] = USART_ReceiveData(USART3);
            i++;
        }
        while(gps_data[i-1] != 0x0A); // checking for \n.
        
        strncpy(ID, gps_data, 5);
        if(strcmp(ID, ACK) == 0)
        {
            if(gps_data[13] == '3') // checking Ack Flag. 3 eq success, else fail.
                return 1;
            else
                return 0;
        }
    }
    return  0;
}

uint8_t GPS_Check(char *chr)
{
    int tries = 0;
    do
    {
        if(tries >= 2)
            return 0;
        USART_Write_String(USART3, chr);
        tries++;
    }
    while(!GPS_Success(5));
    return 1;
}

void GPS_Read(char *chr)
{
    uint8_t length = 0, lengthCord = 0, index = 0;
    uint64_t datetime;
    uint32_t time, latitude, longitude, speed, course, date;
    uint8_t data[8] = {0};
    
    while(*chr != '\0')
    {
        while(*(chr) != ',')
        {
            length++;
            if(*chr == '\0')
                break;
            chr++;
        }
        
        if(length == 0)
        {
            length = 0;
            index++;
            chr++;
            continue;
        }
        
        switch(index)
        {
            case TYPE:
                break;
            case TIME:
                time = GPS_Parse(chr, length);
                break;
            case VALID:
                if(*chr == 'V')
                    data[0] = 0;
                else if(*chr == 'A')
                    data[0] = 1;
                CAN_Write_8(CAN1, GPS_VALIDITY, 1, data);
                break;
            case LAT:
                lengthCord = length;
                break;
            case LAT_DIR:
                latitude = GPS_ParseCord(chr, lengthCord);
                data[0] = (0xFF & latitude);
                data[1] = (0xFF & (latitude >> 8));
                data[2] = (0xFF & (latitude >> 16));
                data[3] = (0xFF & (latitude >> 24));
                CAN_Write_8(CAN1, GPS_LATITUDE, 4, data);
                break;
            case LON:
                lengthCord = length;
                break;
            case LON_DIR:
                longitude = GPS_ParseCord(chr, lengthCord);
                data[0] = (0xFF & longitude);
                data[1] = (0xFF & (longitude >> 8));
                data[2] = (0xFF & (longitude >> 16));
                data[3] = (0xFF & (longitude >> 24));
                CAN_Write_8(CAN1, GPS_LONGITUDE, 4, data);
                break;
            case SPEED:
                speed = GPS_Parse(chr, length);
                data[0] = (0xFF & speed);
                data[1] = (0xFF & (speed >> 8));
                data[2] = (0xFF & (speed >> 16));
                data[3] = (0xFF & (speed >> 24));
                CAN_Write_8(CAN1, GPS_SPEED, 4, data);
                break;
            case COURSE:
                course = GPS_Parse(chr, length);
                data[0] = (0xFF & course);
                data[1] = (0xFF & (course >> 8));
                data[2] = (0xFF & (course >> 16));
                data[3] = (0xFF & (course >> 24));
                CAN_Write_8(CAN1, GPS_COURSE, 4, data);
                break;
            case DATE:
                date = GPS_Parse(chr, length);
                datetime = DATETIME_intToTime(date, time);
                data[0] = (0xFF & datetime);
                data[1] = (0xFF & (datetime >> 8));
                data[2] = (0xFF & (datetime >> 16));
                data[3] = (0xFF & (datetime >> 24));
                CAN_Write_8(CAN1, GPS_TIME_LSB, 4, data);
                data[0] = (0xFF & (datetime >> 32));
                data[1] = (0xFF & (datetime >> 40));
                data[2] = (0xFF & (datetime >> 48));
                data[3] = (0xFF & (datetime >> 56));
                CAN_Write_8(CAN1, GPS_TIME_MSB, 4, data);
                break;
            case NADA1:
            case NADA2:
                break;
            case GPS_CRC:
                break;
            default:
                break;
        }
        
        if(*chr == '\0')
                break;
    
        length = 0;
        index++;
        chr++;
    }
}

uint32_t GPS_Parse(char *chr, uint8_t length)
{
    uint32_t value = 0, multi = 1;
    uint8_t des = 0;
    int i;
    chr--;
    
    for(i = 0;i < length;i++)
    {
        if(*chr != '.')
        {
            value += ((*chr) - '0') * multi;
            multi *= 10;
        }
        else
            des = i;
            
        chr--;
    }
    
    if(des == 0)
        value *= 10000;
    else if(des == 1)
        value *= 1000;
    else if(des == 2)
        value *= 100;
    else if(des == 3)
        value *= 10;
    return value;
}

uint32_t GPS_ParseCord(char *chr, uint8_t length)
{
    uint8_t dir = 0;
    uint32_t cord;
    uint32_t deg = 0; 
    uint32_t min = 0;
    uint32_t sek = 0;
    
    chr--;
    if(*chr == 'N' || *chr == 'E')
        dir = 0;
    else if(*chr == 'S' || *chr == 'W')
        dir = 1;
        
    chr--;
    cord = GPS_Parse(chr, length);
    
    deg = cord / 1000000;
    min = ((cord) / 10000) - (deg * 100);
    sek = cord - (deg * 1000000) - (min * 10000);
    
    return ((dir << 31) + (sek * 600000) + (min * 10000) + sek);
}

/*
//used for creating files.
char* GPS_getDateTime()
{
    put(dateTime, '.');
    put(dateTime, 't');
    put(dateTime, 'x');
    put(dateTime, 't');
    return dateTime;
}
*/
