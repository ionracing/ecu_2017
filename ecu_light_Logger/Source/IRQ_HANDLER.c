#include "IRQ_HANDLER.h"

char chr[200] = {0};

void USART3_IRQHandler(void){
	__disable_irq();
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
	{
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
        
        int i = 0;
        
        chr[i++] = USART_ReceiveData(USART3);
        while(chr[i-1] != '\n')
        {
            chr[i++] = USART_ReceiveData(USART3);
        }
        chr[i] = '\0';
        
        if(chr[0] == '$' && chr[1] == 'G' && chr[2] == 'P' && chr[3] == 'R' && chr[4] == 'M' && chr[5] == 'C')
            GPS_Read(chr);
	}
    __enable_irq();
}
