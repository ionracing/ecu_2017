/**************************************************
*   USART                                         *
*                                                 *
***************************************************/

/*********************INCLUDES*********************/
#include "USART.h"

/*midlertidig defines for at koden skal kompilere*/
#define RPI_DATA 0x000
#define CAN_RPI_DATA 0x001
uint8_t cal_break_data;

volatile uint16_t GPS_New_Data = 0;

char USARTData[100];
volatile int p = 0;


void USART_initialise(USART_TypeDef* USARTx)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
    USART_InitTypeDef USART_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;
	
    /*---Clock---*/
    RCC_AHB1PeriphClockCmd(USART_RCC_GPIO(USARTx), ENABLE);
    USARTx == USART1 ? RCC_APB2PeriphClockCmd(USART_RCC_USART(USARTx), ENABLE) : RCC_APB1PeriphClockCmd(USART_RCC_USART(USARTx), ENABLE);

    /* Configure the GPIO pins */
    GPIO_InitStructure.GPIO_Pin = USART_GPIO_Pin_Tx(USARTx) | USART_GPIO_Pin_Rx(USARTx);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(USART_GPIOx(USARTx), &GPIO_InitStructure);
    
    GPIO_PinAFConfig(USART_GPIOx(USARTx), USART_GPIO_PinSource_Tx(USARTx), USART_GPIO_AF_USARTx(USARTx)); //Tx -> Rx.
	GPIO_PinAFConfig(USART_GPIOx(USARTx), USART_GPIO_PinSource_Rx(USARTx), USART_GPIO_AF_USARTx(USARTx)); //Rx -> Tx.

	USART_InitStruct.USART_BaudRate = USART_USART_BaudRate(USARTx);
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_Init(USARTx, &USART_InitStruct);

	USART_Cmd(USARTx, ENABLE);
    
    NVIC_InitStruct.NVIC_IRQChannel = USART_NVIC_IRQChannel(USARTx);
    NVIC_InitStruct.NVIC_IRQChannelCmd = USART_NVIC_Enable(USARTx);
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = USART_NVIC_IRQChannelPP(USARTx);
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = USART_NVIC_IRQChannelSP(USARTx);
    NVIC_Init(&NVIC_InitStruct);
}


/**
  * @brief 
  * @param 
  * @retval
  */
void USART_Write_8(USART_TypeDef* USARTx, uint16_t Data)
{
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
    USART_ClearFlag(USARTx, USART_FLAG_TC);
	USART_SendData(USARTx, Data);
}

/**
  * @brief 
  * @param 
  * @retval
  */
void USART_Write_16(USART_TypeDef* USARTx, uint16_t Data)
{
    USART_Write_8(USARTx, Data & 0xFF); //LSB
    USART_Write_8(USARTx, (Data >> 8) & 0xFF); //MSB
}

/**
  * @brief 
  * @param 
  * @retval
  */
void USART_Write_String(USART_TypeDef* USARTx, char *chr)
{
    while(*chr != '\n')
    {
        USART_Write_8(USARTx, *chr);
        chr++;
    }
    USART_Write_8(USARTx, '\n');
}

/**
  * @brief 
  * @param 
  * @retval
  */
void USART_Rpi_Read(uint8_t address, uint8_t data1, uint8_t data2)
{
	if(address == RPI_DATA)
    {
		uint8_t data[8];
		data[0] = data2;
		
		CAN_Write_8(CAN1, CAN_RPI_DATA, 1, data);
	}
}

/**
  * @brief 
  * @param 
  * @retval
  */
void USART_Rpi_Send(USART_TypeDef* USARTx, uint8_t address, uint8_t data1, uint8_t data2)
{
	USART_Write_8(USARTx, 0xFF);
	USART_Write_8(USARTx, 0xFF);
	USART_Write_8(USARTx, address);
	USART_Write_8(USARTx, data2);
	USART_Write_8(USARTx, data1);
}
