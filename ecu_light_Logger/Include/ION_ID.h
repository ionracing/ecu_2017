#ifndef ION_ID_H
#define ION_ID_H

#define BTN_STOP            (uint16_t)0x64
#define BTN_START           (uint16_t)0x65
#define BTN_MODE            (uint16_t)0x66
#define BTN_SD              (uint16_t)0x67

#define IMU_RATE            (uint16_t)0x68
#define IMU_ACC_X           (uint16_t)0x69
#define IMU_ACC_Y           (uint16_t)0x6A
#define IMU_ACC_Z           (uint16_t)0x6B
#define IMU_TEMP            (uint16_t)0x6C

#define GPS_TIME_LSB        (uint16_t)0x70
#define GPS_TIME_MSB        (uint16_t)0x71
#define GPS_VALIDITY        (uint16_t)0x72
#define GPS_LATITUDE        (uint16_t)0x73
#define GPS_LONGITUDE       (uint16_t)0x74
#define GPS_SPEED           (uint16_t)0x75
#define GPS_COURSE          (uint16_t)0x76

#define ADC_WATERTEMP_1     (uint16_t)0x90
#define ADC_WATERTEMP_2     (uint16_t)0x91
#define ADC_WATERTEMP_3     (uint16_t)0x92
#define ADC_AIRTEMP         (uint16_t)0x93

#define TRACTION_CONTROL    (uint16_t)0x8B
#endif
