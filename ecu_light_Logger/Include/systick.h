/* Include guard ---------------------------------- */
#ifndef SYSTICK_H
#define SYSTICK_H

/* Includes ---------------------------------------- */
#include "stm32f4xx.h"

#define CLK_COMPLETE  0
#define CLK_RESET 1

void systick_init(void);

extern volatile uint16_t clk1000ms;
extern volatile uint16_t clk100ms;
extern volatile uint16_t clk10ms;
extern volatile uint16_t clk5000ms;

extern volatile uint8_t MODE_FLAG;
extern volatile uint8_t SD_FLAG;
extern volatile uint8_t LOGDATA_FLAG;
extern volatile uint8_t LED_FLAG;

#endif //SYSTICK_H
