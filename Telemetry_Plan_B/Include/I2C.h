#ifndef I2C_H
#define I2C_H

#include "io_mapping_ECU.h"
#include "stm32f4xx_i2c.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

void I2C_Write_8(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t data);
void I2C_Write_Page(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t *data, uint8_t length);
uint8_t I2C_Read_Next(I2C_TypeDef* I2Cx, uint8_t deviceAddress);
uint8_t I2C_Read_8(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress);
void I2C_Read_Page(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t* data, uint8_t length);
uint8_t I2C_Check(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg, uint8_t check);

#endif
