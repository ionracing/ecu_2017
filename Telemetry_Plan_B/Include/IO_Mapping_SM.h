#ifndef IO_MAPPING_ECU_REGULATOR_H
#define IO_MAPPING_ECU_REGULATOR_H

/* LEDs ---------------------------------------------------------*/
typedef enum
{
    LED1 = 0,
    LED2,
    LED3,
    LED4
} LEDS;

#define LED_GPIO_RCC(LED)   ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? RCC_AHB1Periph_GPIOD : 0)
#define LED_GPIO_PIN(LED)   ((LED == LED1) ? GPIO_Pin_0 : (LED == LED2) ? GPIO_Pin_2 : (LED == LED3) ? GPIO_Pin_1 : (LED == LED4 ? GPIO_Pin_3 : 0))
#define LED_GPIO(LED)       ((LED == LED1 | LED == LED2 | LED == LED3 | LED == LED4) ? GPIOD : GPIOD)

/* PINs ---------------------------------------------------------*/
// BRAKE
#define BRAKE_GPIO  GPIOD
#define BRAKE_PIN   GPIO_Pin_5

// RTDS
#define RTDS_GPIO GPIOD
#define RTDS_PIN GPIO_Pin_6

/* ADC ---------------------------------------------------------*/
#define BP_PIN          GPIO_Pin_0
#define BP_CHANNEL      ADC_Channel_0
#define GP_PIN          GPIO_Pin_1
#define GP_CHANNEL      ADC_Channel_1

/* BTNs ---------------------------------------------------------*/

typedef enum
{
    START = 0,
    STOP
} BUTTONS;
#define BTN_GPIO_RCC(BTN)               (BTN == START   ? RCC_AHB1Periph_GPIOB : \
                                        (BTN == STOP    ? RCC_AHB1Periph_GPIOC : 0))
#define BTN_GPIO_Pin(BTN)               (BTN == START   ? GPIO_Pin_12 : \
                                        (BTN == STOP    ? GPIO_Pin_6 : 0))
#define BTN_GPIOx(BTN)                  (BTN == START   ? GPIOB : \
                                        (BTN == STOP    ? GPIOC : (0 == 0 ? GPIOA : GPIOA)))
#define BTN_EXTI_PortSource(BTN)        (BTN == START   ? EXTI_PortSourceGPIOB : \
                                        (BTN == STOP    ? EXTI_PortSourceGPIOC : 0))
#define BTN_EXTI_PinSource(BTN)         (BTN == START   ? EXTI_PinSource12 : \
                                        (BTN == STOP    ? EXTI_PinSource6 : 0))
#define BTN_EXTI_Linex(BTN)             (BTN == START   ? EXTI_Line12 : \
                                        (BTN == STOP    ? EXTI_Line6 : 0))
#define BTN_NVIC_Enable(BTN)            (BTN == START   ? ENABLE : \
                                        (BTN == STOP    ? ENABLE : (0 == 0 ? DISABLE : DISABLE)))
#define BTN_NVIC_IRQChannel(BTN)        (BTN == START   ? EXTI15_10_IRQn : \
                                        (BTN == STOP    ? EXTI9_5_IRQn : 0))
#define BTN_NVIC_IRQChannelPP(BTN)      (BTN == START   ? 0 : \
                                        (BTN == STOP    ? 0 : 0))
#define BTN_NVIC_IRQChannelSP(BTN)      (BTN == START   ? 1 : \
                                        (BTN == STOP    ? 1 : 0))

/* ---CAN--- */
#define CAN_GPIO_RCC                RCC_AHB1Periph_GPIOB
#define CAN_GPIO_MODE               GPIO_Mode_AF
#define CAN_GPIO_OTYPE              GPIO_OType_PP
#define CAN_GPIO_PIN_RX             GPIO_Pin_8
#define CAN_GPIO_PIN_TX             GPIO_Pin_9
#define CAN_GPIO_PUPD               GPIO_PuPd_UP
#define CAN_GPIO_SPEED              GPIO_Speed_100MHz
#define CAN_GPIO_GPIOX              GPIOB
#define CAN_GPIO_PinSource_RX       GPIO_PinSource8
#define CAN_GPIO_PinSource_TX       GPIO_PinSource9
#define CAN_GPIO_AF_CAN             GPIO_AF_CAN1
#define CAN_CAN_RCC                 RCC_APB1Periph_CAN1
#define CAN_CANx                    CAN1
#define CAN_NVIC_IRQChannel         CAN1_RX0_IRQn
#define CAN_NVIC_IRQChannelPP       0
#define CAN_NVIC_IRQChannelSP       1
#define CAN_NVIC_IRQChannelCmd      ENABLE

/* ---USART--- */
#define USART_RCC_USART(USARTx)             ((USARTx == USART1) ? RCC_APB2Periph_USART1 : \
                                            ((USARTx == USART2) ? RCC_APB1Periph_USART2 : \
                                            ((USARTx == USART3) ? RCC_APB1Periph_USART3 : RCC_APB1Periph_USART3)))
#define USART_RCC_GPIO(USARTx)              ((USARTx == USART1) ? RCC_AHB1Periph_GPIOA : \
                                            ((USARTx == USART2) ? RCC_AHB1Periph_GPIOD : \
                                            ((USARTx == USART3) ? RCC_AHB1Periph_GPIOD : RCC_AHB1Periph_GPIOD)))
#define USART_GPIO_Pin_Tx(USARTx)           ((USARTx == USART1) ? GPIO_Pin_9 : \
                                            ((USARTx == USART2) ? GPIO_Pin_5 : \
                                            ((USARTx == USART3) ? GPIO_Pin_8 : GPIO_Pin_8)))
#define USART_GPIO_Pin_Rx(USARTx)           ((USARTx == USART1) ? GPIO_Pin_10 : \
                                            ((USARTx == USART2) ? GPIO_Pin_6 : \
                                            ((USARTx == USART3) ? GPIO_Pin_9 : GPIO_Pin_9)))
#define USART_GPIOx(USARTx)                 ((USARTx == USART1) ? GPIOA : \
                                            ((USARTx == USART2) ? GPIOD : \
                                            ((USARTx == USART3) ? GPIOD : GPIOD)))
#define USART_GPIO_PinSource_Tx(USARTx)     ((USARTx == USART1) ? GPIO_PinSource9 : \
                                            ((USARTx == USART2) ? GPIO_PinSource5 : \
                                            ((USARTx == USART3) ? GPIO_PinSource8 : GPIO_PinSource8)))
#define USART_GPIO_PinSource_Rx(USARTx)     ((USARTx == USART1) ? GPIO_PinSource10 : \
                                            ((USARTx == USART2) ? GPIO_PinSource6 : \
                                            ((USARTx == USART3) ? GPIO_PinSource9 : GPIO_PinSource9)))
#define USART_GPIO_AF_USARTx(USARTx)        ((USARTx == USART1) ? GPIO_AF_USART1 : \
                                            ((USARTx == USART2) ? GPIO_AF_USART2 : \
                                            ((USARTx == USART3) ? GPIO_AF_USART3 : GPIO_AF_USART3)))
#define USART_USART_BaudRate(USARTx)        ((USARTx == USART1) ? 9600 : \
                                            ((USARTx == USART2) ? 115200 : \
                                            ((USARTx == USART3) ? 9600 : 9600)))
#define USART_NVIC_Enable(USARTx)           ((USARTx == USART1) ? ENABLE : \
                                            ((USARTx == USART2) ? ENABLE : \
                                            ((USARTx == USART3) ? ENABLE : ENABLE)))
#define USART_NVIC_IRQChannel(USARTx)       ((USARTx == USART1) ? USART1_IRQn : \
                                            ((USARTx == USART2) ? USART2_IRQn : \
                                            ((USARTx == USART3) ? USART3_IRQn : USART3_IRQn)))
#define USART_NVIC_IRQChannelPP(USARTx)     ((USARTx == USART1) ? 0 : \
                                            ((USARTx == USART2) ? 0 : \
                                            ((USARTx == USART3) ? 0 : 0)))
#define USART_NVIC_IRQChannelSP(USARTx)     ((USARTx == USART1) ? 1 : \
                                            ((USARTx == USART2) ? 1 : \
                                            ((USARTx == USART3) ? 1 : 1)))

#endif //IO_MAPPING_SM_H
