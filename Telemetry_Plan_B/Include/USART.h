/* Include guard ---------------------------------- */
#ifndef USART_H
#define USART_H

/* Includes ---------------------------------------- */
#include "io_mapping.h"
#include "CAN.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "misc.h"

extern char USARTData[100];
extern volatile uint16_t GPS_New_Data;

typedef struct {
    USART_TypeDef*      USARTx;
    uint32_t            RCC_USART;
    uint32_t            RCC_GPIO;
    uint16_t            GPIO_Pin_Tx;
    uint16_t            GPIO_Pin_Rx;
    GPIO_TypeDef*       GPIOx;
    uint8_t             GPIO_PinSource_Tx;
    uint8_t             GPIO_PinSource_Rx;
    uint8_t             GPIO_AF_USARTx;
    uint16_t            USART_BaudRate;
    FunctionalState     NVIC_Enable;
    uint8_t             NVIC_IRQChannel;
    uint8_t             NVIC_IRQChannelPreemptionPriority;
    uint8_t             NVIC_IRQChannelSubPriority;
} USART_Struct;

void USART_initialise(USART_TypeDef* Struct);
void USART_Write_8(USART_TypeDef* USARTx, uint16_t Data);
void USART_Write_16(USART_TypeDef* USARTx, uint16_t Data);
void USART_Write_String(USART_TypeDef* USARTx, char *chr);
void usartRpiRead(uint8_t address, uint8_t data1, uint8_t data2);
void USART_Rpi_Send(USART_TypeDef* USARTx, uint8_t address, uint8_t data1, uint8_t data2);

#endif //USART_H
