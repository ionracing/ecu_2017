/* Include guard ---------------------------------- */
#ifndef LED_H
#define LED_H

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "io_mapping_SM.h"

void LED_Initialise(uint16_t LEDx);
void LEDx_On(uint16_t LEDx);
void LEDx_Off(uint16_t LEDx);
void LEDx_Toggle(uint16_t LEDx);


#endif //LED_H
