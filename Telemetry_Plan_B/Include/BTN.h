#ifndef BTN_H
#define BTN_H

#include "stm32f4xx_gpio.h"
#include "IO_Mapping.h"

typedef enum
{
    BTN_LOW = 0,
    BTN_HIGH = 1
} BTN_STATUS;

void BTN_Initialise(uint16_t BTNx);
uint8_t BTN_Status(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif
