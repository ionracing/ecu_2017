#include "I2C.h"
#include "LED.h"

void I2C_Write_8(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t data)
{
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)){}
    
	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)){}
        
	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}
    
	I2C_SendData(I2Cx, wordAddress & 0xFF);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
        
    I2C_SendData(I2Cx, (wordAddress >> 8) & 0x7F);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
    I2C_SendData(I2Cx, data & 0xFF);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
    I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)){}
}

void I2C_Write_Page(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t *data, uint8_t length)
{
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}

	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}
    
    I2C_SendData(I2Cx, (wordAddress >> 8) & 0x7F);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
	I2C_SendData(I2Cx, wordAddress & 0xFF);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
    for(int i = 0;i < length;i++)
    {
        I2C_SendData(I2Cx, data[i] & 0xFF);
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    }
	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)){}
}

uint8_t I2C_Read_Next(I2C_TypeDef* I2Cx, uint8_t deviceAddress)
{
	uint8_t data = 0;

	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)){}

    I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Next);
	I2C_AcknowledgeConfig(I2Cx, DISABLE);
    
	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Receiver);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)){}
    
	//not sure if this has been fixed or not.
	//need to add a wait here for new data, right now it goes to fast to get the new data.
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE)){}
	data = I2C_ReceiveData(I2Cx);
    //while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));

	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)){}

	return data;
}

uint8_t I2C_Read_8(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress)
{
	uint8_t data = 0;

	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)){}

	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}

	I2C_SendData(I2Cx, (wordAddress >> 8) & 0x7F);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
	I2C_SendData(I2Cx, wordAddress & 0xFF);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}

	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Receiver);
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)){}

	I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
	I2C_AcknowledgeConfig(I2Cx, DISABLE);

    //not sure if this has been fixed or not.
	//need to add a wait here for new data, right now it goes to fast to get the new data.
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE)){}
	data = I2C_ReceiveData(I2Cx);
	//while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));

	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)){}

	return data;
}

void I2C_Read_Page(I2C_TypeDef* I2Cx, uint8_t deviceAddress, uint16_t wordAddress, uint8_t* data, uint8_t length)
{
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)){}

	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}

	I2C_SendData(I2Cx, (wordAddress >> 8) & 0x7F);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}
    
	I2C_SendData(I2Cx, wordAddress & 0xFF);
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){}

	I2C_GenerateSTART(I2Cx, ENABLE);
	while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_SB)){}

	I2C_Send7bitAddress(I2Cx, deviceAddress << 1, I2C_Direction_Receiver);
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)){}

    for(int i = 0; i < length;i++)
    {
        //not sure if this has been fixed or not.
        //need to add a wait here for new data, right now it goes to fast to get the new data.
        while(!I2C_GetFlagStatus(I2Cx, I2C_FLAG_RXNE)){}
        data[i] = I2C_ReceiveData(I2Cx);
        for(int i = 0;i < 0xFFFFF;i++);
        //while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED));
        
        if(i == (length - 2))
        {
            I2C_NACKPositionConfig(I2Cx, I2C_NACKPosition_Current);
            I2C_AcknowledgeConfig(I2Cx, DISABLE);
        }
    }
	I2C_GenerateSTOP(I2Cx, ENABLE);
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_STOPF)){}
}

uint8_t I2C_Check(I2C_TypeDef* I2Cx, uint8_t addr, uint8_t reg, uint8_t check)
{
    uint8_t mem = I2C_Read_8(I2Cx, addr, reg);
    if(mem == check)
        return 1;
    return 0;
}
