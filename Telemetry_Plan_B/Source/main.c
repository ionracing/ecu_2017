//#include "IO_Mapping_SM.h"
#include "IO_Mapping.h"
#include "LED.h"
#include "CAN.h"
#include "IO.h"
#include "systick.h"
#include "watchdog.h"
#include "USART.h"

int main(void)
{
    SystemCoreClockUpdate();
    
    // ---INCLDUES--- //
    LED_Initialise(LED1); //LED
    LED_Initialise(LED2); //LED
    LED_Initialise(LED3); //LED
    LED_Initialise(LED4); //LED
    USART_initialise(USART2);
    IO_initPinOut(GPIOC, GPIO_Pin_10); // CAN enable pin
    GPIO_SetBits(GPIOC, GPIO_Pin_10);
    CAN_Initialise();
    watchdog_init();

    while(1)
    {
        watchdog_pet();
        LEDx_Toggle(LED2);
    }
}
