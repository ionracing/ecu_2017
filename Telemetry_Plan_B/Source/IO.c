#include "IO.h"

void IO_initPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode, GPIOOType_TypeDef GPIO_OType, GPIOPuPd_TypeDef GPIO_PuPd, GPIOSpeed_TypeDef GPIO_Speed)
{
    GPIO_InitTypeDef  GPIO_InitStructure;
  
    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(IO_GPIO_RCC(GPIOx), ENABLE);

    /* Configure the GPIO_LED pins */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode;
    GPIO_InitStructure.GPIO_OType = GPIO_OType;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}

void IO_initPinOut(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    IO_initPin(GPIOx, GPIO_Pin, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz);
}

void IO_initPinIn(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
    IO_initPin(GPIOx, GPIO_Pin, GPIO_Mode_IN, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz);
}
