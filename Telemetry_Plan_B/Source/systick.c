
/*---INCLUDES---*/
#include "systick.h"

volatile uint16_t clk1000ms=CLK_RESET; 
volatile uint16_t clk100ms=CLK_RESET;
volatile uint16_t clk2000ms=CLK_RESET;

volatile uint8_t BRAKE_FLAG;
volatile uint8_t GAS_FLAG;
volatile uint8_t LED1_FLAG;
volatile uint8_t LED2_FLAG;


void systick_init(void)
{
	SysTick_Config(SystemCoreClock / 1000);
}

void SysTick_Handler(void)
{
    if (clk2000ms != CLK_COMPLETE) // 2 second
    {
		clk2000ms++;
		if (clk2000ms >= 2001)
            clk2000ms = CLK_COMPLETE;
	}
	
	if (clk1000ms != CLK_COMPLETE) // 1 second
    {
		clk1000ms++;
		if (clk1000ms >= 1001)
        {
            clk1000ms = CLK_COMPLETE;
            LED1_FLAG = SET;
            LED2_FLAG = SET;
        }
	}

	if (clk100ms != CLK_COMPLETE) // 0.1 seconds
    {
		clk100ms++;
		if (clk100ms >= 101)
        {
            clk100ms = CLK_COMPLETE;
            BRAKE_FLAG = SET;
            GAS_FLAG = SET;
        }
	}
}
