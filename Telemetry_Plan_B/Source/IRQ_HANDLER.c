#include "IRQ_HANDLER.h"

CanRxMsg msgRx;
void CAN1_RX0_IRQHandler (void)
{
	__disable_irq();
	if(CAN1->RF0R & CAN_RF0R_FMP0) //checks if a message is pending
	{
        LEDx_Toggle(LED1);
        CAN_Receive(CAN1, 0, &msgRx);
        USART_Write_8(USART2, 0xFF);
        USART_Write_8(USART2, 0xFF);
        USART_Write_8(USART2, (0xFF & (msgRx.StdId >> 8)));
        USART_Write_8(USART2, 0xFF & msgRx.StdId);
        USART_Write_8(USART2, msgRx.DLC >= 1 ? msgRx.Data[0] : 0);
        USART_Write_8(USART2, msgRx.DLC >= 2 ? msgRx.Data[1] : 0);
        USART_Write_8(USART2, msgRx.DLC >= 3 ? msgRx.Data[2] : 0);
        USART_Write_8(USART2, msgRx.DLC >= 4 ? msgRx.Data[3] : 0);
        if(msgRx.DLC > 4)
        {
            USART_Write_8(USART2, 0xFF);
            USART_Write_8(USART2, 0xFF);
            USART_Write_8(USART2, (0xFF & (msgRx.StdId >> 8)));
            USART_Write_8(USART2, 0xFF & msgRx.StdId);
            USART_Write_8(USART2, msgRx.DLC >= 5 ? msgRx.Data[4] : 0);
            USART_Write_8(USART2, msgRx.DLC >= 6 ? msgRx.Data[5] : 0);
            USART_Write_8(USART2, msgRx.DLC >= 7 ? msgRx.Data[6] : 0);
            USART_Write_8(USART2, msgRx.DLC >= 8 ? msgRx.Data[7] : 0);
        }
	}
	__enable_irq();
}
