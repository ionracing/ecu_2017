//  ___ ___  _   _   ____            _             
// |_ _/ _ \| \ | | |  _ \ __ _  ___(_)_ __   __ _ 
//  | | | | |  \| | | |_) / _` |/ __| | '_ \ / _` |
//  | | |_| | |\  | |  _ < (_| | (__| | | | | (_| |
// |___\___/|_| \_| |_| \_\__,_|\___|_|_| |_|\__, |
//                                           |___/ 
// Fredrik Wigsnes 2017

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "watchdog.h"
#include "BTN.h"
#include "LED.h"
#include "GPS.h"
#include "IO_Mapping.h"
#include "ION_ID.h"
#include "systick.h"
#include "ADC_SIMPLE.h"

void initialize(void);
void logSensors(void);

uint8_t data[8] = {1};
BitAction modeStatus = Bit_RESET;
BitAction SdStatus = Bit_RESET;

uint16_t waterTemp_1_Val = 0;
uint16_t waterTemp_2_Val = 0;
uint16_t waterTemp_3_Val = 0;
uint16_t ariTemp_Val = 0;

int main(void)
{
    SystemCoreClockUpdate(); //MUST BE HERE.
    
    initialize();
    
    while(1)
    {
        if(LED_FLAG == SET)
        {
            LED_FLAG = RESET;
            LEDx_Toggle(LED1);
        }
        if(LOGDATA_FLAG == SET)
        {
            LOGDATA_FLAG = RESET;
            logSensors();
        }
        if(clk1000ms == CLK_COMPLETE)
            clk1000ms = CLK_RESET;
        if(clk100ms == CLK_COMPLETE)
            clk100ms = CLK_RESET;
        
        watchdog_pet();
    }
}

void initialize()
{
    LED_Initialise(LED1);
    systick_init();
    CAN_Initialise();
    ADC_SIMPLE_init(GPIOC, WATER_TEMP_1_PIN);
    ADC_SIMPLE_init(GPIOC, WATER_TEMP_2_PIN);
    ADC_SIMPLE_init(GPIOB, WATER_TEMP_3_PIN);
    ADC_SIMPLE_init(GPIOB, AIR_TEMP_PIN);
    GPS_Initialise();
    watchdog_init();
}

void logSensors()
{
    waterTemp_1_Val = ADC_SIMPLE_readChannel(WATER_TEMP_1_CHANNEL);
    waterTemp_2_Val = ADC_SIMPLE_readChannel(WATER_TEMP_2_CHANNEL);
    waterTemp_3_Val = ADC_SIMPLE_readChannel(WATER_TEMP_3_CHANNEL);
    ariTemp_Val = ADC_SIMPLE_readChannel(AIR_TEMP_CHANNEL);
    
    CAN_Write_16_bit(CAN1, ADC_WATERTEMP_1, waterTemp_1_Val);
    CAN_Write_16_bit(CAN1, ADC_WATERTEMP_2, waterTemp_1_Val);
    CAN_Write_16_bit(CAN1, ADC_WATERTEMP_3, waterTemp_1_Val);
    CAN_Write_16_bit(CAN1, ADC_AIRTEMP, waterTemp_1_Val);
}
