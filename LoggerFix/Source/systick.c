
/*---INCLUDES---*/
#include "systick.h"

volatile uint16_t clk1000ms=CLK_RESET; 
volatile uint16_t clk100ms=CLK_RESET;
volatile uint16_t clk10ms=CLK_RESET; 
volatile uint16_t clk5000ms=CLK_RESET;

volatile uint8_t MODE_FLAG;
volatile uint8_t SD_FLAG;
volatile uint8_t LOGDATA_FLAG;
volatile uint8_t LED_FLAG;


void systick_init(void)
{
	SysTick_Config(SystemCoreClock / 1000);
}

void SysTick_Handler(void)
{	
	if (clk1000ms != CLK_COMPLETE) // 1 second
    {
		clk1000ms++;
		if (clk1000ms >= 1001)
        {
            clk1000ms = CLK_COMPLETE;
            LED_FLAG = SET;
        }
	}

	if (clk100ms != CLK_COMPLETE) // 0.1 seconds
    {
		clk100ms++;
		if (clk100ms >= 101)
        {
            clk100ms = CLK_COMPLETE;
            LOGDATA_FLAG = SET;
            MODE_FLAG = SET;
            SD_FLAG = SET;
        }
	}
}
