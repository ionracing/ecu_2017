#ifndef GLOBAL_H
#define GLOBAL_H

#include "stdint.h"

typedef struct sensorPackage
{
    uint16_t ID;
    uint32_t DATA;
    uint32_t TIME;
} sensorPackage;

typedef enum carStatus
{
    Standby = 0,
    Running
} carStatus;

extern carStatus CARSTATUS;

extern uint32_t relativeTime;
extern sensorPackage pkgBuffer[100];

#endif //GLOBAL_H
