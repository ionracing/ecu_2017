#include "CAN.h"

/* 
* RPI Comm board & ECU board & STM32F4-Discovery board
* PB8 : CAN1 RX  
* PB9 : CAN1 TX
*
* ECU board
* PB5 : CAN2 RX
* PB6 : CAN2 TX
*/

void CAN_Initialise()
{
	CAN_InitTypeDef         CAN_InitStructure;
    GPIO_InitTypeDef        GPIO_InitStructure;
    CAN_FilterInitTypeDef   CAN_FilterInitStructure;
    
    RCC_AHB1PeriphClockCmd(CAN_GPIO_RCC, ENABLE);
	RCC_APB1PeriphClockCmd(CAN_CAN_RCC, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = CAN_GPIO_PIN_RX | CAN_GPIO_PIN_TX;  
    GPIO_InitStructure.GPIO_Mode = CAN_GPIO_MODE;
    GPIO_InitStructure.GPIO_Speed = CAN_GPIO_SPEED;
    GPIO_InitStructure.GPIO_OType = CAN_GPIO_OTYPE;
    GPIO_InitStructure.GPIO_PuPd  = CAN_GPIO_PUPD;
    GPIO_Init(CAN_GPIO_GPIOX, &GPIO_InitStructure);
    
    GPIO_PinAFConfig(CAN_GPIO_GPIOX, CAN_GPIO_PinSource_RX, CAN_GPIO_AF_CAN);
    GPIO_PinAFConfig(CAN_GPIO_GPIOX, CAN_GPIO_PinSource_TX, CAN_GPIO_AF_CAN);

	CAN_DeInit(CAN_CANx);

    CAN_InitStructure.CAN_TTCM = DISABLE;
    CAN_InitStructure.CAN_ABOM = DISABLE;
    CAN_InitStructure.CAN_AWUM = DISABLE;
    CAN_InitStructure.CAN_NART = DISABLE;
    CAN_InitStructure.CAN_RFLM = DISABLE;
    CAN_InitStructure.CAN_TXFP = DISABLE;
    CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;//CAN_Mode_LoopBack;
    CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;

    /* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/
    CAN_InitStructure.CAN_BS1 = CAN_BS1_11tq;
    CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;
    CAN_InitStructure.CAN_Prescaler = 6;
    CAN_Init(CAN_CANx, &CAN_InitStructure);

    CAN_FilterInitStructure.CAN_FilterNumber = 0;
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
    CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);

    /*
    // Enable FIFO 0 message pending Interrupt //
    CAN_ITConfig(CAN_CANx, CAN_IT_FMP0, ENABLE);
    
    NVIC_InitStructure.NVIC_IRQChannel = CAN_NVIC_IRQChannel;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = CAN_NVIC_IRQChannelPP;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = CAN_NVIC_IRQChannelSP;
    NVIC_InitStructure.NVIC_IRQChannelCmd = CAN_NVIC_IRQChannelCmd;
    NVIC_Init(&NVIC_InitStructure);
    */
}

CanTxMsg msg;
void CAN_Write_8(CAN_TypeDef* CANx, uint16_t id, uint8_t length, uint8_t data[8])
{
    msg.StdId = id;
	msg.IDE = CAN_Id_Standard;
	msg.RTR = CAN_RTR_Data;
	msg.DLC = length;
    
    for(uint8_t i = 0;i < length;i++)
    {
        msg.Data[i] = data[i];
    }
    
    while(CAN_Transmit(CANx, &msg) == CAN_TxStatus_NoMailBox);
}

void CAN_Write_16_bit(CAN_TypeDef* CANx, uint16_t id, uint16_t data)
{
    uint8_t msg[8] = {0};
    
    msg[0] = 0xFF & data;
    msg[1] = 0xFF & (data >> 8);
    
    CAN_Write_8(CANx, id, 2, msg);
}

void CAN_Write_32_bit(CAN_TypeDef* CANx, uint16_t id, uint32_t data)
{
    uint8_t msg[8] = {0};
    
    msg[0] = 0xFF & data;
    msg[1] = 0xFF & (data >> 8);
    msg[2] = 0xFF & (data >> 16);
    msg[3] = 0xFF & (data >> 24);
    
    CAN_Write_8(CANx, id, 4, msg);
}
