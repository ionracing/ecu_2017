#include "DATETIME.h"

uint64_t DATETIME_stringToTime(char date[], char time[])
{
    return DATETIME_getIonTime(
        ((date[0] - '0') * 10) + (date[1] - '0'),
        ((date[2] - '0') * 10) + (date[3] - '0'),
        ((date[4] - '0') * 10) + (date[5] - '0'),
        ((time[0] - '0') * 10) + (time[1] - '0'),
        ((time[2] - '0') * 10) + (time[3] - '0'),
        ((time[4] - '0') * 10) + (time[5] - '0'),
        ((time[7] - '0') * 100) + ((time[8] - '0') * 10) + (time[9] - '0')
    );
}

uint64_t DATETIME_intToTime(uint32_t date, uint32_t time)
{
    uint8_t y, m, d, h, M, s, ms;
    
    date /= 10000; // removes unused desimals
    d = date % 100;
    date /= 100;
    m = date % 100;
    date /= 100;
    y = date % 100;
    
    time /= 10; // removes unused desimals
    ms = time % 1000;
    time /= 1000;
    s = time % 100;
    time /= 100;
    M = time % 100;
    time /= 100;
    h = time % 100;
    
    return DATETIME_getIonTime(y, m, d, h, M, s, ms);
}

uint64_t DATETIME_getIonTime(uint8_t y, uint8_t m, uint8_t d, uint8_t h, uint8_t M, uint8_t s, uint16_t ms)
{
    int32_t n_y = y - 2001;
    int32_t n_m = m - 1;
    int32_t n_d = d - 1;
    
    uint16_t lookup[12];
    lookup[0] = 0;
    lookup[1] = 31;
    lookup[2] = 60;
    lookup[3] = 91;
    lookup[4] = 121;
    lookup[5] = 152;
    lookup[6] = 182;
    lookup[7] = 213;
    lookup[8] = 244;
    lookup[9] = 274;
    lookup[10] = 305;
    lookup[11] = 335;

    int32_t leapYear = (y%4 == 0 && (y%100 != 0 || y%400 == 0));

    int32_t mod = (leapYear && m > 2) ? 1 : 0;

    float yearDays = 365.2425*n_y + 0.2424;
	uint64_t totalDays = n_d + lookup[n_m] + mod + (int)yearDays;
	uint64_t totalMilli = totalDays*24*60*60*1000 + h*60*60*1000 + M*60*1000 + s*1000 + ms;
    return totalMilli;
}

//int main(void) {
	//long long int a = getIonTime(2017,1,10,0,0,0,0);
	//printf("%llu", a);
	//return 0;
//}
