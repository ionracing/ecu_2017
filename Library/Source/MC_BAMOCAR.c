#include "MC_BAMOCAR.h"

void MC_BAMOCAR_Init()
{
    IO_initPinOut(FRG_RUN_GPIO, FRG_RUN_PIN);
    IO_initPinOut(RFE_GPIO, RFE_PIN);
    
    IO_pinHigh(FRG_RUN_GPIO, FRG_RUN_PIN);
    IO_pinHigh(RFE_GPIO, RFE_PIN);
    
    MC_BAMOCAR_Controll(Speed_reference, Speed_reference, FIRST);
}

uint8_t MC_BAMOCAR_Command(uint8_t data[8], uint8_t length)
{
    CanTxMsg msg;
    
    msg.IDE = 0;
    msg.RTR = 0;
    msg.StdId = MC_BAMOCAR_ID;
    msg.DLC = length;
    
    for(uint8_t i = 0;i < length;i++)
    {
        msg.Data[i] = data[i];
    }
    
    return CAN_Transmit(CAN_CANx, &msg);
}

uint8_t MC_BAMOCAR_Responce(CanRxMsg msg, uint8_t data[8])
{    
    for(uint8_t i = 0;i < msg.DLC;i++)
    {
        if(msg.Data[i] != data[i])
            return 0;
    }
    return 1;
}
uint32_t count = 0;
void MC_BAMOCAR_Controll(uint8_t command[8], uint8_t response[8], MC_ERRORS err)
{
    msgResponse.Data[0] = 0;
    msgResponse.Data[1] = 0;
    msgResponse.Data[2] = 0;
    
    MC_BAMOCAR_Command(command, 3);
    
    count = 0;
    while(!msgPendingFlag || count < 0xFFFFFFFF)
        count++;
    
    msgPendingFlag = 0;
    if(!MC_BAMOCAR_Responce(msgResponse, response))
        CAN_Write_16_bit(CAN_CANx, MC_ERROR_ID, err);
}

void MC_BAMOCAR_START(void)
{
    MC_BAMOCAR_Command(Enable_controller, 3);
}
void MC_BAMOCAR_STOP(void)
{
    MC_BAMOCAR_Command(Disable_controller, 3);
}
