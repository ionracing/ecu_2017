#include "IMU_SCC2130.h"

void IMU_Initialise()
{
    SPI_initialise(SPI1);
    for(int i = 0;i < 0xFFFFF;i++);
}

imu_command IMU_CreateCom(uint32_t command)
{
    imu_command com;
    
    com.write_read = (0x01 & (command >> 31));
    com.address = (0x1F & (command >> 26));
    com.return_status = (0x03 & (command >> 24));
    com.data = (0xFFFF & (command >> 8));
    com.crc = (0xFF & command);
    
    return com;
}

uint32_t IMU_WriteCom(imu_command *com)
{
    uint32_t response = 0;
    //ChipSelect(SPI1, LOW);
    //SPI_Send(SPI1, 0xFF);
    response |= (SPI_WriteRead(SPI1, (0xFF & ((com->write_read << 7) | (com->address << 2) | com->return_status)))) << 24;
    response |= (SPI_WriteRead(SPI1, (0xFF & (com->data >> 8)))) << 16;
    response |= (SPI_WriteRead(SPI1, (0xFF & (com->data >> 8)))) << 8;
    response |= SPI_WriteRead(SPI1, (0xFF & (com->data)));
    //NOT SURE IF THIS IS RIGHT.
    SPI_Write(SPI1, (0xFF & (com->crc)));
    
    return response;
}

uint32_t IMU_WriteRead(uint32_t com)
{
    uint32_t response = 0;
    SPI3_ChipSelect(PIN_LOW);
    response |= (SPI_WriteRead(SPI1, (0xFF & (com >> 24)))) << 24;
    response |= (SPI_WriteRead(SPI1, (0xFF & (com >> 16)))) << 16;
    response |= (SPI_WriteRead(SPI1, (0xFF & (com >> 8)))) << 8;
    response |= (SPI_WriteRead(SPI1, (0xFF & (com))));
    SPI3_ChipSelect(PIN_HIGH);
    return response;
}

// Calculate CRC for 24 MSB's of the 32 bit dword 
// (8 LSB's are the CRC field and are not included in CRC calculation) 
uint8_t CalculateCRC(uint32_t Data)
{   
    uint8_t BitIndex, BitValue, xCRC = 0xFF;
    
    for(BitIndex = 31;BitIndex > 7;BitIndex--)
    {
        BitValue = (uint8_t)((Data >> BitIndex) & 0x01);
        xCRC = CRC8(BitValue, xCRC);
    }
    xCRC = (uint8_t)~xCRC;
    
    return xCRC;
}
 
uint8_t CRC8(uint8_t BitValue, uint8_t xCRC)
{
    uint8_t Temp;
    Temp = (uint8_t)(xCRC & 0x80);
    if (BitValue == 0x01)
    {
        Temp ^= 0x80;
    }
    xCRC <<= 1;
    if (Temp > 0)
    {
        xCRC ^= 0x1D;
    }
    return xCRC;
} 
