#include "TIM.h"

void TIM_initialise()
{
    TIM_TimeBaseInitTypeDef TIM_initStruct;
    NVIC_InitTypeDef NVIC_initStruct;
    
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    
    TIM_initStruct.TIM_Period            = 2 - 1;
	TIM_initStruct.TIM_Prescaler         = 42000 - 1;
	TIM_initStruct.TIM_ClockDivision     = TIM_CKD_DIV4;
	TIM_initStruct.TIM_CounterMode       = TIM_CounterMode_Up;
	TIM_initStruct.TIM_RepetitionCounter = 0x0000;
    TIM_TimeBaseInit(TIM2, &TIM_initStruct);
  
	TIM_Cmd(TIM2, ENABLE);
    
    NVIC_initStruct.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_initStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_initStruct.NVIC_IRQChannelSubPriority = 1;
    NVIC_initStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_initStruct);
    
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        //relativeTime++;
    }
}
