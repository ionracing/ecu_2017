#include "ADC_SIMPLE.h"

/* Single channel configuration. */
void ADC_SIMPLE_init(ADCS ADCx)
{	
	ADC_InitTypeDef ADC_initStruct;
	ADC_CommonInitTypeDef ADC_commonInitStruct;
	GPIO_InitTypeDef GPIO_initStruct;

	RCC_APB2PeriphClockCmd(ADC_RCC(ADCx), ENABLE);
    RCC_AHB1PeriphClockCmd(ADC_GPIO_RCC(ADCx), ENABLE);
    
	ADC_StructInit(&ADC_initStruct);
	ADC_CommonStructInit(&ADC_commonInitStruct);
	GPIO_StructInit(&GPIO_initStruct);
	
    /* Configurate GPIO */
    GPIO_initStruct.GPIO_Pin = ADC_GPIO_PIN(ADCx);
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(ADC_GPIO(ADCx), &GPIO_initStruct);
    
	/* Configure ADC in independent mode */
	ADC_commonInitStruct.ADC_DMAAccessMode    = ADC_DMAAccessMode_Disabled;
	ADC_commonInitStruct.ADC_Mode             = ADC_Mode_Independent;
	ADC_commonInitStruct.ADC_Prescaler        = ADC_Prescaler_Div2;
	ADC_commonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_commonInitStruct);
	
	/* Configure the ADC: 12-bit single conversion */
	ADC_initStruct.ADC_Resolution           = ADC_Resolution_12b;
	ADC_initStruct.ADC_ScanConvMode         = DISABLE;
	ADC_initStruct.ADC_ContinuousConvMode   = DISABLE;
	ADC_initStruct.ADC_ExternalTrigConvEdge = 0;
	ADC_initStruct.ADC_ExternalTrigConv     = 0;
	ADC_initStruct.ADC_DataAlign            = ADC_DataAlign_Right;
	ADC_initStruct.ADC_NbrOfConversion      = 1;
	ADC_Init(ADC_ADC(ADCx), &ADC_initStruct);
	
	ADC_Cmd(ADC_ADC(ADCx), ENABLE);
}

uint16_t ADC_SIMPLE_readChannel(ADCS ADCx)
{
	ADC_RegularChannelConfig(ADC_ADC(ADCx), ADC_GPIO_PIN_CHANNEL(ADCx), 1, ADC_SampleTime_480Cycles);
	ADC_SoftwareStartConv(ADC_ADC(ADCx));
	while((ADC_GetFlagStatus(ADC_ADC(ADCx), ADC_FLAG_EOC) == RESET));
	return ADC_GetConversionValue(ADC_ADC(ADCx));
}
