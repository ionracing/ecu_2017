#include "EEPROM_AT24C256C.h"

void EEPROM_AT24C256C_Initialise()
{
    GPIO_InitTypeDef GPIO_InitStruct;
    I2C_InitTypeDef I2C_InitStruct;
    
    RCC_APB1PeriphClockCmd(EEPROM_I2C_RCC, ENABLE);
	RCC_AHB1PeriphClockCmd(EEPROM_GPIO_RCC, ENABLE);
    
    if(EEPROM_GPIO_WP != GPIO_Pin_All)
    {
        GPIO_InitStruct.GPIO_Mode   =   GPIO_Mode_OUT;
        GPIO_InitStruct.GPIO_OType  =   GPIO_OType_OD;
        GPIO_InitStruct.GPIO_Pin    =   EEPROM_GPIO_WP;
        GPIO_InitStruct.GPIO_PuPd   =   GPIO_PuPd_NOPULL;
        GPIO_InitStruct.GPIO_Speed  =   GPIO_Speed_50MHz;
        GPIO_Init(EEPROM_GPIO_GPIOX, &GPIO_InitStruct);
    }
	GPIO_InitStruct.GPIO_Mode   =   EEPROM_GPIO_MODE;
	GPIO_InitStruct.GPIO_OType  =   EEPROM_GPIO_OTYPE;
	GPIO_InitStruct.GPIO_Pin    =   EEPROM_GPIO_PIN_SCL | EEPROM_GPIO_PIN_SDA;
	GPIO_InitStruct.GPIO_PuPd   =   EEPROM_GPIO_PUPD;
	GPIO_InitStruct.GPIO_Speed  =   EEPROM_GPIO_SPEED;
    GPIO_Init(EEPROM_GPIO_GPIOX, &GPIO_InitStruct);

    GPIO_PinAFConfig(EEPROM_GPIO_GPIOX, EEPROM_GPIO_PinSource_SCL, EEPROM_GPIO_AF_I2C);	// SCL
    GPIO_PinAFConfig(EEPROM_GPIO_GPIOX, EEPROM_GPIO_PinSource_SDA, EEPROM_GPIO_AF_I2C); // SDA
    
	I2C_InitStruct.I2C_Ack                  =   EEPROM_I2C_ACK;
	I2C_InitStruct.I2C_AcknowledgedAddress  =   EEPROM_I2C_ACKNOWLEDGEDADRESS;
	I2C_InitStruct.I2C_ClockSpeed           =   EEPROM_I2C_CLOCKSPEED;
	I2C_InitStruct.I2C_DutyCycle            =   EEPROM_I2C_DUTYCYCLE;
	I2C_InitStruct.I2C_Mode                 =   EEPROM_I2C_MODE;
	I2C_InitStruct.I2C_OwnAddress1          =   EEPROM_I2C_OWNADDRESS1;
    I2C_Init(EEPROM_I2C_I2CX, &I2C_InitStruct);
    
	I2C_Cmd(EEPROM_I2C_I2CX, ENABLE);
}

void EEPROM_AT24C256C_SoftwareReset()
{
    I2C_GenerateSTART(EEPROM_I2C_I2CX, ENABLE);
    for(int i = 0;i < 9;i++)
    {
        GPIO_ResetBits(EEPROM_GPIO_GPIOX, EEPROM_GPIO_PIN_SCL | EEPROM_GPIO_PIN_SDA);
        for(int i = 0;i < 0xFFF;i++);
        GPIO_SetBits(EEPROM_GPIO_GPIOX, EEPROM_GPIO_PIN_SCL | EEPROM_GPIO_PIN_SDA);
        for(int i = 0;i < 0xFFF;i++);
    }
    I2C_GenerateSTART(EEPROM_I2C_I2CX, ENABLE);
    I2C_GenerateSTOP(EEPROM_I2C_I2CX, ENABLE);
}

uint8_t EEPROM_AT24C256C_AckPolling(uint8_t Direction)
{
    I2C_GenerateSTART(EEPROM_I2C_I2CX, ENABLE);
	while(!I2C_CheckEvent(EEPROM_I2C_I2CX, I2C_EVENT_MASTER_MODE_SELECT)){}
        
	I2C_Send7bitAddress(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS << 1, Direction);
	while(!I2C_CheckEvent(EEPROM_I2C_I2CX, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){}
        
    return GPIO_ReadInputDataBit(EEPROM_GPIO_GPIOX, EEPROM_GPIO_PinSource_SDA);
}

void EEPROM_AT24C256C_Write8(uint16_t wordAddress, uint8_t value)
{
    I2C_Write_8(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress, value);
}

uint8_t EEPROM_AT24C256C_Read8(uint16_t wordAddress)
{
    return I2C_Read_8(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress);
}

void EEPROM_AT24C256C_Write_Struct(uint16_t wordAddress, struct storedValues *values)
{
    /* Cast struct to a byte array */
    uint8_t *valueArray = (uint8_t*)(values);
    uint32_t sizeStruct = sizeof(struct storedValues);
    uint32_t sizeUint = sizeof(uint8_t);
    
    uint8_t length = (sizeStruct / sizeUint);
    
	// WP_Unlock();
	
    /* Write preamble */
    I2C_Write_8(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress, EEPROM_AT24C256C_PREAMBLE);
    wordAddress += 8;
    for(int i = 0;i < 0xFFFFF;i++);
    /* Write the values */
    I2C_Write_Page(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress, valueArray, length);
    
    // WP_Lock();
}

void EEPROM_AT24C256C_Read_Struct(uint16_t wordAddress, struct storedValues *values)
{
    uint32_t sizeStruct = sizeof(struct storedValues);
    uint32_t sizeUint = sizeof(uint8_t);
    
    uint8_t length = sizeStruct / sizeUint;
    uint8_t valueArray[sizeof(struct storedValues)] = {0};
    
    /* Check if the preamble is there */
    uint16_t Preamble = I2C_Read_8(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress);
    wordAddress += 8;
    if(Preamble == EEPROM_AT24C256C_PREAMBLE)
    {
        I2C_Read_Page(EEPROM_I2C_I2CX, EEPROM_AT24C256C_ADDRESS, wordAddress, valueArray, length);
        *values = *((storedValues*)valueArray);
    }
    else /* Corrupt or missing data */
    {
        /* Save the current values used instead */
        EEPROM_AT24C256C_Write_Struct(wordAddress, values);
    }
}
