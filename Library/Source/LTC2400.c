#include "LTC2400.h"

void LTC2400_initialise()
{
    SPI_initialise(SPI1);
}

uint32_t LTC2400_read(uint8_t channel)
{
    uint32_t timeStamp;
    do
    {
        GPIO_SetBits(GPIOD, CS_BackLeftDamper);
        timeStamp = relativeTime;
        while((timeStamp + 10) > relativeTime); // Wait x amount of time not set yet.
        GPIO_ResetBits(GPIOD, CS_BackLeftDamper);
    } while(!GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_2));
    GPIO_SetBits(GPIOD, CS_BackLeftDamper);
    timeStamp = relativeTime;
    while((timeStamp + 10) > relativeTime); // Small wait.
    GPIO_ResetBits(GPIOD, CS_BackLeftDamper);
    // Begin read.
    return 1;
}

void LTC2400_write(uint32_t data)
{
    
}
