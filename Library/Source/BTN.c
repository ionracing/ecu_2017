#include "BTN.h"

void BTN_Initialise(uint16_t BTNx)
{
	GPIO_InitTypeDef GPIO_InitStruct;
    EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

    RCC_AHB1PeriphClockCmd(BTN_GPIO_RCC(BTNx), ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    GPIO_InitStruct.GPIO_Pin = BTN_GPIO_Pin(BTNx);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init(BTN_GPIOx(BTNx), &GPIO_InitStruct);
    
    SYSCFG_EXTILineConfig(BTN_EXTI_PortSource(BTNx), BTN_EXTI_PinSource(BTNx));

	EXTI_InitStruct.EXTI_Line = BTN_EXTI_Linex(BTNx);
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = BTN_NVIC_IRQChannel(BTNx);
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = BTN_NVIC_IRQChannelPP(BTNx);
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = BTN_NVIC_IRQChannelSP(BTNx);
	NVIC_InitStruct.NVIC_IRQChannelCmd = BTN_NVIC_Enable(BTNx);
	NVIC_Init(&NVIC_InitStruct);
}

uint8_t BTN_Status(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, BitAction status)
{
    uint16_t counter = 50;
    while(counter > 0)
    {
        if(GPIO_ReadInputDataBit(GPIOx, GPIO_Pin) == status)
            counter++;
        else
            counter--;

        if(counter > 10000){
            return BTN_HIGH;
        }
    }
    return BTN_LOW;
}
