#include "Buffer.h"

static uint16_t HEAD, TAIL, LENGTH;
static uint64_t READ_Counter, WRITE_Counter;
static sensorPackage* START;

void Buffer_Init(sensorPackage *buffer, uint16_t length)
{
    HEAD = 0;
    TAIL = 0;
    START = buffer;
    LENGTH = length;
}

void Buffer_Add(sensorPackage pkg)
{
    if(++WRITE_Counter - READ_Counter > LENGTH)
    {
        TAIL++;
        READ_Counter++;
    }
    else if(HEAD+1 == TAIL)
        TAIL++;
        
    if(TAIL == LENGTH)
        TAIL = 0;
    
    *(START + HEAD) = pkg;
    
    if(++HEAD == LENGTH)
        HEAD = 0;
}

sensorPackage Buffer_Get()
{
    READ_Counter++;
    if(TAIL == LENGTH)
        TAIL = 0;
    return *(START + TAIL++);
}

uint8_t Buffer_hasValue()
{
    if(READ_Counter == WRITE_Counter)
        return 0;
    else
        return 1;
}
