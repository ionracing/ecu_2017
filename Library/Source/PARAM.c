#include "PARAM.h"

//saves the pointer to an array of pointers and then gets the value from the SD card using the index, and saves that to the pointer value.
void PARAM_getValue(uint32_t *value, TRACTION_CONTROL_STATUS param)
{
    FATFS FatFs;
    FIL fil;
    TCHAR buff;
    
    //Save value pointer to array
    ptrValues[param] = value;
    
    //get value from SD_Card and save to pointer value.
    SD_CARD_Mount(&FatFs);
    f_open(&fil, "Param.txt", FA_OPEN_EXISTING | FA_READ); //Remember to create the file.
    f_lseek(&fil, param * 6); //the offset is not correct.
    f_gets(&buff, 6, &fil);
    
    *value = buff; //This is wrong.
    
    f_close(&fil);
}

void PARAM_changeValue(uint32_t value, TRACTION_CONTROL_STATUS param) //not sure if I can use Parameters as typpe on this function.
{
    FATFS FatFs;
    FIL fil;
    
    //Save new value to SD_Card.
    SD_CARD_Mount(&FatFs);
    f_open(&fil, "Param.txt", FA_OPEN_EXISTING | FA_READ | FA_WRITE); //Remember to create the file.
    f_lseek(&fil, (param - 0x8A) * 6); //the offset is not correct.
    
    char index[6] = {(param - 0x8A), value >> 24, value >> 16, value >> 8, value, '\n'};
    
    f_puts(index, &fil); //Not sure if this overwrites the old value.
    
    //get pointer from array and change the value to the new value.
    *ptrValues[(param - 0x8A)] = value;
    
    f_close(&fil);
}
