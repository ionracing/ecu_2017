#ifndef LTC2400_H
#define LTC2400_H

#include "stm32f4xx_gpio.h"
#include "stdint.h"
#include "global.h"
#include "SPI.h"

#define CS_FrontRightDamper     GPIO_Pin_1
#define CS_FrontLeftDamper      GPIO_Pin_2
#define CS_BackRightDamper      GPIO_Pin_3
#define CS_BackLeftDamper       GPIO_Pin_4

uint32_t LTC2400_read(uint8_t channel);
void LTC2400_write(uint32_t data);

#endif
