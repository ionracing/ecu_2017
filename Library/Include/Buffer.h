#ifndef BUFFER_H
#define BUFFER_H

#include "global.h"
#include "stdint.h"

void Buffer_Init(sensorPackage *pkg, uint16_t length);
void Buffer_Add(sensorPackage pkg);
sensorPackage Buffer_Get(void);
uint8_t Buffer_hasValue(void);

#endif //BUFFER_H
