#ifndef DATETIME_H
#define DATETIME_H

#include "stdio.h"
#include "stdint.h"

typedef enum
{
    YEAR = 2001,
    MONTH = 1,
    DAY = 1
} START_DATE;

uint64_t DATETIME_stringToTime(char date[], char time[]);
uint64_t DATETIME_intToTime(uint32_t date, uint32_t time);
uint64_t DATETIME_getIonTime(uint8_t y, uint8_t m, uint8_t d, uint8_t h, uint8_t M, uint8_t s, uint16_t ms);

#endif
