#ifndef MC_BAMOCAR_H
#define MC_BAMOCAR_H

#include "stdint.h"
#include "CAN.h"
#include "IO.h"

#define MC_BAMOCAR_ID           0x210
#define MC_BAMOCAR_RESPONSE_ID  0x181
#define MC_ERROR_ID             0x12C

uint8_t msgPendingFlag = 0;
CanRxMsg msgResponse;

typedef enum
{
    FIRST = 0,
    SECOND
} MC_ERRORS;

static uint8_t Enable_controller[3] = {0x51,0x02,0x00};
static uint8_t Disable_controller[3] = {0x51,0x04,0x00};
static uint8_t Speed_reference[3] = {0x31,0xCD,0x0C};

void MC_BAMOCAR_Init(void);
uint8_t MC_BAMOCAR_Command(uint8_t data[8], uint8_t length);
uint8_t MC_BAMOCAR_Responce(CanRxMsg msg, uint8_t data[8]);
void MC_BAMOCAR_Controll(uint8_t command[8], uint8_t response[8], MC_ERRORS err);
void MC_BAMOCAR_START(void);
void MC_BAMOCAR_STOP(void);

#endif // MC_BAMOCAR_H
