#ifndef PARAM_H
#define PARAM_H

#include "stm32f4xx.h"
#include "SD_CARD.h"
#include "ff.h"

typedef enum
{
    NOT_ACTIVE,
    ACTIVE
} TRACTION_CONTROL_STATUS;

#define MAX 100

extern uint32_t *ptrValues[MAX];

void PARAM_getValue(uint32_t *value, TRACTION_CONTROL_STATUS param);
void PARAM_changeValue(uint32_t value, TRACTION_CONTROL_STATUS param);


#endif //PARAM_H
