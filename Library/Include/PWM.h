#ifndef PWM_H
#define PWM_H

#include "stm32f4xx.h"
#include <stdio.h>

#define PWM_SPEED_HERTZ 20000 //20KHz

// This is calcualted values that use The clock of TIM1 & TIM8
#define PWM_PRESCALER(Hz)   (Hz == 20000 ? 0 : 0)
#define PWM_PERIOD(Hz)      (Hz == 20000 ? 8400: 8400)

#define PWM_DUTY_CYCLE(duty)   (((PWM_PERIOD(PWM_SPEED_HERTZ) + 1) * duty) / 100) // Duty is from 0-100, value is in percentage

void PWM_Init(void);
void PWM_BMS_Init(void);
void PWM_BMS_Init_v2(void);
void TIM_Config(void);
void PWM_Config(void);

#endif //PWM_H
