#ifndef EEPROM_AT24C256C_H
#define EEPROM_AT24C256C_H

#include "IO_Mapping.h"
#include "I2C.h"

#define EEPROM_AT24C256C_ADDRESS 0x57   
#define EEPROM_AT24C256C_PREAMBLE 0xAA

#define CARISRUNNING_ADDRESS    0x00
#define PEDALDIFFERENCE_ADDRESS 0x02

typedef struct EEPROM_Byte_Write
{
    unsigned        start_bits:4;   // always 1010
    unsigned        A2_A1_A0:3;     // Devise address bits
    unsigned        R_W:1;          // Read = 1, Write = 0
} EEPROM_Byte_Write;

typedef struct storedValues
{
    int8_t int8Test;
    uint8_t uint8Test;
    int16_t int16Test;
    uint16_t uint16Test;
    int32_t int32Test;
    uint32_t uint32Test;
    int64_t int64Test;
    uint64_t uint64Test;
} storedValues;


void EEPROM_AT24C256C_Initialise(void);
void EEPROM_AT24C256C_SoftwareReset(void);
uint8_t EEPROM_AT24C256C_AckPolling(uint8_t Direction);
void EEPROM_AT24C256C_Write8(uint16_t wordAddress, uint8_t value);
uint8_t EEPROM_AT24C256C_Read8(uint16_t wordAddress);
void EEPROM_AT24C256C_Write_Struct(uint16_t wordAddress, struct storedValues *values);
void EEPROM_AT24C256C_Read_Struct(uint16_t wordAddress, struct storedValues *values);

#endif //EEPROM_AT24C256C_H
