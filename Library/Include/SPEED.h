#ifndef SPEED_H
#define SPEED_H

#include "IC.h"
#include "CAN.h"

void SPEED_Initialise(void);
void SPEED_Send(void);
uint16_t SPEED_Calculate(void);

#endif //SPEED_H
