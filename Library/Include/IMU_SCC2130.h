#ifndef IMU_SCC2130_h
#define IMU_SCC2130_h

#include "stm32f4xx.h"
#include "SPI.h"
#include "global.h"
#include "ION_ID.h"

#define RATE                0x040000F7
#define ACC_X               0x100000E9
#define ACC_Y               0x140000EF
#define ACC_Z               0x180000E5
#define TEMP                0x1C0000E3
#define RATE_STATUS_1       0x240000C7
#define RATE_STATUS_2       0x280000CD
#define ACC_STATUS          0x3C0000D3
#define HARD_RESET          0xD8000431
#define MONITOR_ST          0xD80008AD
#define SERIAL_ID0          0x600000A1
#define SERIAL_ID1          0x640000A7
#define COMMOM_STATUS       0x6C0000AB
#define STATUS_SUMMARY      0x7C0000B3
#define WRITE_FLT_60        0xFC200002
#define WRITE_FLT_10        0xFC1000C7

typedef struct
{
    unsigned write_read:1;          //Write = 1, Read = 0.
    unsigned address:5;
    unsigned return_status:2;
    unsigned data:16;
    unsigned crc:8;                 //Checksum - all of the above.
} imu_command;

typedef enum
{
    ININT       = 0,
    NORMAL      = 1,
    SELFTEST    = 2,
    IMU_ERROR   = 3
} IMU_RESPONSE;

void IMU_Initialise(void);
imu_command IMU_CreateCom(uint32_t command);
uint32_t IMU_WriteCom(imu_command *com);
uint32_t IMU_WriteRead(uint32_t com);
uint8_t CalculateCRC(uint32_t Data);
uint8_t CRC8(uint8_t BitValue, uint8_t xCRC);
void IMU_getValues(void);
#endif
