#ifndef IO_H
#define IO_H

#include "stdint.h"
#include "stm32f4xx_gpio.h"
#include "IO_Mapping.h"

#define IO_GPIO_RCC(GPIOx)      ((GPIOx == GPIOA) ? RCC_AHB1Periph_GPIOA : \
                                ((GPIOx == GPIOB) ? RCC_AHB1Periph_GPIOB : \
                                ((GPIOx == GPIOC) ? RCC_AHB1Periph_GPIOC : \
                                ((GPIOx == GPIOD) ? RCC_AHB1Periph_GPIOD : \
                                ((GPIOx == GPIOE) ? RCC_AHB1Periph_GPIOE : RCC_AHB1Periph_GPIOF)))))

void IO_initPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode, GPIOOType_TypeDef GPIO_OType, GPIOPuPd_TypeDef GPIO_PuPd, GPIOSpeed_TypeDef GPIO_Speed);
void IO_initPinOut(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void IO_initPinIn(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void IO_pinHigh(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void IO_pinLow(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void IO_pinToggle(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif //IO_H
