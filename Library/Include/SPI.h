#ifndef SPI_H
#define SPI_H

#include "stm32f4xx_spi.h"
#include "IO_Mapping.h"

typedef enum
{
    PIN_HIGH = 1,
    PIN_LOW = 0
} STATE;

#define temp 1

#warning "change this later"
#define SPI1_ChipSelect(STATE)   (STATE == PIN_HIGH ? GPIO_SetBits(GPIOA, temp) : GPIO_ResetBits(GPIOA, temp))
#define SPI2_ChipSelect(STATE)   (STATE == PIN_HIGH ? GPIO_SetBits(GPIOA, temp) : GPIO_ResetBits(GPIOA, temp))
#define SPI3_ChipSelect(STATE)   (STATE == PIN_HIGH ? GPIO_SetBits(GPIOA, temp) : GPIO_ResetBits(GPIOA, temp))


typedef enum
{
    SPI_SPEED_42MHz     = 0,
    SPI_SPEED_21MHz     = 1,
    SPI_SPEED_10MHz     = 2,
    SPI_SPEED_5MHz      = 3,
    SPI_SPEED_2600KHz   = 4,
    SPI_SPEED_1300KHz   = 5,
    SPI_SPEED_650KHz    = 6,
    SPI_SPEED_320KHz    = 7,
    SPI_SPEED_160KHz    = 8
} SPI_SPEED;

void SPI_initialise(SPI_TypeDef* SPIx);
void SPI_SetSpeed(SPI_TypeDef* SPIx, SPI_SPEED speed);
uint8_t SPI_Read(SPI_TypeDef* SPIx);
void SPI_Write(SPI_TypeDef* SPIx, uint8_t Data);
uint8_t SPI_WriteRead(SPI_TypeDef* SPIx, uint8_t msg);

#endif //SPI_H
